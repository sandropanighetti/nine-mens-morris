package com.lpi.ninemensmorris;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;
import com.lpi.ninemensmorris.game.MatchInfo;

public class Statistics extends LinkedList<MatchInfo>{

	public List<MatchInfo> getAllMatchesInfo() {
		try {

			FileHandle file = Gdx.files.local("statistics.conf");

			Json gson = new Json();
			LinkedList<MatchInfo> current = null;

			// Gdx.app.debug("result", result);
//			current = gson.fromJson(file.readString(),
//					new TypeToken<LinkedList<MatchInfo>>() {
//					}.getType());
			
			current = gson.fromJson(Statistics.class, file);

			return current;
		} catch (Exception e) {
			return null;
		}
	}

	public static void addMatchInfo(MatchInfo matchInfo) {
		FileHandle file = Gdx.files.local("statistics.conf");

		Json gson = new Json();
		LinkedList<MatchInfo> current = null;

		try {
			String result = file.readString();
//			Gdx.app.debug("result", result);
//			current = gson.fromJson(result,
//					new TypeToken<LinkedList<MatchInfo>>() {
//					}.getType());
			
			current = gson.fromJson(Statistics.class, result);

		} catch (Exception e) {
//			e.printStackTrace();
		}
		if(current == null)
			current = new LinkedList<MatchInfo>();
		current.add(matchInfo);
//		String toWrite = gson.toJson(current,
//				new TypeToken<LinkedList<MatchInfo>>() {
//				}.getType());
		
		String toWrite = gson.toJson(current);

		file.writeString(toWrite, false);
	}
}
