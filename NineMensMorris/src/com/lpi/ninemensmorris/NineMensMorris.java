package com.lpi.ninemensmorris;

import com.badlogic.gdx.Game;
import com.lpi.ninemensmorris.Messages.Language;
import com.lpi.ninemensmorris.game.MatchInfo;
import com.lpi.ninemensmorris.game.Opponent.OpponentColor;
import com.lpi.ninemensmorris.game.Opponent.OpponentType;
import com.lpi.ninemensmorris.screen.GameScreen;
import com.lpi.ninemensmorris.screen.HelpScreen;
import com.lpi.ninemensmorris.screen.MatchInfoScreen;
import com.lpi.ninemensmorris.screen.MenuScreen;
import com.lpi.ninemensmorris.screen.SettingMatchScreen;

public class NineMensMorris extends Game {

	private GameScreen gameScreen;
	private SettingMatchScreen settingMatchScreen;
	private Settings settings;

	@Override
	public void create() {
//		settings.save();

		// Loading assets
		Assets.load();
		
		// Load settings
		settings = new Settings();
		settings.loadSettings();
//		settings.setLanguage(Language.EN);
		Messages.load(settings.getLanguage());
//		Assets.finalMusic.play();

		@SuppressWarnings("unused")
		MatchConfiguration matchConfiguration = new MatchConfiguration(
				OpponentType.HUMAN, OpponentType.COMPUTER);

		MatchInfo matchInfo = new MatchInfo();
		matchInfo.setMoves(40);
		matchInfo.setOpponentColorWinner(OpponentColor.WHITE);
		matchInfo.setOpponentTypeA(OpponentType.HUMAN);
		matchInfo.setOpponentTypeB(OpponentType.COMPUTER);
		matchInfo.setTime(45.04f);
		this.setScreen(new MatchInfoScreen(this, matchInfo));
		// this.setScreen(new GameScreen(this, matchConfiguration));
		// this.setScreen(new SettingMatchScreen(this));

//		this.setScreen(new MenuScreen(this));
		

	}

	/**
	 * @return the gameScreen
	 */
	public GameScreen getGameScreen() {
		return gameScreen;
	}

	/**
	 * @param gameScreen
	 *            the gameScreen to set
	 */
	public void setGameScreen(GameScreen gameScreen) {
		this.gameScreen = gameScreen;
	}

	/**
	 * @return the settingMatchScreen
	 */
	public SettingMatchScreen getSettingMatchScreen() {
		if (settingMatchScreen == null)
			settingMatchScreen = new SettingMatchScreen(this);
		return settingMatchScreen;
	}

	/**
	 * @param settingMatchScreen
	 *            the settingMatchScreen to set
	 */
	public void setSettingMatchScreen(SettingMatchScreen settingMatchScreen) {
		this.settingMatchScreen = settingMatchScreen;
	}

	/**
	 * @return the settings
	 */
	public Settings getSettings() {
		return settings;
	}

	/**
	 * @param settings the settings to set
	 */
	public void setSettings(Settings settings) {
		this.settings = settings;
	}
}
