package com.lpi.ninemensmorris.game.ai;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class AppletTester extends Applet implements MouseListener,
		MouseMotionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -357531028621360550L;

	private static int threadCounter = 0;
	private static int numberOfThread = 1;
	private static long startTime;
	private static long finalTime;
	
	private int lastx;
	private int lasty;

	final int depth = 4;
	AiNode first;

	public void init() {
		System.out.println("------------START------------");
		startTime = System.currentTimeMillis();

		new Thread(new Runnable() {

			@Override
			public void run() {
				int pre = -1;
				int cnt = 0;
				System.out.println("counter[" + cnt + "]: " + AiNode.getCounter());
				while (threadCounter < numberOfThread) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (pre != AiNode.getCounter()) {
						cnt++;
						pre = AiNode.getCounter();
						System.out.println("counter[" + cnt + "]: "
								+ AiNode.getCounter());
					}

				}
			}
		}).start();

//		first = new AiNode("abcdknpquefhjlmswy", true);
//		first = new AiNode("bhoqrtw00cegijklmy", true);
//		first = new AiNode("000000000000000000", true);
		first = new AiNode("bgimoprtvacdfklnq1", false);
		first.fillChildren(depth);
		threadCounter++;

		new Thread(new Runnable() {

			@Override
			public void run() {
				boolean flag = true;
				while (flag) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (threadCounter == numberOfThread)
						flag = false;
				}

				finalTime = System.currentTimeMillis();
				long totalTime = finalTime - startTime;
				System.out.println("------------STOP------------");
				System.out.println("Total time: " + totalTime);
			}
		}).start();

		// System.out.println("counter: " + AiNode.getCounter());

		first.setWidth(80);
		first.setHeight(80);
		first.setX(this.getWidth() / 2);
		first.setY(40);
		first.setVisible(true);
		


		addMouseListener(this);
		addMouseMotionListener(this);
	}

	public void paint(Graphics g) {

		drawSchema(g, first);
	}

	public void drawSchema(Graphics g, AiNode aiNode) {
		if (!aiNode.isVisible())
			return;

//		if (node.getParent() != null) {
			if (aiNode.isJustDoneATris()) {
				if (aiNode.isMoveWhite())
					g.setColor(Color.blue);
				else
					g.setColor(Color.red);
			} else {
				if (!aiNode.isMoveWhite())
					g.setColor(Color.blue);
				else
					g.setColor(Color.red);
			}
//		}

//		if (!node.isMoveWhite())
//			g.setColor(Color.blue);
//		else
//			g.setColor(Color.red);
		// g.drawRect(node.getX() - node.getWidth() / 2,
		// node.getY() - node.getHeight() / 2, node.getWidth(),
		// node.getHeight());

		int spacing = aiNode.getWidth() / 8;

		g.drawRect(aiNode.getX() - aiNode.getWidth() / 2 + spacing, aiNode.getY()
				- aiNode.getHeight() / 2 + spacing, spacing * 6, spacing * 6);

		g.drawRect(aiNode.getX() - aiNode.getWidth() / 2 + spacing * 2, aiNode.getY()
				- aiNode.getHeight() / 2 + spacing * 2, spacing * 4, spacing * 4);

		g.drawRect(aiNode.getX() - aiNode.getWidth() / 2 + spacing * 3, aiNode.getY()
				- aiNode.getHeight() / 2 + spacing * 3, spacing * 2, spacing * 2);

		g.drawLine(aiNode.getX() - aiNode.getWidth() / 2 + spacing * 4, aiNode.getY()
				- aiNode.getHeight() / 2 + spacing, aiNode.getX() - aiNode.getWidth()
				/ 2 + spacing * 4, aiNode.getY() - aiNode.getHeight() / 2 + spacing
				* 3);
		
		g.drawLine(aiNode.getX() - aiNode.getWidth() / 2 + spacing * 4, aiNode.getY()
				- aiNode.getHeight() / 2 + spacing*5, aiNode.getX() - aiNode.getWidth()
				/ 2 + spacing * 4, aiNode.getY() - aiNode.getHeight() / 2 + spacing
				* 7);

		g.drawLine(aiNode.getX() - aiNode.getWidth() / 2 + spacing, aiNode.getY()
				- aiNode.getHeight() / 2 + spacing * 4,
				aiNode.getX() - aiNode.getWidth() / 2 + spacing * 3, aiNode.getY()
						- aiNode.getHeight() / 2 + spacing * 4);

		g.drawLine(aiNode.getX() - aiNode.getWidth() / 2 + spacing*5, aiNode.getY()
				- aiNode.getHeight() / 2 + spacing * 4,
				aiNode.getX() - aiNode.getWidth() / 2 + spacing * 7, aiNode.getY()
						- aiNode.getHeight() / 2 + spacing * 4);

		g.setColor(Color.blue);
		int ballSize = spacing * 5 / 6;
		for (byte i = 0; i < 9; i++) {
			char current = aiNode.getState().charAt(i);
			if ('0' != current && '1' != current) {
				drawPiece(g, aiNode.getX() - aiNode.getWidth() / 2 + spacing
						* (1 + AiNode.getJ(current)) + spacing / 4,
						aiNode.getY() - aiNode.getHeight() / 2 + spacing
								* (1 + AiNode.getI(current)) + spacing / 4,
						ballSize, ballSize);
			}
		}

		g.setColor(Color.red);
		for (byte i = 9; i < 18; i++) {
			char current = aiNode.getState().charAt(i);
			if ('0' != current && '1' != current) {
				drawPiece(g, aiNode.getX() - aiNode.getWidth() / 2 + spacing
						* (1 + AiNode.getJ(current)) + spacing / 4,
						aiNode.getY() - aiNode.getHeight() / 2 + spacing
								* (1 + AiNode.getI(current)) + spacing / 4,
						ballSize, ballSize);
			}
		}

		int row = 5;
		g.setFont(new Font(null, Font.PLAIN, aiNode.getHeight() / row));
		g.setColor(Color.black);
		g.drawString("Value:", aiNode.getX() + aiNode.getWidth() * 2 / 4,
				aiNode.getY() - aiNode.getHeight() / 2 + aiNode.getHeight() / row * 1);
		g.drawString("Depth:", aiNode.getX() + aiNode.getWidth() * 2 / 4,
				aiNode.getY() - aiNode.getHeight() / 2 + aiNode.getHeight() / row * 2);
		g.drawString("In base:", aiNode.getX() + aiNode.getWidth() * 2 / 4,
				aiNode.getY() - aiNode.getHeight() / 2 + aiNode.getHeight() / row * 3);
		g.drawString("In game:", aiNode.getX() + aiNode.getWidth() * 2 / 4,
				aiNode.getY() - aiNode.getHeight() / 2 + aiNode.getHeight() / row * 4);
		g.drawString("Just tris:", aiNode.getX() + aiNode.getWidth() * 2 / 4,
				aiNode.getY() - aiNode.getHeight() / 2 + aiNode.getHeight() / row * 5);

		int spacingA = 15;
		int spacingB = 35;
		g.drawString(aiNode.isJustDoneATris() + "", aiNode.getX() + aiNode.getWidth()
				* 3 / 4 + spacingA + spacingB * 1,
				aiNode.getY() - aiNode.getHeight() / 2 + aiNode.getHeight() / row * 5);

		g.setColor(Color.blue);
		g.drawString(aiNode.getWhiteValue() + "", aiNode.getX() + aiNode.getWidth()
				* 3 / 4 + spacingA + spacingB * 1,
				aiNode.getY() - aiNode.getHeight() / 2 + aiNode.getHeight() / row * 1);
		g.drawString(
				aiNode.getWhiteDepthFromTris() + "",
				aiNode.getX() + aiNode.getWidth() * 3 / 4 + spacingA + spacingB * 1,
				aiNode.getY() - aiNode.getHeight() / 2 + aiNode.getHeight() / row * 2);
		g.drawString(
				aiNode.getWhitePiecesInBase() + "",
				aiNode.getX() + aiNode.getWidth() * 3 / 4 + spacingA + spacingB * 1,
				aiNode.getY() - aiNode.getHeight() / 2 + aiNode.getHeight() / row * 3);
		g.drawString(
				aiNode.getWhitePiecesInGame() + "",
				aiNode.getX() + aiNode.getWidth() * 3 / 4 + spacingA + spacingB * 1,
				aiNode.getY() - aiNode.getHeight() / 2 + aiNode.getHeight() / row * 4);

		g.setColor(Color.red);

//		g.drawString(node.getBlackValue() + "", node.getX() + node.getWidth()
//				* 3 / 4 + spacingA + spacingB * 2,
//				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 1);
//		g.drawString(
//				node.getBlackDepthFromTris() + "",
//				node.getX() + node.getWidth() * 3 / 4 + spacingA + spacingB * 2,
//				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 2);
//		g.drawString(
//				node.getBlackPiecesInBase() + "",
//				node.getX() + node.getWidth() * 3 / 4 + spacingA + spacingB * 2,
//				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 3);
//		g.drawString(
//				node.getBlackPiecesInGame() + "",
//				node.getX() + node.getWidth() * 3 / 4 + spacingA + spacingB * 2,
//				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 4);

		for (AiNode n : aiNode.getChildren()) {
			if (n.isVisible()) {
				// if (!node.isMoveWhite())
				// g.setColor(Color.red);
				// else
				// g.setColor(Color.blue);
				g.setColor(Color.black);
				g.drawLine(aiNode.getX(), aiNode.getY() + aiNode.getHeight() / 2,
						n.getX(), n.getY() - n.getHeight() / 2);
				drawSchema(g, n);
			}
		}
	}

	private void drawPiece(Graphics g, int x, int y, int width, int height) {
		g.fillRect(x - width / 2, y - height / 2, width, height);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// System.out.println("pressed");
		AiNode dum = first.getAtPosition(e.getX(), e.getY());
		if (dum != null) {

			if (dum.isVisible()) {
				System.out.println("number of white tris: " + dum.getNumberOfWhiteTris());
				System.out.println("number of black tris: " + dum.getNumberOfBlackTris());
				boolean isAChildrenVisible = false;
				for (AiNode aiNode : dum.getChildren())
					if (aiNode.isVisible())
						isAChildrenVisible = true;

				if (isAChildrenVisible) {
					dum.setVisible(false);
					dum.setVisible(true);
					// System.out.println("a children is visible");
				} else {
					if (dum.getParent() != null) {
						dum.getParent().setChildrenVisible(false);
						dum.getParent().setChildrenVisible(true);
					}
					dum.setChildrenVisible(true);
					dum.setVisible(true);
					dum.fillChildren(depth);
				}
				// dum.setVisible(true);

				repaint();
			}
		}

		// if (first.contains(e.getX(), e.getY()))
		// System.out.println("contains");
	}

	@Override
	public void mousePressed(MouseEvent e) {
		lastx = e.getX();
		lasty = e.getY();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		first.setX(first.getX()+e.getX()-lastx);
		first.setY(first.getY()+e.getY()-lasty);
		
//		first.x += e.getX() - lastx;
//		root.y += e.getY() - lasty;
//		root.setCord();
		lastx = e.getX();
		lasty = e.getY();
		repaint();
//		AiNode dum = first.getAtPosition(e.getX(), e.getY());
//		if (dum == first) {
//			first.setX(e.getX());
//			first.setY(e.getY());
//			repaint();
//		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
