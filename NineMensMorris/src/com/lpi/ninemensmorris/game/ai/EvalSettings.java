package com.lpi.ninemensmorris.game.ai;

public class EvalSettings {
	public static final int millFormable = 50;
	public static final int millFormed = 70;
	public static final int millBlocked = 60;
	public static final int millOpponent = -80;
	public static final int capturedPiece = 70;
	public static final int lostPiece = -110;
	public static final int adjacentSpot = 2;
	public static final int blockedOpponentSpot = 2;
	public static final int worstScore = -10000;
	public static final int bestScore = 10000;
	public static final int justTris = 4000;
	public static final int justOpponentTris = -400;
	
}
