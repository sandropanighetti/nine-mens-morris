package com.lpi.ninemensmorris.game.ai;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.lpi.ninemensmorris.game.ListOfPieces;
import com.lpi.ninemensmorris.game.Node;
import com.lpi.ninemensmorris.game.Piece;
import com.lpi.ninemensmorris.game.Schema;
import com.lpi.ninemensmorris.game.Opponent.OpponentColor;

public class AiNode {

	private static int counter = 0;
	public static boolean enableToKillEveryoneIfInTris = false;

	private String state;

	private AiNode parent;

	private List<AiNode> children;

	private int whiteValue;
	private int blackValue;
	private byte whiteDepthFromTris = 99;
	private byte blackDepthFromTris = 99;
	private boolean justDoneATris;

	private boolean moveWhite;

	private int x;
	private int y;
	private int width;
	private int height;
	private boolean visible;

	/**
	 * @param state
	 * @param parent
	 * @param lastMoveWhite
	 */
	public AiNode(AiNode parent, boolean moveWhite) {
		super();
		this.state = parent.state;
		this.parent = parent;
		this.moveWhite = moveWhite;
		children = new LinkedList<AiNode>();
		counter++;
	}

	/**
	 * @param state
	 * @param parent
	 * @param lastMoveWhite
	 */
	public AiNode(boolean moveWhite) {
		super();
		/** 9 white pieces, 9 black pieces, white value (hex), black value (hex) **/
		this.state = "000000000000000000";
		this.moveWhite = moveWhite;
		children = new LinkedList<AiNode>();
		counter++;
	}

	/**
	 * @param state
	 * @param parent
	 * @param lastMoveWhite
	 */
	public AiNode(String state, boolean moveWhite) {
		super();
		this.state = state;
		this.moveWhite = moveWhite;
		children = new LinkedList<AiNode>();
		counter++;
	}

	public void copySchemeFromParent() {
		if (parent == null)
			return;

		this.state = parent.state;
	}

	public void evaluate() {
		int val = 0;
		//Il giocatore vince
		if (getBlackPiecesKilled() > 6)
			val = EvalSettings.bestScore;
		//L'avversario vince
		else if (getWhitePiecesKilled() > 6)
			val = EvalSettings.worstScore;
		else {
			//Pedine nemiche eliminate
			val += getBlackPiecesKilled() * EvalSettings.capturedPiece;
			//Pedine personali eliminate
			val += getWhitePiecesKilled() * EvalSettings.lostPiece;

			//Numero di tris personali creati
			val += getNumberOfWhiteTris() * EvalSettings.millFormed;
			//Numero di tris avversari creati
			val += getNumberOfBlackTris() * EvalSettings.millOpponent;
			//Distanza dal prossimo tris formabile
			val += whiteDepthFromTris * EvalSettings.millFormable;

			//Tris appena creato
			if (justDoneATris)
				val += EvalSettings.justTris;

			//Pezzi bloccati
			boolean blocked = true;
			char current;
			for (byte i = 9; i < 18; i++) {
				current = state.charAt(i);
				char[] result = null;
				blocked = true;
				if (current != '1' && current != '0') {
					if (getBlackPiecesInBase() == 0
							&& getBlackPiecesInGame() == 3)
						result = getAllPossibilities();
					else
						result = getPossibilities(current);

					for (byte j = 0; j < result.length; j++) {
						if (!isOccupied(result[j]))
							blocked = false;
					}
				}
				if (blocked)
					val += EvalSettings.blockedOpponentSpot;
			}

		}

		whiteValue = val;
		// evaluateFromChildren();
		// System.out.println("whiteValue: " + whiteValue);
	}

	public void evaluateFromChildren() {
		int best = 0;
		if (moveWhite) {
			best = EvalSettings.worstScore;
			for (AiNode node : children) {
				if (node.getWhiteValue() > best)
					best = node.getWhiteValue();
			}
		} else {
			best = EvalSettings.bestScore;
			for (AiNode node : children) {
				if (node.getWhiteValue() < best)
					best = node.getWhiteValue();
			}
		}
		whiteValue = best;
	}

	public void fillChildren(int depth) {
		// if terminal
		if (depth <= 0 || (getBlackPiecesKilled() > 6)
				|| (getWhitePiecesKilled() > 6)) {

			evaluate();
			// if (parent != null)
			// parent.evaluateFromChildren();
			return;
		}

		// Generating children for second phase
		if (children.size() == 0) {

			if (justDoneATris) {

				if (moveWhite) {

					// if (parent.getBlackPiecesInGame() == 4
					// && parent.getBlackPiecesInBase() == 0) {
					// // setWhiteValue((short) 999);
					// // return;
					// }
					for (byte i = 9; i < 18; i++) {
						char current = state.charAt(i);
						if (current != '0' && current != '1') {
							if (!isInAMill(current) || areAllBlackInAMill()) {
								AiNode aiNode = new AiNode(this, false);
								children.add(aiNode);
								aiNode.move(current, '1');
								aiNode.fillChildren(depth - 1);
							}
						}
					}
				} else {
					// if (parent.getWhitePiecesInGame() == 4
					// && parent.getWhitePiecesInBase() == 0) {
					// // setBlackValue((short) 999);
					// // return;
					// }
					for (byte i = 0; i < 9; i++) {
						char current = state.charAt(i);
						if (current != '0' && current != '1') {
							if (!isInAMill(current) || areAllWhiteInAMill()) {
								AiNode aiNode = new AiNode(this, true);
								children.add(aiNode);
								aiNode.move(current, '1');
								aiNode.fillChildren(depth - 1);
							}
						}
					}
				}
			} else {
				if (moveWhite) {
					// First phase
					if (getWhitePiecesInBase() > 0) {
						searchInBase: for (byte i = 0; i < 9; i++) {
							char current = state.charAt(i);
							if (current == '0') {
								char[] result = getAllPossibilities();

								for (byte j = 0; j < result.length; j++) {
									if (!isOccupied(result[j])) {
										AiNode aiNode = new AiNode(this, false);
										children.add(aiNode);
										aiNode.move(current, result[j]);
										if (getNumberOfWhiteTris() < aiNode
												.getNumberOfWhiteTris()) {
											if (areAllBlackInAMill()
													&& enableToKillEveryoneIfInTris
													|| !areAllBlackInAMill()) {
												aiNode.justDoneATris = true;
												aiNode.moveWhite = true;
											}
										}
										aiNode.fillChildren(depth - 1);
									}
								}
								break searchInBase;
							}
						}
					} else {

						for (byte i = 0; i < 9; i++) {
							char current = state.charAt(i);
							if (current != '1') {
								char[] result = null;
								if (getWhitePiecesInBase() == 0
										&& getWhitePiecesInGame() == 3)
									result = getAllPossibilities();
								else
									result = getPossibilities(current);

								for (byte j = 0; j < result.length; j++) {
									if (!isOccupied(result[j])) {
										AiNode aiNode = new AiNode(this, false);
										children.add(aiNode);
										aiNode.move(current, result[j]);
										if (getNumberOfWhiteTris() < aiNode
												.getNumberOfWhiteTris()) {
											if (areAllBlackInAMill()
													&& enableToKillEveryoneIfInTris
													|| !areAllBlackInAMill()) {
												aiNode.justDoneATris = true;
												aiNode.moveWhite = true;
											}
										}
										aiNode.fillChildren(depth - 1);
									}
								}
							}
						}
					}

				} else {
					// First phase
					if (getBlackPiecesInBase() > 0) {
						searchInBase: for (byte i = 9; i < 18; i++) {
							char current = state.charAt(i);
							if (current == '0') {
								char[] result = getAllPossibilities();

								for (byte j = 0; j < result.length; j++) {
									if (!isOccupied(result[j])) {
										AiNode aiNode = new AiNode(this, true);
										children.add(aiNode);
										aiNode.moveBlack(current, result[j]);
										if (getNumberOfBlackTris() < aiNode
												.getNumberOfBlackTris()) {
											if (areAllWhiteInAMill()
													&& enableToKillEveryoneIfInTris
													|| !areAllWhiteInAMill()) {
												aiNode.justDoneATris = true;
												aiNode.moveWhite = false;
											}
										}
										aiNode.fillChildren(depth - 1);
									}
								}
								break searchInBase;
							}
						}
					} else {
						for (byte i = 9; i < 18; i++) {
							char current = state.charAt(i);
							if (current != '1') {
								char[] result = null;
								if (getBlackPiecesInBase() == 0
										&& getBlackPiecesInGame() == 3)
									result = getAllPossibilities();
								else
									result = getPossibilities(current);
								for (byte j = 0; j < result.length; j++) {
									if (!isOccupied(result[j])) {
										AiNode aiNode = new AiNode(this, true);
										children.add(aiNode);
										aiNode.move(current, result[j]);
										if (getNumberOfBlackTris() < aiNode
												.getNumberOfBlackTris()) {
											if (areAllWhiteInAMill()
													&& enableToKillEveryoneIfInTris
													|| !areAllWhiteInAMill()) {
												aiNode.justDoneATris = true;
												aiNode.moveWhite = false;
											}
										}
										aiNode.fillChildren(depth - 1);
									}
								}
							}
						}
					}
				}
			}
		} else {
			for (AiNode aiNode : children)
				aiNode.fillChildren(depth - 1);
		}

		whiteDepthFromTris = (byte) 90;
		blackDepthFromTris = (byte) 90;
		for (AiNode aiNode : children) {
			if (aiNode.getWhiteDepthFromTris() < whiteDepthFromTris)
				whiteDepthFromTris = aiNode.getWhiteDepthFromTris();

			if (aiNode.getBlackDepthFromTris() < blackDepthFromTris)
				blackDepthFromTris = aiNode.getBlackDepthFromTris();

		}
		if (justDoneATris)
			if (moveWhite)
				whiteDepthFromTris = 0;
			else
				blackDepthFromTris = 0;
		else {
			whiteDepthFromTris++;
			blackDepthFromTris++;
		}
		if (parent != null)
			parent.fillChildren(1);

		evaluate();

	}

	public AiNode getAtPosition(int x, int y) {
		// if(!visible)
		// return null;

		if (this.contains(x, y))
			return this;

		for (AiNode aiNode : children) {
			if (aiNode.visible) {
				AiNode dum = aiNode.getAtPosition(x, y);
				if (dum != null)
					return dum;
			}
		}

		return null;
	}

	private static char[][] getMillPossibilities() {
		char[][] result = new char[][] { { 'a', 'b', 'c' }, { 'c', 'd', 'e' },
				{ 'e', 'f', 'g' }, { 'g', 'h', 'a' },

				{ 'i', 'j', 'k' }, { 'k', 'l', 'm' }, { 'm', 'n', 'o' },
				{ 'o', 'p', 'i' },

				{ 'q', 'r', 's' }, { 's', 't', 'u' }, { 'u', 'v', 'w' },
				{ 'w', 'y', 'q' },

				{ 'b', 'j', 'r' }, { 'd', 'l', 't' }, { 'f', 'n', 'u' },
				{ 'h', 'p', 'y' }

		};

		return result;
	}

	private static char[][] getMillPossibilities(char c) {
		char[][] result = new char[3][3];
		char[][] allPossibilities = getMillPossibilities();
		byte cnt = 0;
		for (byte i = 0; i < allPossibilities.length; i++) {
			if (allPossibilities[i][0] == c || allPossibilities[i][1] == c
					|| allPossibilities[i][2] == c) {
				result[cnt] = allPossibilities[i];
				cnt++;
			}
		}

		return result;
	}

	private boolean isInAMill(char c) {
		boolean whiteMill = false;
		for (byte i = 0; i < 9; i++)
			if (state.charAt(i) == c)
				whiteMill = true;

		char[][] millPossibilities = getMillPossibilities(c);

		for (byte i = 0; i < millPossibilities.length; i++) {
			if (whiteMill) {
				if (isOccupiedByWhite(millPossibilities[i][0])
						&& isOccupiedByWhite(millPossibilities[i][1])
						&& isOccupiedByWhite(millPossibilities[i][2]))
					return true;
			} else {
				if (isOccupiedByBlack(millPossibilities[i][0])
						&& isOccupiedByBlack(millPossibilities[i][1])
						&& isOccupiedByBlack(millPossibilities[i][2]))
					return true;
			}
		}

		return false;
	}

	private boolean areAllWhiteInAMill() {
		for (byte i = 0; i < 9; i++) {
			char current = state.charAt(i);
			if (!isInAMill(current))
				return false;
		}
		return true;
	}

	private boolean areAllBlackInAMill() {
		for (byte i = 9; i < 18; i++) {
			char current = state.charAt(i);
			if (!isInAMill(current))
				return false;
		}
		return true;
	}

	private boolean contains(int x, int y) {
		if (this.x - this.width / 2 <= x && this.x + this.width / 2 >= x
				&& this.y - this.height / 2 < y && this.y + this.height / 2 > y)
			return true;
		return false;
	}

	public boolean isOccupied(char c) {
		for (byte i = 0; i < 18; i++)
			if (state.charAt(i) == c)
				return true;
		return false;
	}

	public boolean isOccupiedByWhite(char c) {
		for (byte i = 0; i < 9; i++)
			if (state.charAt(i) == c)
				return true;
		return false;
	}

	public boolean isOccupiedByBlack(char c) {
		for (byte i = 9; i < 18; i++)
			if (state.charAt(i) == c)
				return true;
		return false;
	}

	public byte getNumberOfWhiteTris() {
		byte cnt = 0;
		// External
		if (isOccupiedByWhite('a') && isOccupiedByWhite('b')
				&& isOccupiedByWhite('c'))
			cnt++;
		if (isOccupiedByWhite('c') && isOccupiedByWhite('d')
				&& isOccupiedByWhite('e'))
			cnt++;
		if (isOccupiedByWhite('g') && isOccupiedByWhite('f')
				&& isOccupiedByWhite('e'))
			cnt++;
		if (isOccupiedByWhite('a') && isOccupiedByWhite('h')
				&& isOccupiedByWhite('g'))
			cnt++;

		// Middle
		if (isOccupiedByWhite('i') && isOccupiedByWhite('j')
				&& isOccupiedByWhite('k'))
			cnt++;
		if (isOccupiedByWhite('k') && isOccupiedByWhite('l')
				&& isOccupiedByWhite('m'))
			cnt++;
		if (isOccupiedByWhite('m') && isOccupiedByWhite('n')
				&& isOccupiedByWhite('o'))
			cnt++;
		if (isOccupiedByWhite('i') && isOccupiedByWhite('p')
				&& isOccupiedByWhite('o'))
			cnt++;

		// Internal
		if (isOccupiedByWhite('q') && isOccupiedByWhite('r')
				&& isOccupiedByWhite('s'))
			cnt++;
		if (isOccupiedByWhite('s') && isOccupiedByWhite('t')
				&& isOccupiedByWhite('u'))
			cnt++;
		if (isOccupiedByWhite('u') && isOccupiedByWhite('v')
				&& isOccupiedByWhite('w'))
			cnt++;
		if (isOccupiedByWhite('w') && isOccupiedByWhite('y')
				&& isOccupiedByWhite('q'))
			cnt++;

		//
		if (isOccupiedByWhite('b') && isOccupiedByWhite('j')
				&& isOccupiedByWhite('r'))
			cnt++;
		if (isOccupiedByWhite('d') && isOccupiedByWhite('l')
				&& isOccupiedByWhite('t'))
			cnt++;
		if (isOccupiedByWhite('f') && isOccupiedByWhite('n')
				&& isOccupiedByWhite('v'))
			cnt++;
		if (isOccupiedByWhite('h') && isOccupiedByWhite('p')
				&& isOccupiedByWhite('y'))
			cnt++;

		return cnt;
	}

	public byte getNumberOfBlackTris() {
		byte cnt = 0;
		// External
		if (isOccupiedByBlack('a') && isOccupiedByBlack('b')
				&& isOccupiedByBlack('c'))
			cnt++;
		if (isOccupiedByBlack('c') && isOccupiedByBlack('d')
				&& isOccupiedByBlack('e'))
			cnt++;
		if (isOccupiedByBlack('g') && isOccupiedByBlack('f')
				&& isOccupiedByBlack('e'))
			cnt++;
		if (isOccupiedByBlack('a') && isOccupiedByBlack('h')
				&& isOccupiedByBlack('g'))
			cnt++;

		// Middle
		if (isOccupiedByBlack('i') && isOccupiedByBlack('j')
				&& isOccupiedByBlack('k'))
			cnt++;
		if (isOccupiedByBlack('k') && isOccupiedByBlack('l')
				&& isOccupiedByBlack('m'))
			cnt++;
		if (isOccupiedByBlack('m') && isOccupiedByBlack('n')
				&& isOccupiedByBlack('o'))
			cnt++;
		if (isOccupiedByBlack('i') && isOccupiedByBlack('p')
				&& isOccupiedByBlack('o'))
			cnt++;

		// Internal
		if (isOccupiedByBlack('q') && isOccupiedByBlack('r')
				&& isOccupiedByBlack('s'))
			cnt++;
		if (isOccupiedByBlack('s') && isOccupiedByBlack('t')
				&& isOccupiedByBlack('u'))
			cnt++;
		if (isOccupiedByBlack('u') && isOccupiedByBlack('v')
				&& isOccupiedByBlack('w'))
			cnt++;
		if (isOccupiedByBlack('w') && isOccupiedByBlack('y')
				&& isOccupiedByBlack('q'))
			cnt++;

		//
		if (isOccupiedByBlack('b') && isOccupiedByBlack('j')
				&& isOccupiedByBlack('r'))
			cnt++;
		if (isOccupiedByBlack('d') && isOccupiedByBlack('l')
				&& isOccupiedByBlack('t'))
			cnt++;
		if (isOccupiedByBlack('f') && isOccupiedByBlack('n')
				&& isOccupiedByBlack('v'))
			cnt++;
		if (isOccupiedByBlack('h') && isOccupiedByBlack('p')
				&& isOccupiedByBlack('y'))
			cnt++;

		return cnt;
	}

	public byte getWhitePiecesInBase() {
		byte cnt = 0;
		for (byte i = 0; i < 9; i++)
			if (state.charAt(i) == '0')
				cnt++;

		return cnt;
	}

	public byte getWhitePiecesInGame() {
		byte cnt = 0;
		for (byte i = 0; i < 9; i++)
			if (state.charAt(i) != '0' && state.charAt(i) != '1')
				cnt++;

		return cnt;
	}

	public byte getBlackPiecesInGame() {
		byte cnt = 0;
		for (byte i = 9; i < 18; i++)
			if (state.charAt(i) != '0' && state.charAt(i) != '1')
				cnt++;

		return cnt;
	}

	public byte getBlackPiecesInBase() {
		byte cnt = 0;
		for (byte i = 9; i < 18; i++)
			if (state.charAt(i) == '0')
				cnt++;

		return cnt;
	}

	public byte getWhitePiecesKilled() {
		byte cnt = 0;
		for (byte i = 0; i < 9; i++)
			if (state.charAt(i) == '1')
				cnt++;

		return cnt;
	}

	public byte getBlackPiecesKilled() {
		byte cnt = 0;
		for (byte i = 9; i < 18; i++)
			if (state.charAt(i) == '1')
				cnt++;

		return cnt;
	}

	public void move(char from, char to) {
		for (byte i = 0; i < 18; i++) {
			if (state.charAt(i) == from) {
				state = replace(state, i, to);
				return;
			}
		}
	}

	public void moveWhite(char from, char to) {
		for (byte i = 0; i < 9; i++) {
			if (state.charAt(i) == from) {
				state = replace(state, i, to);
				return;
			}
		}
	}

	public void moveBlack(char from, char to) {
		for (byte i = 9; i < 18; i++) {
			if (state.charAt(i) == from) {
				state = replace(state, i, to);
				return;
			}
		}
	}

	/*
	 * From stack overflow
	 * http://stackoverflow.com/questions/6952363/java-replace
	 * -a-character-at-a-specific-index-in-a-string
	 */
	private static String replace(String str, int index, char replace) {
		if (str == null) {
			return str;
		} else if (index < 0 || index >= str.length()) {
			return str;
		}
		char[] chars = str.toCharArray();
		chars[index] = replace;
		return String.valueOf(chars);
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the parent
	 */
	public AiNode getParent() {
		return parent;
	}

	/**
	 * @param parent
	 *            the parent to set
	 */
	public void setParent(AiNode parent) {
		this.parent = parent;
	}

	/**
	 * @return the children
	 */
	public List<AiNode> getChildren() {
		return children;
	}

	/**
	 * @param children
	 *            the children to set
	 */
	public void setChildren(List<AiNode> children) {
		this.children = children;
	}

	/**
	 * @return the whiteValue
	 */
	public int getWhiteValue() {
		return whiteValue;
	}

	/**
	 * @param whiteValue
	 *            the whiteValue to set
	 */
	public void setWhiteValue(int whiteValue) {
		this.whiteValue = whiteValue;
	}

	/**
	 * @return the blackValue
	 */
	public int getBlackValue() {
		return blackValue;
	}

	/**
	 * @param blackValue
	 *            the blackValue to set
	 */
	public void setBlackValue(int blackValue) {
		this.blackValue = blackValue;
	}

	/**
	 * @return the lastMoveWhite
	 */
	public boolean isMoveWhite() {
		return moveWhite;
	}

	/**
	 * @param lastMoveWhite
	 *            the lastMoveWhite to set
	 */
	public void setMoveWhite(boolean lastMoveWhite) {
		this.moveWhite = lastMoveWhite;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x
	 *            the x to set
	 */
	public void setX(int x) {
		this.x = x;
		int childrenSize = children.size();
		int cnt = 0;
		int totalSchemeWidth = width + 140;
		for (AiNode aiNode : children) {
			if (aiNode.isVisible()) {
				aiNode.setX(this.x + totalSchemeWidth / 2 - (childrenSize / 2)
						* totalSchemeWidth + cnt * totalSchemeWidth);
				// node.setWidth(width);
				// node.setHeight(height);
				// s.setY(y+height*3/2);
				cnt++;
			}
		}
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y
	 *            the y to set
	 */
	public void setY(int y) {
		this.y = y;
		for (AiNode aiNode : children) {
			if (aiNode.isVisible()) {
				aiNode.setY(y + height * 3 / 2);
			}
		}
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width
	 *            the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
		for (AiNode aiNode : children) {
			if (aiNode.isVisible())
				aiNode.setWidth(width);
		}
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param height
	 *            the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
		for (AiNode aiNode : children) {
			if (aiNode.isVisible())
				aiNode.setHeight(height);
		}
	}

	/**
	 * @return the whiteDepthFromTris
	 */
	public byte getWhiteDepthFromTris() {
		return whiteDepthFromTris;
	}

	/**
	 * @param whiteDepthFromTris
	 *            the whiteDepthFromTris to set
	 */
	public void setWhiteDepthFromTris(byte whiteDepthFromTris) {
		this.whiteDepthFromTris = whiteDepthFromTris;
	}

	/**
	 * @return the blackDepthFromTris
	 */
	public byte getBlackDepthFromTris() {
		return blackDepthFromTris;
	}

	/**
	 * @param blackDepthFromTris
	 *            the blackDepthFromTris to set
	 */
	public void setBlackDepthFromTris(byte blackDepthFromTris) {
		this.blackDepthFromTris = blackDepthFromTris;
	}

	public static char[] getAllPossibilities() {
		return new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
				'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
				'w', 'y' };
	}

	public static char[] getPossibilities(char c) {
		switch (c) {
		case 'a':
			return new char[] { 'b', 'h' };

		case 'b':

			return new char[] { 'a', 'c', 'j' };

		case 'c':

			return new char[] { 'b', 'd' };

		case 'd':

			return new char[] { 'c', 'e', 'l' };

		case 'e':

			return new char[] { 'd', 'f' };

		case 'f':

			return new char[] { 'e', 'g', 'n' };

		case 'g':

			return new char[] { 'h', 'f' };

		case 'h':

			return new char[] { 'a', 'g', 'p' };

		case 'i':

			return new char[] { 'j', 'p' };

		case 'j':

			return new char[] { 'b', 'k', 'i', 'r' };

		case 'k':

			return new char[] { 'j', 'l' };

		case 'l':

			return new char[] { 'd', 'k', 'm', 't' };

		case 'm':

			return new char[] { 'n', 'l' };

		case 'n':

			return new char[] { 'f', 'o', 'm', 'v' };

		case 'o':

			return new char[] { 'n', 'p' };

		case 'p':

			return new char[] { 'i', 'o', 'h', 'y' };

		case 'q':

			return new char[] { 'r', 'y' };

		case 'r':

			return new char[] { 'j', 's', 'q' };

		case 's':

			return new char[] { 'r', 't' };

		case 't':

			return new char[] { 's', 'l', 'u' };

		case 'u':

			return new char[] { 't', 'v' };

		case 'v':

			return new char[] { 'u', 'n', 'w' };

		case 'w':

			return new char[] { 'y', 'v' };

		case 'y':

			return new char[] { 'q', 'w', 'p' };

		}
		return new char[] {};
	}

	public static int getJ(char c) {
		if (c == 'a' || c == 'h' || c == 'g')
			return 0;
		if (c == 'i' || c == 'p' || c == 'o')
			return 1;
		if (c == 'q' || c == 'y' || c == 'w')
			return 2;
		if (c == 'b' || c == 'j' || c == 'r' || c == 'v' || c == 'n'
				|| c == 'f')
			return 3;
		if (c == 's' || c == 't' || c == 'u')
			return 4;
		if (c == 'k' || c == 'l' || c == 'm')
			return 5;
		if (c == 'c' || c == 'd' || c == 'e')
			return 6;
		return -1;
	}

	public static int getI(char c) {
		if (c == 'a' || c == 'b' || c == 'c')
			return 0;
		if (c == 'i' || c == 'j' || c == 'k')
			return 1;
		if (c == 'q' || c == 'r' || c == 's')
			return 2;
		if (c == 'h' || c == 'p' || c == 'y' || c == 't' || c == 'l'
				|| c == 'd')
			return 3;
		if (c == 'w' || c == 'v' || c == 'u')
			return 4;
		if (c == 'o' || c == 'n' || c == 'm')
			return 5;
		if (c == 'g' || c == 'f' || c == 'e')
			return 6;
		return -1;
	}

	public void setChildrenVisible(boolean visible) {
		for (AiNode n : children)
			n.setVisible(visible);
		setX(x);
		setY(y);
		setWidth(width);
		setHeight(height);
	}

	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible
	 *            the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
		if (!visible) {
			setChildrenVisible(false);
		}
	}

	/**
	 * @return the counter
	 */
	public static int getCounter() {
		return counter;
	}

	/**
	 * @param counter
	 *            the counter to set
	 */
	public static void setCounter(int counter) {
		AiNode.counter = counter;
	}

	/**
	 * @return the justDoneATris
	 */
	public boolean isJustDoneATris() {
		return justDoneATris;
	}

	/**
	 * @param justDoneATris
	 *            the justDoneATris to set
	 */
	public void setJustDoneATris(boolean justDoneATris) {
		this.justDoneATris = justDoneATris;
	}

	public AiNode getBestNode() {
		if (children.size() == 0)
			return null;

		AiNode bestNode = children.get(0);
		if (moveWhite) {
			Gdx.app.debug("best", "white");
			for (AiNode node : children) {
				if (bestNode.whiteValue > node.whiteValue)
					bestNode = node;
			}
		} else {
			Gdx.app.debug("best", "black");
			for (AiNode node : children) {
				if (bestNode.whiteValue < node.whiteValue)
					bestNode = node;
			}
		}
		return bestNode;
	}

	public static AiNode getNodeFromSchema(Schema schema) {
		AiNode result = new AiNode(true);
		String state = "000000000000000000";
		int cnt = 0;
		int whiteCnt = 0;
		int blackCnt = 9;
		for (Node node : schema.getNodes()) {
			if (node.getPiece() != null) {
				if (node.getPiece().getOpponent().getOpponentColor() == OpponentColor.WHITE) {
					state = replace(state, whiteCnt, getCharAtIndex(node.id));
					whiteCnt++;
				} else {
					state = replace(state, blackCnt, getCharAtIndex(node.id));
					blackCnt++;
				}
			}
			cnt++;
		}
		whiteCnt = schema.getWhitePieces().size();
		blackCnt = schema.getBlackPieces().size() + 9;

		while (whiteCnt < 9) {
			state = replace(state, whiteCnt, '1');
			whiteCnt++;
		}
		while (blackCnt < 18) {
			state = replace(state, blackCnt, '1');
			blackCnt++;
		}
		result.state = state;
		return result;
	}

	public static char getCharAtIndex(int index) {
		String all = "abcdefghijklmnopqrstuvwy";
		return all.charAt(index);
	}

	public static int getIndexAtChar(char c) {
		String all = "abcdefghijklmnopqrstuvwy";
		return all.indexOf(c);
	}

}
