package com.lpi.ninemensmorris.game;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.lpi.ninemensmorris.Utility.Direction;
import com.lpi.ninemensmorris.Utility.Orientation;

/**
 * @author Sandro Panighetti - 16-gen-2013 NineMensMorris
 */
public abstract class Opponent {

	public static Opponent getNewOpponent(World world,
			OpponentType opponentType, OpponentColor opponentColor) {
		switch (opponentType) {
		case HUMAN:
			return new HumanAutomaticOpponent(world, opponentColor);
		case COMPUTER:
			return new IntelligentAutomaticOpponent(world, opponentColor);
		}
		return null;
	}

	public enum OpponentType {
		HUMAN, COMPUTER;
	}

	public enum OpponentColor {
		WHITE, BLACK;
	}

	public enum OpponentState {
		WAITING_FOR_OTHER_PLAYER, PLAYING;
	}

	public enum OpponentPhase {
		FIRST, SECOND, THIRD;
	}

	protected OpponentColor opponentColor;
	protected OpponentState opponentState;
	protected OpponentPhase opponentPhase;

	protected boolean toKill = false;
	public int piecesToPlace = 9;
	protected int piecesInGame = 0;

	protected float killingSpeed = 50.0f;

	protected World world;

	protected Movement movement = null;

	/**
	 * @param opponentColor
	 */
	public Opponent(World world, OpponentColor opponentColor) {
		super();
		this.world = world;
		this.opponentColor = opponentColor;
		this.opponentPhase = OpponentPhase.FIRST;
	}

	public boolean enableToKill(Piece piece) {
		if (piece.getOpponent() == this)
			return false;

		Node node = piece.getNode();
		if (null == node) {
			// Gdx.app.debug("node", "=null");
			return false;
		}

		boolean allTris = true;
		for (Piece p : world.getScheme().getPiecesOfOpponent(
				world.getReverseOpponent(this))) {
			if (null != p.getNode()) {
				if (countMaxNeighbours(p.getNode(), p.getOpponent()) < 2) {
					allTris = false;
					break;
				}
			}
		}
		if (allTris)
			if (world.getGameScreen().getNineMensMorris().getSettings()
					.isAllTrisKillEnabled())
				return true;
			else
				return false;

		// If in TRIS
		if (countMaxNeighbours(node, piece.getOpponent()) == 2)
			return false;

		// if (countNeighbours(node, piece.getOpponent(), 0)
		// + countNeighbours(node, piece.getOpponent(), 2) == 2
		// || countNeighbours(node, piece.getOpponent(), 1)
		// + countNeighbours(node, piece.getOpponent(), 3) == 2)
		// return false;

		return true;
	}

	/**
	 * @param opponent
	 *            of the mill
	 * @return
	 */
	public boolean enableToKill(Opponent opponent) {

		for (Piece p : world.getScheme().getPiecesOfOpponent(
				world.getReverseOpponent(opponent)))
			if (null != p.getNode()) {
				if (enableToKill(p))
					return true;
				if (countMaxNeighbours(p.getNode(), p.getOpponent()) < 2)
					return true;
			}

		return false;
	}

	protected void logNeighbours(Node node, Opponent opponent) {

		Gdx.app.debug("----------", "");
		Gdx.app.debug("countNeighbours, this, UP",
				countNeighbours(node, opponent, Direction.UP) + "");
		Gdx.app.debug("countNeighbours, this, RIGHT",
				countNeighbours(node, opponent, Direction.RIGHT) + "");
		Gdx.app.debug("countNeighbours, this, DOWN",
				countNeighbours(node, opponent, Direction.DOWN) + "");
		Gdx.app.debug("countNeighbours, this, LEFT",
				countNeighbours(node, opponent, Direction.LEFT) + "");
	}

	protected int countMaxNeighbours(Node node, Opponent opponent) {
		return Math.max(
				countNeighbours(node, opponent, Orientation.HORIZONTAL),
				countNeighbours(node, opponent, Orientation.VERTICAL));
	}

	protected int countNeighbours(Node node, Opponent opponent,
			Orientation orientation) {
		return orientation == Orientation.VERTICAL ? countNeighbours(node,
				opponent, Direction.UP)
				+ countNeighbours(node, opponent, Direction.DOWN)
				: countNeighbours(node, opponent, Direction.RIGHT)
						+ countNeighbours(node, opponent, Direction.LEFT);

	}

	protected int countAllNeighbours(Node node) {
		return countNeighbours(node, this, Orientation.HORIZONTAL)
				+ countNeighbours(node, this, Orientation.VERTICAL)
				+ countNeighbours(node, world.getReverseOpponent(this),
						Orientation.HORIZONTAL)
				+ countNeighbours(node, world.getReverseOpponent(this),
						Orientation.VERTICAL);
	}

	protected int countNeighbours(Node node, Opponent opponent,
			Direction direction) {
		return null != node.getPiece() ? countNeighbours(node, opponent, -1,
				direction) : countNeighbours(node, opponent, 0, direction);
		// int cnt = countNeighbours(node, opponent, -1, direction);
		// return countNeighbours(node, opponent, -1, direction);
	}

	protected int countNeighbours(Node node, Direction direction) {
		return countNeighbours(node, world.getOpponentA(), direction)
				+ countNeighbours(node, world.getOpponentB(), direction);
	}

	protected int countNeighbours(Node node, Opponent opponent, int cnt,
			Direction direction) {

		if (null != node.getPiece())
			if (node.getPiece().getOpponent() == opponent)
				cnt++;

		if (Direction.UP == direction) {
			if (null != node.getUp())
				cnt = countNeighbours(node.getUp(), opponent, cnt, direction);
		} else if (Direction.RIGHT == direction) {
			if (null != node.getRight())
				cnt = countNeighbours(node.getRight(), opponent, cnt, direction);
		} else if (Direction.DOWN == direction) {
			if (null != node.getDown())
				cnt = countNeighbours(node.getDown(), opponent, cnt, direction);
		} else if (Direction.LEFT == direction) {
			if (null != node.getLeft())
				cnt = countNeighbours(node.getLeft(), opponent, cnt, direction);
		}

		return cnt;
	}

	protected boolean enableToPlace(Piece piece, Node node) {

		if (null != node.getPiece())
			return false;

		switch (opponentPhase) {
		case FIRST:
			return true;
		case SECOND:

			if (!isNear(piece, node))
				return false;
			return true;
		case THIRD:
			return true;
		}

		return false;

	}

	private boolean isNear(Piece piece, Node node) {
		if (null == piece.getNode())
			return false;

		if (null != node.getUp())
			if (piece.getNode() == node.getUp())
				return true;

		if (null != node.getRight())
			if (piece.getNode() == node.getRight())
				return true;

		if (null != node.getDown())
			if (piece.getNode() == node.getDown())
				return true;

		if (null != node.getLeft())
			if (piece.getNode() == node.getLeft())
				return true;

		return false;
	}

	protected boolean isFreeNearAPice(Piece piece) {
		return isFreeNearAPiece(piece, Direction.UP)
				|| isFreeNearAPiece(piece, Direction.RIGHT)
				|| isFreeNearAPiece(piece, Direction.DOWN)
				|| isFreeNearAPiece(piece, Direction.LEFT);
	}

	protected boolean isFreeNearAPiece(Piece piece, Direction direction) {
		if (null == piece.getNode())
			return false;

		if (null != piece.getNode().getNode(direction))
			if (null == piece.getNode().getNode(direction).getPiece())
				return true;

		return false;
	}

	public boolean enableToMove() {

		for (Piece p : world.getScheme().getPiecesOfOpponent(this))
			if (enableToMovePiece(p))
				return true;

		return false;
	}

	protected boolean enableToMovePiece(Piece piece) {
		switch (opponentPhase) {
		case FIRST:
			// Piece - Opponent
			if (this != piece.getOpponent())
				return false;

			if (null != piece.getNode())
				return false;

			return true;
		case SECOND:

			// Piece - Opponent
			if (this != piece.getOpponent())
				return false;

			if (null == piece.getNode())
				return false;

			if (!isFreeNearAPice(piece))
				return false;

			return true;
		case THIRD:
			// Piece - Opponent
			if (this != piece.getOpponent())
				return false;

			if (null == piece.getNode())
				return false;

			return true;
		}

		return false;
	}

	protected List<Node> getEmptyNodesNearAMill() {

		List<Node> problematicNodes = new LinkedList<Node>();
		for (Node n : world.getScheme().getNodes()) {
			if (null == n.getPiece())
				if (countMaxNeighbours(n, world.getReverseOpponent(this)) > 1){
//					Gdx.app.debug("adding","true");
					problematicNodes.add(n);
				}
		}

		return problematicNodes;
	}

	protected List<Node> getNodesNearAMill() {

		List<Node> nodes = new LinkedList<Node>();
		for (Node n : world.getScheme().getNodes()) {
			if (null == n.getPiece())
				if (countMaxNeighbours(n, this) > 1)
					nodes.add(n);
		}

		return nodes;
	}

	/**
	 * @return the opponentColor
	 */
	public OpponentColor getOpponentColor() {
		return opponentColor;
	}

	/**
	 * @param opponentColor
	 *            the opponentColor to set
	 */
	public void setOpponentColor(OpponentColor opponentColor) {
		this.opponentColor = opponentColor;
	}

	public abstract void update(float delta);

	/**
	 * @return the opponentState
	 */
	public OpponentState getOpponentState() {
		return opponentState;
	}

	/**
	 * @param opponentState
	 *            the opponentState to set
	 */
	public void setOpponentState(OpponentState opponentState) {
		this.opponentState = opponentState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Opponent [opponentColor=" + opponentColor + ", opponentState="
				+ opponentState + "]";
	}

	/**
	 * @return the opponentPhase
	 */
	public OpponentPhase getOpponentPhase() {
		return opponentPhase;
	}

	/**
	 * @param opponentPhase
	 *            the opponentPhase to set
	 */
	public void setOpponentPhase(OpponentPhase opponentPhase) {
		this.opponentPhase = opponentPhase;
		// Gdx.app.debug("OpponentPhase", getOpponentColor().toString() + " : "
		// + getOpponentPhase().toString());
	}

	public void addAPieceInGame() {
		piecesInGame++;
		// Gdx.app.debug(opponentColor.toString() + " piecesInGame",
		// piecesInGame
		// + "");
	}

	public void removeAPieceInGame() {
		piecesInGame--;
		// Gdx.app.debug(opponentColor.toString() + " piecesInGame",
		// piecesInGame
		// + "");
	}

	public void removeAPieceToPlace() {
		piecesToPlace--;
		// Gdx.app.debug(opponentColor.toString() + " piecesToPlace",
		// piecesToPlace + "");
	}

	/**
	 * @return the piecesToPlace
	 */
	public int getPiecesToPlace() {
		return piecesToPlace;
	}

	/**
	 * @param piecesToPlace
	 *            the piecesToPlace to set
	 */
	public void setPiecesToPlace(int piecesToPlace) {
		this.piecesToPlace = piecesToPlace;
	}

	/**
	 * @return the piecesInGame
	 */
	public int getPiecesInGame() {
		return piecesInGame;
	}

	/**
	 * @param piecesInGame
	 *            the piecesInGame to set
	 */
	public void setPiecesInGame(int piecesInGame) {
		this.piecesInGame = piecesInGame;
	}

}
