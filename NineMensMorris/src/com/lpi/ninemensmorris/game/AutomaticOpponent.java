package com.lpi.ninemensmorris.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.lpi.ninemensmorris.Assets;
import com.lpi.ninemensmorris.game.ai.AiNode;
import com.lpi.ninemensmorris.screen.AbstractScreen;

public abstract class AutomaticOpponent extends Opponent {

	private Vector2 speed;
	protected Vector2 killVector;
	protected Rectangle killZone;

	protected float movingSpeed = 30.0f;

	public AutomaticOpponent(World world, OpponentColor opponentColor) {
		super(world, opponentColor);

		killingSpeed = 20.0f;

		piecesToPlace = 9;
		speed = null;
		killZone = new Rectangle(AbstractScreen.SCREEN_WIDTH / 2.0f - 200.0f,
				-AbstractScreen.SCREEN_HEIGHT / 2.0f + 200.0f, 400.0f, 400.0f);
	}

	private boolean enableToPlaySound = true;

	@Override
	public void update(float delta) {

		if (opponentState == OpponentState.WAITING_FOR_OTHER_PLAYER)
			return;

		if (null == movement) {

			switch (opponentPhase) {
			case FIRST:
				movement = firstPhase();
				break;
			case SECOND:
				movement = secondPhase();
				break;
			case THIRD:
				movement = thirdPhase();
				break;
			}

			if (null != movement) {
//				try {
					world.move(movement);

					prepareToMove();
//				} catch (Exception e) {
//				}
			}

		} else {

			if (toKill) {
				// Gdx.app.debug("toKill", "ok");

				movement.getPieceToKill().position.add(killVector);
				movement.getPieceToKill().setCoordinates();

				if (killZone.contains(movement.getPieceToKill().position.x,
						movement.getPieceToKill().position.y)) {
					world.move(movement);
					world.turnFinished(this);
					toKill = false;
					movement = null;
				}
				return;
			}

			movement.getPiece().position.add(speed);
			movement.getPiece().setCoordinates();

			if (movement.getNodeTo().bounds.contains(
					movement.getPiece().position.x,
					movement.getPiece().position.y)) {

				if (enableToPlaySound) {
					if (world.getGameScreen().getNineMensMorris().getSettings()
							.isSoundEnabled())
						Assets.tocSound.play();

					enableToPlaySound = false;
				}

				movement.getPiece().setCoordinates(
						movement.getNodeTo().position.x,
						movement.getNodeTo().position.y);
				movement.getPiece().setStartCoordinates();

				// ToKill
				if (!(countMaxNeighbours(movement.getNodeTo(), this) < 2 || !enableToKill(this))) {
					Piece pieceToKill = getPieceToKill();

					if (null != pieceToKill) {
						movement.setPieceToKill(pieceToKill);

						killVector = new Vector2(killZone.x + killZone.width
								/ 2.0f - pieceToKill.position.x, killZone.y
								- killZone.height / 2.0f
								- pieceToKill.position.y);
						killVector.div(killingSpeed);
						toKill = true;
						AiNode aiNode = AiNode.getNodeFromSchema(world
								.getScheme());
						// Gdx.app.debug("pieceToKill", "notnull");
					}
				} else {
					if (world.getGameScreen().getNineMensMorris().getSettings()
							.isSoundEnabled())
						Assets.tocSound.play(0.6f);
					world.turnFinished(this);
					enableToPlaySound = true;
					movement = null;
					AiNode aiNode = AiNode.getNodeFromSchema(world.getScheme());
					// Gdx.app.debug("aiNode", aiNode.getState());
				}
			}
			return;
		}
	}

	protected abstract Movement firstPhase();

	protected abstract Movement secondPhase();

	protected abstract Movement thirdPhase();

	private void prepareToMove() {
		speed = new Vector2(movement.getNodeTo().position.x
				- movement.getPiece().position.x,
				movement.getNodeTo().position.y
						- movement.getPiece().position.y);
		speed.div(movingSpeed);
	}

	protected abstract Piece getPieceToKill();

}
