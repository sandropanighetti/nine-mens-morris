package com.lpi.ninemensmorris.game;

import com.lpi.ninemensmorris.game.Opponent.OpponentType;

public class MatchInfo {

	private Opponent.OpponentType opponentWinner;
	private Opponent.OpponentColor opponentColorWinner;
	private int moves;
	private float time=0.0f;
	
	@SuppressWarnings("unused")
	private String schema="";
	private OpponentType opponentTypeA;
	private OpponentType opponentTypeB;
	
	/**
	 * 
	 */
	public MatchInfo() {
		super();
	}
	
	
	
	/**
	 * @param delta
	 */
	public void updateTime(float delta){
		time += delta;
	}

	/**
	 * @return the opponentWinner
	 */
	public Opponent.OpponentType getOpponentWinner() {
		return opponentWinner;
	}

	/**
	 * @param opponentWinner the opponentWinner to set
	 */
	public void setOpponentWinner(Opponent.OpponentType opponentWinner) {
		this.opponentWinner = opponentWinner;
	}

	/**
	 * @return the opponentColorWinner
	 */
	public Opponent.OpponentColor getOpponentColorWinner() {
		return opponentColorWinner;
	}

	/**
	 * @param opponentColorWinner the opponentColorWinner to set
	 */
	public void setOpponentColorWinner(Opponent.OpponentColor opponentColorWinner) {
		this.opponentColorWinner = opponentColorWinner;
	}

	/**
	 * @return the moves
	 */
	public int getMoves() {
		return moves;
	}

	/**
	 * @param moves the moves to set
	 */
	public void setMoves(int moves) {
		this.moves = moves;
	}



	/**
	 * @return the time
	 */
	public float getTime() {
		return time;
	}



	/**
	 * @param time the time to set
	 */
	public void setTime(float time) {
		this.time = time;
	}

	/**
	 * @return the schema
	 */
	public String getSchema() {
		return schema;
	}

	/**
	 * @param schema the schema to set
	 */
	public void setSchema(String schema) {
		this.schema = schema;
	}

	/**
	 * @return the opponentTypeA
	 */
	public OpponentType getOpponentTypeA() {
		return opponentTypeA;
	}

	/**
	 * @param opponentTypeA the opponentTypeA to set
	 */
	public void setOpponentTypeA(OpponentType opponentTypeA) {
		this.opponentTypeA = opponentTypeA;
	}

	/**
	 * @return the opponentTypeB
	 */
	public OpponentType getOpponentTypeB() {
		return opponentTypeB;
	}

	/**
	 * @param opponentTypeB the opponentTypeB to set
	 */
	public void setOpponentTypeB(OpponentType opponentTypeB) {
		this.opponentTypeB = opponentTypeB;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MatchInfo [opponentWinner=");
		builder.append(opponentWinner);
		builder.append(", opponentColorWinner=");
		builder.append(opponentColorWinner);
		builder.append(", moves=");
		builder.append(moves);
		builder.append(", time=");
		builder.append(time);
		builder.append(", schema=");
		builder.append(schema);
		builder.append(", opponentTypeA=");
		builder.append(opponentTypeA);
		builder.append(", opponentTypeB=");
		builder.append(opponentTypeB);
		builder.append("]");
		return builder.toString();
	}



	
	
}
