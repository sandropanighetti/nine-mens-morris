package com.lpi.ninemensmorris.game;

import java.util.Collections;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.lpi.ninemensmorris.Utility;
import com.lpi.ninemensmorris.game.ai.AiNode;

public class IntelligentAutomaticOpponent extends AutomaticOpponent {

	private static final int DEPTH = 4;

	/**
	 * @param world
	 * @param opponentColor
	 */
	public IntelligentAutomaticOpponent(World world, OpponentColor opponentColor) {
		super(world, opponentColor);
	}

	Movement dum = null;
	boolean started = false;

	@Override
	protected Movement firstPhase() {
		if (!started) {
			new Thread() {
				@Override
				public void run() {
					started = true;
					AiNode aiNode = AiNode.getNodeFromSchema(world.getScheme());
					// Gdx.app.debug("aiNode", aiNode.getState());

					Node node;
					Piece piece;

					List<Node> problematicNodes = getEmptyNodesNearAMill();
					List<Node> nodesNearAMill = getNodesNearAMill();
					// Gdx.app.debug("problematicNodes cnt",
					// problematicNodes.size() + "");
					// Gdx.app.debug("nodesNearAMill cnt", nodesNearAMill.size()
					// + "");

					if (nodesNearAMill.size() > 0) {
						node = nodesNearAMill.get(0);
					} else if (problematicNodes.size() > 0) {
						node = problematicNodes.get(0);
						// Gdx.app.debug("mill", "opened");
					} else {
						List<Node> sortedNodes = world.getScheme().getNodes();
						Collections.sort(sortedNodes);
						// Node bestNode = nodesNearAMill.get(0);
						// for (Node n : world.getScheme().getNodes()) {
						// if (null == n.getPiece()) {
						// if (n.getNumberOfNeighbour() > bestNode
						// .getNumberOfNeighbour())
						// bestNode = n;
						// }
						// }
						Node bestNode = null;
						mainloop: for (Node n : sortedNodes) {
							if (n.getPiece() == null) {
								bestNode = n;
								break mainloop;
							}
						}

						int bestNodeStepsFromOther;
						if (getOpponentColor() == OpponentColor.WHITE)
							bestNodeStepsFromOther = bestNode
									.getStepsFromAPiece(world.getOpponentA(),
											bestNode, 8);
						else
							bestNodeStepsFromOther = bestNode
									.getStepsFromAPiece(world.getOpponentB(),
											bestNode, 8);

						for (Node n : sortedNodes) {
							if (null == n.getPiece()) {
								int currentNodeStepsFromOther;
								if (getOpponentColor() == OpponentColor.WHITE)
									currentNodeStepsFromOther = n
											.getStepsFromAPiece(
													world.getOpponentA(), n, 8);
								else
									currentNodeStepsFromOther = n
											.getStepsFromAPiece(
													world.getOpponentA(), n, 8);

								if (currentNodeStepsFromOther > bestNodeStepsFromOther
										&& n.getNumberOfNeighbour() >= bestNode
												.getNumberOfNeighbour()) {
									bestNodeStepsFromOther = currentNodeStepsFromOther;
									bestNode = n;
								}
							}
						}

						node = bestNode;

						// do {
						// node = world
						// .getScheme()
						// .getNodes()
						// .get((int) (Math.random() * (double) world
						// .getScheme().getNodes().size()));
						// } while (null != node.getPiece());

					}
					if (getOpponentColor() == OpponentColor.WHITE)
						piece = world.getScheme().getWhitePieces()
								.getFirstPieceInBase();
					else
						piece = world.getScheme().getBlackPieces()
								.getFirstPieceInBase();
					dum = new Movement(null, node, piece);
					// Gdx.app.debug("piece", piece.getNode() + "");
					// dum = new Movement(null, nodeTo, piece);
				}
			}.start();
		} else if (dum != null) {
			started = false;
			Movement a = dum;
			dum = null;
			a.setOpponent(this);
			// dum.setOpponent(this);
			return a;
		}

		return null;
	}

	protected Movement secondPhase() {
		if (!started) {
			new Thread() {
				@Override
				public void run() {
					started = true;
					AiNode node = AiNode.getNodeFromSchema(world.getScheme());
					// Gdx.app.debug("node", node.getState());
					if (getOpponentColor() == OpponentColor.WHITE)
						node.setMoveWhite(true);
					else
						node.setMoveWhite(false);
					node.fillChildren(5);
					AiNode bestNode = node.getBestNode();
					// Gdx.app.debug("bestNode", bestNode.getState());

					char charNodeFrom = ' ';
					char charNodeTo = ' ';
					mainLoop: for (byte i = 0; i < node.getState().length(); i++) {
						if (node.getState().charAt(i) != bestNode.getState()
								.charAt(i)) {
							if (node.getState().charAt(i) != '0') {
								charNodeFrom = node.getState().charAt(i);
								charNodeTo = bestNode.getState().charAt(i);
								break mainLoop;
							}
						}
					}
					// Gdx.app.debug("charFrom", charNodeFrom + "");
					// Gdx.app.debug("charNodeTo", charNodeTo + "");

					Node nodeFrom = null;
					// = world.getScheme().getNodes()
					// .get(AiNode.getIndexAtChar(charNodeFrom));
					Node nodeTo = null;
					int iNodeFrom = AiNode.getIndexAtChar(charNodeFrom);
					int iNodeTo = AiNode.getIndexAtChar(charNodeTo);

					for (Node n : world.getScheme().getNodes()) {
						if (n.id == iNodeFrom)
							nodeFrom = n;
						else if (n.id == iNodeTo)
							nodeTo = n;
					}
					// = world.getScheme().getNodes()
					// .get(AiNode.getIndexAtChar(charNodeTo));
					Piece piece = nodeFrom.getPiece();

					dum = new Movement(null, nodeFrom, nodeTo, piece);
					// dum = movement;
				}
			}.start();
		} else if (dum != null) {
			started = false;
			Movement a = dum;
			dum = null;
			a.setOpponent(this);
			// dum.setOpponent(this);
			return a;
		}
		// return movement;
		return null;

	}

	protected Movement thirdPhase() {
		if (!started) {
			new Thread() {
				@Override
				public void run() {
					started = true;
					AiNode node = AiNode.getNodeFromSchema(world.getScheme());
					// Gdx.app.debug("node", node.getState());
					if (getOpponentColor() == OpponentColor.WHITE)
						node.setMoveWhite(true);
					else
						node.setMoveWhite(false);
					node.fillChildren(3);
					AiNode bestNode = node.getBestNode();
					// Gdx.app.debug("bestNode", bestNode.getState());

					char charNodeFrom = ' ';
					char charNodeTo = ' ';
					try {
						mainLoop: for (byte i = 0; i < node.getState().length(); i++) {
							if (node.getState().charAt(i) != bestNode
									.getState().charAt(i)) {
								if (node.getState().charAt(i) != '0') {
									charNodeFrom = node.getState().charAt(i);
									charNodeTo = bestNode.getState().charAt(i);
									break mainLoop;
								}
							}
						}
					} catch (Exception e) {
					}
					// Gdx.app.debug("charFrom", charNodeFrom + "");
					// Gdx.app.debug("charNodeTo", charNodeTo + "");

					Node nodeFrom = null;
					// = world.getScheme().getNodes()
					// .get(AiNode.getIndexAtChar(charNodeFrom));
					Node nodeTo = null;
					int iNodeFrom = AiNode.getIndexAtChar(charNodeFrom);
					int iNodeTo = AiNode.getIndexAtChar(charNodeTo);

					for (Node n : world.getScheme().getNodes()) {
						if (n.id == iNodeFrom)
							nodeFrom = n;
						else if (n.id == iNodeTo)
							nodeTo = n;
					}
					// = world.getScheme().getNodes()
					// .get(AiNode.getIndexAtChar(charNodeTo));
					Piece piece = nodeFrom.getPiece();

					dum = new Movement(null, nodeFrom, nodeTo, piece);
					// return movement;
				}
			}.start();
		} else if (dum != null) {
			started = false;
			Movement a = dum;
			dum = null;
			a.setOpponent(this);
			// dum.setOpponent(this);
			return a;
		}
		// return movement;
		return null;
	}

	Piece pieceToKill = null;

	@Override
	protected Piece getPieceToKill() {
		if (!started) {
			new Thread() {
				@Override
				public void run() {
					started = true;
					AiNode node = AiNode.getNodeFromSchema(world.getScheme());

					if (getOpponentColor() == OpponentColor.WHITE)
						node.setMoveWhite(true);
					else
						node.setMoveWhite(false);
					node.setJustDoneATris(true);
					node.fillChildren(2);
					AiNode bestNode = node.getBestNode();

					// Gdx.app.debug("bestNode", bestNode.getState());

					char charNodeFrom = ' ';
					char charNodeTo = ' ';
					mainLoop: for (byte i = 0; i < node.getState().length(); i++) {
						if (node.getState().charAt(i) != bestNode.getState()
								.charAt(i)) {
							if (node.getState().charAt(i) != '0') {
								charNodeFrom = node.getState().charAt(i);
								charNodeTo = bestNode.getState().charAt(i);
								break mainLoop;
							}
						}
					}
					// Gdx.app.debug("charFrom", charNodeFrom + "");
					// Gdx.app.debug("charNodeTo", charNodeTo + "");

					Node nodeFrom = null;
					// = world.getScheme().getNodes()
					// .get(AiNode.getIndexAtChar(charNodeFrom));
					int iNodeFrom = AiNode.getIndexAtChar(charNodeFrom);
					// Gdx.app.debug("iNodeFrom", iNodeFrom + "");

					for (Node n : world.getScheme().getNodes()) {
						if (n.id == iNodeFrom)
							nodeFrom = n;
					}
					// = world.getScheme().getNodes()
					// .get(AiNode.getIndexAtChar(charNodeTo));

					pieceToKill = nodeFrom.getPiece();
				}
			}.start();
		} else if (pieceToKill != null) {
			started = false;
			Piece a = pieceToKill;
			pieceToKill = null;
			// dum.setOpponent(this);
			return a;
		}

		return null;
		// List<Node> problematicNodes = world.getReverseOpponent(this)
		// .getEmptyNodesNearAMill();
		//
		// Piece piece;
		// try {
		// for (Node node : problematicNodes) {
		// if (enableToKill(node.getPiece()))
		// return node.getPiece();
		// }
		// } catch (Exception e) {
		// // e.printStackTrace();
		// }
		//
		// do {
		// piece = world
		// .getScheme()
		// .getPiecesOfReverseOpponent(this)
		// .get((int) (Math.random() * (double) world.getScheme()
		// .getPiecesOfReverseOpponent(this).size()));
		// } while (!enableToKill(piece));

		// return null;
	}

}
