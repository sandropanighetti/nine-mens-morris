package com.lpi.ninemensmorris.game;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.lpi.ninemensmorris.Assets;
import com.lpi.ninemensmorris.MatchConfiguration;
import com.lpi.ninemensmorris.Messages;
import com.lpi.ninemensmorris.game.Opponent.OpponentColor;
import com.lpi.ninemensmorris.game.Opponent.OpponentState;
import com.lpi.ninemensmorris.game.Opponent.OpponentType;
import com.lpi.ninemensmorris.screen.AbstractScreen;
import com.lpi.ninemensmorris.screen.GameScreen;
import com.lpi.ninemensmorris.screen.MatchInfoScreen;

/**
 * @author Sandro Panighetti - 16-gen-2013 NineMensMorris
 */
public class World {
	public enum GameState {
		RUNNING, PAUSED, GAME_OVER;
	}

	private MatchConfiguration matchConfiguration;
	private GameScreen gameScreen;

	public GameState gameState;

	private Schema schema;
	private Opponent opponentA;
	private Opponent opponentB;

	private Opponent winner;

	private List<Movement> movements;

	public World(GameScreen gameScreen, MatchConfiguration matchConfiguration) {
		gameState = GameState.PAUSED;
		this.matchConfiguration = matchConfiguration;
		this.gameScreen = gameScreen;

		// Loading
		// Initialize Opponent A
		opponentA = Opponent.getNewOpponent(this,
				matchConfiguration.getOpponentTypeA(), OpponentColor.WHITE);
		opponentB = Opponent.getNewOpponent(this,
				matchConfiguration.getOpponentTypeB(), OpponentColor.BLACK);

		// switch (matchConfiguration.getOpponentTypeA()) {
		// case HUMAN:
		// opponentA = new HumanAutomaticOpponent(this, OpponentColor.WHITE);
		// break;
		// case COMPUTER:
		// opponentA = new RandomAutomaticOpponent(this, OpponentColor.WHITE);
		// break;
		// }
		//
		// // Initialize Opponent B
		// switch (matchConfiguration.getOpponentTypeB()) {
		// case HUMAN:
		// opponentB = new HumanAutomaticOpponent(this, OpponentColor.BLACK);
		// break;
		// case COMPUTER:
		// opponentB = new RandomAutomaticOpponent(this, OpponentColor.BLACK);
		// break;
		// }

		opponentA.setOpponentState(OpponentState.PLAYING);
		opponentB.setOpponentState(OpponentState.WAITING_FOR_OTHER_PLAYER);
//		opponentA.setPiecesToPlace(4);
//		opponentB.setPiecesToPlace(4);

		schema = new Schema(this, AbstractScreen.SCREEN_WIDTH / 2.0f,
				AbstractScreen.SCREEN_HEIGHT - AbstractScreen.SCREEN_HEIGHT
						* 6.0f / 11.0f, AbstractScreen.SCREEN_WIDTH,
				AbstractScreen.SCREEN_WIDTH);
		schema.initNodes();
		schema.initPieces();
		// schema.printNodes();

		movements = new LinkedList<Movement>();

	}

	public void update(float delta) {
		switch (gameState) {
		case RUNNING:
			updateRunning(delta);
			break;
		case PAUSED:
			updatePaused(delta);
			break;
		default:
			break;
		}
	}

	/**
	 * @param delta
	 */
	private void updateRunning(float delta) {
		opponentA.update(delta);
		opponentB.update(delta);
		gameScreen.matchInfo.updateTime(delta);
	}

	private void updatePaused(float delta) {

	}

	/**
	 * @param movement
	 */
	public void move(Movement movement) {
		movements.add(movement);
		schema.move(movement);
		Opponent opponent = schema.loser();
		if (null != opponent) {
			gameState = GameState.GAME_OVER;
			winner = getReverseOpponent(opponent);
			gameScreen.matchInfo.setMoves(movements.size());
			gameScreen.matchInfo.setOpponentColorWinner(winner.opponentColor);
			if (winner instanceof HumanAutomaticOpponent)
				gameScreen.matchInfo.setOpponentWinner(OpponentType.HUMAN);
			else
				gameScreen.matchInfo.setOpponentWinner(OpponentType.COMPUTER);

			gameScreen.getNineMensMorris().setScreen(
					new MatchInfoScreen(gameScreen.getNineMensMorris(),
							gameScreen.matchInfo));
//			Gdx.app.debug("MatchInfo", "" + gameScreen.matchInfo.toString());
			// gameScreen.getTitleText().text = winner.getOpponentColor()
			// .toString() + " WIN";

		}
	}

//	public void writeSchemaToFile() {
//		Gson gson = new Gson();
//		FileHandle file = Gdx.files.local("current_schema.conf");
//		String toWrite = gson.toJson(schema, new TypeToken<Schema>() {
//		}.getType());
//		Gdx.app.debug("toWrite", toWrite);
//	}

	public void turnFinished(Opponent opponent) {
//		writeSchemaToFile();
		opponent.setOpponentState(OpponentState.WAITING_FOR_OTHER_PLAYER);
		getReverseOpponent(opponent).setOpponentState(OpponentState.PLAYING);
		if (getReverseOpponent(opponent).getOpponentColor() == OpponentColor.WHITE)
			gameScreen.setTitleText(Messages.Game.WHITE_TURN);
		else
			gameScreen.setTitleText(Messages.Game.BLACK_TURN);

		// gameScreen.setTitleText(getReverseOpponent(opponent).getOpponentColor()
		// .toString());
		// Gdx.app.debug("currentTurn",
		// getReverseOpponent(opponent).getOpponentColor().toString());

	}

	/**
	 * @return the schema
	 */
	public Schema getScheme() {
		return schema;
	}

	/**
	 * @param schema
	 *            the schema to set
	 */
	public void setScheme(Schema schema) {
		this.schema = schema;
	}

	/**
	 * @return the opponentA
	 */
	public Opponent getOpponentA() {
		return opponentA;
	}

	/**
	 * @param opponentA
	 *            the opponentA to set
	 */
	public void setOpponentA(Opponent opponentA) {
		this.opponentA = opponentA;
	}

	/**
	 * @return the opponentB
	 */
	public Opponent getOpponentB() {
		return opponentB;
	}

	/**
	 * @param opponentB
	 *            the opponentB to set
	 */
	public void setOpponentB(Opponent opponentB) {
		this.opponentB = opponentB;
	}

	/**
	 * @return the gameScreen
	 */
	public GameScreen getGameScreen() {
		return gameScreen;
	}

	/**
	 * @param gameScreen
	 *            the gameScreen to set
	 */
	public void setGameScreen(GameScreen gameScreen) {
		this.gameScreen = gameScreen;
	}

	public Opponent getReverseOpponent(Opponent opponent) {
		if (opponentA == opponent)
			return opponentB;
		else if (opponentB == opponent)
			return opponentA;
		return null;
	}

	/**
	 * @return the winner
	 */
	public Opponent getWinner() {
		return winner;
	}

	/**
	 * @param winner
	 *            the winner to set
	 */
	public void setWinner(Opponent winner) {
		this.winner = winner;
	}

	/**
	 * @return the matchConfiguration
	 */
	public MatchConfiguration getMatchConfiguration() {
		return matchConfiguration;
	}

	/**
	 * @param matchConfiguration
	 *            the matchConfiguration to set
	 */
	public void setMatchConfiguration(MatchConfiguration matchConfiguration) {
		this.matchConfiguration = matchConfiguration;
	}
}
