package com.lpi.ninemensmorris.game;

import com.lpi.ninemensmorris.Utility.Direction;

/**
 * @author Sandro Panighetti - 16-gen-2013 NineMensMorris
 */
public class Node extends GameObject implements Comparable<Node> {

	private Schema schema;

	private Piece piece;

	private Node up;
	private Node right;
	private Node down;
	private Node left;

	public int id;

	/**
	 * 
	 */
	public Node() {
		super();
	}

	/**
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public Node(float x, float y, float width, float height) {
		super(x, y, width, height);
	}

	/**
	 * @param schema
	 */
	public Node(Schema schema, int id) {
		super();
		this.schema = schema;
		this.id = id;
	}

	public int getNumberOfNeighbour() {
		int cnt = 0;
		if (null != up)
			cnt++;
		if (null != right)
			cnt++;
		if (null != down)
			cnt++;
		if (null != left)
			cnt++;

		return cnt;

	}

	public String getIdsOfNeighbour() {
		StringBuilder stringBuilder = new StringBuilder();

		if (null != up)
			stringBuilder.append(up.id);
		else
			stringBuilder.append(".");
		stringBuilder.append(" - ");

		if (null != right)
			stringBuilder.append(right.id);
		else
			stringBuilder.append(".");
		stringBuilder.append(" - ");

		if (null != down)
			stringBuilder.append(down.id);
		else
			stringBuilder.append(".");
		stringBuilder.append(" - ");
		if (null != left)
			stringBuilder.append(left.id);
		else
			stringBuilder.append(".");

		return stringBuilder.toString();
	}

	/**
	 * @return the schema
	 */
	public Schema getScheme() {
		return schema;
	}

	/**
	 * @param schema
	 *            the schema to set
	 */
	public void setScheme(Schema schema) {
		this.schema = schema;
	}

	public Node getNode(Direction direction) {
		switch (direction) {
		case UP:
			if (null != up)
				return up;
			return null;
		case RIGHT:
			if (null != right)
				return right;
			return null;
		case DOWN:
			if (null != down)
				return down;
			return null;
		case LEFT:
			if (null != left)
				return left;
			return null;
		}
		return null;
	}

	public int getStepsFromAPiece(Opponent opponent, Node node, int deep) {
		if (null != node.getPiece())
//			if (node.getPiece().getOpponent() == opponent)
				return 0;
		if (deep < 0)
			return 999;

		int min = 999;
		if (null != node.up) {
			int dum = getStepsFromAPiece(opponent, node.up, deep - 1);
			if (dum < min)
				min = dum;
		}
		if (null != node.right) {
			int dum = getStepsFromAPiece(opponent, node.right, deep - 1);
			if (dum < min)
				min = dum;
		}
		if (null != node.down) {
			int dum = getStepsFromAPiece(opponent, node.down, deep - 1);
			if (dum < min)
				min = dum;
		}
		if (null != node.left) {
			int dum = getStepsFromAPiece(opponent, node.left, deep - 1);
			if (dum < min)
				min = dum;
		}

		return min+1;
	}

	/**
	 * @return the piece
	 */
	public Piece getPiece() {
		return piece;
	}

	/**
	 * @param piece
	 *            the piece to set
	 */
	public void setPiece(Piece piece) {
		this.piece = piece;
	}

	/**
	 * @return the up
	 */
	public Node getUp() {
		return up;
	}

	/**
	 * @param up
	 *            the up to set
	 */
	public void setUp(Node up) {
		this.up = up;
	}

	/**
	 * @return the right
	 */
	public Node getRight() {
		return right;
	}

	/**
	 * @param right
	 *            the right to set
	 */
	public void setRight(Node right) {
		this.right = right;
	}

	/**
	 * @return the down
	 */
	public Node getDown() {
		return down;
	}

	/**
	 * @param down
	 *            the down to set
	 */
	public void setDown(Node down) {
		this.down = down;
	}

	/**
	 * @return the left
	 */
	public Node getLeft() {
		return left;
	}

	/**
	 * @param left
	 *            the left to set
	 */
	public void setLeft(Node left) {
		this.left = left;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Node [id=");
		builder.append(id);
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int compareTo(Node o) {
		if(o.getNumberOfNeighbour()>getNumberOfNeighbour())
			return 1;

		if(o.getNumberOfNeighbour()==getNumberOfNeighbour())
			return 0;
		// TODO Auto-generated method stub
		return -1;
	}

}
