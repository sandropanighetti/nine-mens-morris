package com.lpi.ninemensmorris.game;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * @author Sandro Panighetti - 29-gen-2013
 * NineMensMorris
 */
public class GameObject {

	public Rectangle bounds;

	public Vector2 position;

	/**
	 * 
	 */
	public GameObject() {
		super();
		bounds = new Rectangle();
		position = new Vector2();
	}

	/**
	 * @param bounds
	 * @param position
	 */
	public GameObject(Rectangle bounds, Vector2 position) {
		super();
		this.bounds = bounds;
		this.position = position;
	}

	/**
	 * @param x
	 *            (center of the object)
	 * @param y
	 *            (center of the object)
	 * @param width
	 * @param height
	 */
	public GameObject(float x, float y, float width, float height) {
		super();
		bounds = new Rectangle();
		position = new Vector2();
		setCoordinates(x, y, width, height);
	}

	public void setCoordinates(float x, float y, float width, float height) {
		position.set(x, y);
		bounds.set(x - width / 2, y - height / 2, width, height);
	}
	public void setCoordinates(float x, float y) {
		setCoordinates(x, y, bounds.width, bounds.height);
	}
	
	public void setCoordinates(){
		setCoordinates(position.x,position.y);
	}

}
