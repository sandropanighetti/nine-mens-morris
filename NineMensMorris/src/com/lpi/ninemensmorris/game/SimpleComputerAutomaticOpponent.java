package com.lpi.ninemensmorris.game;

import java.util.List;

import com.badlogic.gdx.Gdx;

public class SimpleComputerAutomaticOpponent extends AutomaticOpponent {

	public SimpleComputerAutomaticOpponent(World world,
			OpponentColor opponentColor) {
		super(world, opponentColor);
	}

	@Override
	protected Movement firstPhase() {
		Node node;
		Piece piece;

		List<Node> problematicNodes = getEmptyNodesNearAMill();
		if (problematicNodes.size() > 0) {
			node = problematicNodes.get(0);
			Gdx.app.debug("mill", "opened");
		} else {
			List<Node> nodes = getNodesNearAMill();
			if (nodes.size() > 0) {
				node = nodes.get(0);
			} else {

				do {
					node = world
							.getScheme()
							.getNodes()
							.get((int) (Math.random() * (double) world
									.getScheme().getNodes().size()));
				} while (null != node.getPiece());
			}
		}
		piece = world.getScheme().getPiecesOfOpponent(this)
				.getFirstPieceInBase();

		return new Movement(this, node, piece);
	}

	@Override
	protected Movement secondPhase() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Movement thirdPhase() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Piece getPieceToKill() {
		// TODO Auto-generated method stub
		return null;
	}




}
