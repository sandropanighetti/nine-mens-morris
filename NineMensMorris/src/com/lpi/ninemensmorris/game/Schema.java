package com.lpi.ninemensmorris.game;

import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.lpi.ninemensmorris.Assets;
import com.lpi.ninemensmorris.game.Opponent.OpponentPhase;
import com.lpi.ninemensmorris.screen.AbstractScreen;

/**
 * @author Sandro Panighetti - 29-gen-2013 NineMensMorris
 */
public class Schema extends GameObject {

	private World world;

	private List<Node> nodes;
	private ListOfPieces whitePieces;
	private ListOfPieces blackPieces;

	/**
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public Schema(World world, float x, float y, float width, float height) {
		super(x, y, width, height);
		this.world = world;
	}

	public void initNodes() {
		nodes = new LinkedList<Node>();

		// Initialize all nodes
		for (int i = 0; i < 24; i++) {
			nodes.add(new Node(this, i));
		}

		// Initialize neighbors
		for (int i = 0; i < 24; i++) {
			if (Assets.nodesNeighbors[i][0] != -1)
				nodes.get(i).setUp(nodes.get(Assets.nodesNeighbors[i][0]));
			if (Assets.nodesNeighbors[i][1] != -1)
				nodes.get(i).setRight(nodes.get(Assets.nodesNeighbors[i][1]));
			if (Assets.nodesNeighbors[i][2] != -1)
				nodes.get(i).setDown(nodes.get(Assets.nodesNeighbors[i][2]));
			if (Assets.nodesNeighbors[i][3] != -1)
				nodes.get(i).setLeft(nodes.get(Assets.nodesNeighbors[i][3]));
		}

		// Initialize positions
		float spacing = bounds.width / 8.0f;
		float dim = 1.0f;

		int cnt = 0;
		for (Node n : nodes) {
			n.setCoordinates(position.x - bounds.width / 2.0f + spacing
					* (1 + Assets.nodesCoordinates[cnt][0]),
					AbstractScreen.SCREEN_HEIGHT
							- (position.y - bounds.height / 2.0f + spacing
									* (1 + Assets.nodesCoordinates[cnt][1])),
					spacing * dim, spacing * dim);
			cnt++;
		}
		// nodes.get(0).setCoordinates(0, 0, 10, 10);

	}

	public void initPieces() {
		float pieceWidth = bounds.width / 8.0f / 1.5f;
		whitePieces = new ListOfPieces(world.getOpponentA());
		for (int i = 0; i < whitePieces.getOpponent().getPiecesToPlace(); i++) {
			whitePieces.add(new Piece(whitePieces.getOpponent(),
					AbstractScreen.SCREEN_WIDTH / 2.0f - pieceWidth / 2.0f
							* (i + 2),
					AbstractScreen.SCREEN_HEIGHT * 2.5f / 11.0f, pieceWidth,
					pieceWidth));
		}

		blackPieces = new ListOfPieces(world.getOpponentB());
		for (int i = 0; i < blackPieces.getOpponent().getPiecesToPlace(); i++) {
			blackPieces.add(new Piece(blackPieces.getOpponent(),
					AbstractScreen.SCREEN_WIDTH / 2.0f + pieceWidth / 2.0f
							* (11) - pieceWidth / 2.0f * (i + 1),
					AbstractScreen.SCREEN_HEIGHT * 2.5f / 11.0f, pieceWidth,
					pieceWidth));
		}
		// int cnt=0;
		// for(Piece p : whitePieces){
		// Gdx.app.debug("piece " + cnt, p.bounds.width+"");
		// cnt++;
		// }

	}

	public void printNodes() {
		StringBuilder stringBuilder = new StringBuilder();
		int i = 0;
		for (Node n : nodes) {
			stringBuilder.append(i);
			stringBuilder.append(": ");
			stringBuilder.append(n.getIdsOfNeighbour());
			stringBuilder.append("\n");
			i++;
		}
		Gdx.app.debug("nodes", stringBuilder.toString());
	}

	public Opponent loser() {

		if (getPiecesOfOpponent(world.getOpponentA()).size() < 3
				|| !world.getOpponentA().enableToMove())
			return world.getOpponentA();
		else if (getPiecesOfOpponent(world.getOpponentB()).size() < 3
				|| !world.getOpponentB().enableToMove())
			return world.getOpponentB();

		return null;

	}

	public void move(Movement movement) {

		// To kill
		if (null != movement.getPieceToKill()) {
			// Gdx.app.debug("toKill", "ok");
			world.getReverseOpponent(movement.getOpponent())
					.removeAPieceInGame();

			movement.getPieceToKill().getNode().setPiece(null);
			movement.getPieceToKill().setNode(null);
			getPiecesOfReverseOpponent(movement.getOpponent()).remove(
					movement.getPieceToKill());

			if (world.getReverseOpponent(movement.getOpponent())
					.getPiecesInGame() == 3
					&& world.getReverseOpponent(movement.getOpponent()).piecesToPlace == 0) {

				world.getReverseOpponent(movement.getOpponent())
						.setOpponentPhase(OpponentPhase.THIRD);
			}
			return;
		}

		// Gdx.app.debug("Opponent phase - "
		// + movement.getOpponent().getOpponentColor().toString(),
		// movement.getOpponent().getOpponentPhase().toString());

		switch (movement.getOpponent().getOpponentPhase()) {
		case FIRST:
			// Remove a pice from base
			movement.getOpponent().removeAPieceToPlace();

			// Add a piece in game
			movement.getOpponent().addAPieceInGame();
			movement.getPiece().setNode(movement.getNodeTo());
			movement.getNodeTo().setPiece(movement.getPiece());

			// If all pices are in the schema, go to the second phase
			if (movement.getOpponent().getPiecesToPlace() == 0)
				movement.getOpponent().setOpponentPhase(OpponentPhase.SECOND);

			break;
		case SECOND:
			movement.getNodeFrom().setPiece(null);
			// Setting Node - Piece
			movement.getNodeTo().setPiece(movement.getPiece());
			movement.getPiece().setNode(movement.getNodeTo());
			break;
		case THIRD:
			movement.getNodeFrom().setPiece(null);
			// Setting Node - Piece
			movement.getNodeTo().setPiece(movement.getPiece());
			movement.getPiece().setNode(movement.getNodeTo());
			break;
		}
		if (world.getReverseOpponent(movement.getOpponent()).getPiecesInGame() == 3
				&& world.getReverseOpponent(movement.getOpponent()).piecesToPlace == 0) {

			world.getReverseOpponent(movement.getOpponent()).setOpponentPhase(
					OpponentPhase.THIRD);
		}

	}

	public List<Schema> getChildrenOfScheme() {
		return null;
	}

	public Node overANode(float x, float y) {

		for (Node n : nodes)
			if (n.bounds.contains(x, y)) {
				return n;
			}
		return null;
	}

	public Piece overAPiece(float x, float y) {
		Piece piece = null;

		for (Piece p : whitePieces) {
			if (p.bounds.contains(x, y)) {
				piece = p;
			}
		}

		for (Piece p : blackPieces) {
			if (p.bounds.contains(x, y)) {
				piece = p;
			}
		}

		return piece;
	}

	/**
	 * @param opponent
	 * @return
	 */
	public ListOfPieces getPiecesOfOpponent(Opponent opponent) {
		return opponent == whitePieces.getOpponent() ? whitePieces
				: blackPieces;
	}

	/**
	 * @param reverse
	 *            opponent
	 * @return
	 */
	public ListOfPieces getPiecesOfReverseOpponent(Opponent opponent) {
		return opponent != whitePieces.getOpponent() ? whitePieces
				: blackPieces;
	}

	/**
	 * @return the nodes
	 */
	public List<Node> getNodes() {
		return nodes;
	}

	/**
	 * @param nodes
	 *            the nodes to set
	 */
	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}

	/**
	 * @return the whitePieces
	 */
	public ListOfPieces getWhitePieces() {
		return whitePieces;
	}

	/**
	 * @param whitePieces
	 *            the whitePieces to set
	 */
	public void setWhitePieces(ListOfPieces whitePieces) {
		this.whitePieces = whitePieces;
	}

	/**
	 * @return the blackPieces
	 */
	public ListOfPieces getBlackPieces() {
		return blackPieces;
	}

	/**
	 * @param blackPieces
	 *            the blackPieces to set
	 */
	public void setBlackPieces(ListOfPieces blackPieces) {
		this.blackPieces = blackPieces;
	}

}
