package com.lpi.ninemensmorris.game;

import com.badlogic.gdx.Gdx;

public class HumanAutomaticOpponent extends AutomaticOpponent {

	protected Piece selectedPiece;

	public HumanAutomaticOpponent(World world, OpponentColor opponentColor) {
		super(world, opponentColor);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Movement firstPhase() {
		// Single click
		if (Gdx.input.justTouched()) {
			world.getGameScreen().updateTouchPoint();

			Node node = world.getScheme().overANode(
					world.getGameScreen().touchPoint.x,
					world.getGameScreen().touchPoint.y);
			
			

			if (null != node) {
//				int index = world.getScheme().getNodes().indexOf(node);
//				Gdx.app.debug("index",node.id+"");
				Piece piece = world.getScheme().getPiecesOfOpponent(this)
						.getFirstPieceInBase();

				if (!enableToPlace(piece, node))
					return null;

				return new Movement(this, node, piece);
			}
		} else if (Gdx.input.isTouched()) {
			world.getGameScreen().updateTouchPoint();
			if (null == selectedPiece) {

				Piece piece = world.getScheme().overAPiece(
						world.getGameScreen().touchPoint.x,
						world.getGameScreen().touchPoint.y);

				if (null != piece) {
					if (enableToMovePiece(piece))
						selectedPiece = piece;
				}
			}

			if (null != selectedPiece) {
				selectedPiece.setCoordinates(
						world.getGameScreen().touchPoint.x,
						world.getGameScreen().touchPoint.y);
			}

		}
		// TouchedUp
		else {
			if (null != selectedPiece) {
				world.getGameScreen().updateTouchPoint();

				Node node = world.getScheme().overANode(
						world.getGameScreen().touchPoint.x,
						world.getGameScreen().touchPoint.y);

				if (null != node && enableToPlace(selectedPiece, node)) {
					Movement m = new Movement(this, node, selectedPiece);
					selectedPiece = null;
					return m;
				} else {
					if (null != selectedPiece.getNode())
						selectedPiece.setCoordinates(
								selectedPiece.getNode().position.x,
								selectedPiece.getNode().position.y);
					else
						selectedPiece.setCoordinates(
								selectedPiece.startPosition.x,
								selectedPiece.startPosition.y);

					selectedPiece = null;
				}

			}
		}
		return null;
	}

	@Override
	protected Movement secondPhase() {
		if (Gdx.input.isTouched()) {

			world.getGameScreen().updateTouchPoint();
			if (null == selectedPiece) {

				Piece piece = world.getScheme().overAPiece(
						world.getGameScreen().touchPoint.x,
						world.getGameScreen().touchPoint.y);

				if (null != piece) {
					if (enableToMovePiece(piece))
						selectedPiece = piece;
				}
			}

			if (null != selectedPiece) {
				selectedPiece.setCoordinates(
						world.getGameScreen().touchPoint.x,
						world.getGameScreen().touchPoint.y);
			}

		}
		// TouchedUp
		else {
			if (null != selectedPiece) {
				world.getGameScreen().updateTouchPoint();

				Node node = world.getScheme().overANode(
						world.getGameScreen().touchPoint.x,
						world.getGameScreen().touchPoint.y);

				if (null != node && enableToPlace(selectedPiece, node)) {
					Movement m = new Movement(this, selectedPiece.getNode(), node, selectedPiece);
					selectedPiece = null;
					return m;
				} else {
					if (null != selectedPiece.getNode())
						selectedPiece.setCoordinates(
								selectedPiece.getNode().position.x,
								selectedPiece.getNode().position.y);
					else
						selectedPiece.setCoordinates(
								selectedPiece.startPosition.x,
								selectedPiece.startPosition.y);

					selectedPiece = null;
				}

			}
		}
		return null;
	}

	@Override
	protected Movement thirdPhase() {		
		return secondPhase();
	}

	@Override
	protected Piece getPieceToKill() {

		if (Gdx.input.justTouched()) {
			world.getGameScreen().updateTouchPoint();

			Piece piece = world.getScheme().overAPiece(
					world.getGameScreen().touchPoint.x,
					world.getGameScreen().touchPoint.y);

			if (null != piece)
				if (enableToKill(piece))
					return piece;
		}
		return null;
	}
}
