package com.lpi.ninemensmorris.game;

import java.util.Collection;
import java.util.LinkedList;

public class ListOfPieces extends LinkedList<Piece>{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6187532561130061459L;
	private Opponent opponent;

	/**
	 * 
	 */
	public ListOfPieces(Opponent opponent) {
		super();
		this.opponent = opponent;
	}

	/**
	 * @param c
	 */
	public ListOfPieces(Opponent opponent, Collection<? extends Piece> c) {
		super(c);
		this.opponent = opponent;
	}

	/**
	 * @return the opponent
	 */
	public Opponent getOpponent() {
		return opponent;
	}

	/**
	 * @param opponent the opponent to set
	 */
	public void setOpponent(Opponent opponent) {
		this.opponent = opponent;
	}
	
	public Piece getFirstPieceInBase(){
		for(Piece p : this)
			if(null == p.getNode())
				return p;
		
		return null;
	}

	
}
