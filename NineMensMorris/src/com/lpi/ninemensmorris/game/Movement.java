package com.lpi.ninemensmorris.game;

/**
 * @author Sandro Panighetti - 16-gen-2013 NineMensMorris
 */
public class Movement {

	private Node nodeFrom;
	private Node nodeTo;
	private Piece piece;
	private Piece pieceToKill;
	private Opponent opponent;

	/**
	 * @param nodeFrom
	 * @param nodeTo
	 * @param piece
	 */
	public Movement(Opponent opponent, Node nodeFrom, Node nodeTo, Piece piece) {
		super();
		this.opponent = opponent;
		this.nodeFrom = nodeFrom;
		this.nodeTo = nodeTo;
		this.piece = piece;
	}


	/**
	 * @param nodeTo
	 * @param piece
	 */
	public Movement(Opponent opponent, Node nodeTo, Piece piece) {
		super();
		this.opponent = opponent;
		this.nodeTo = nodeTo;
		this.piece = piece;
	}
	
	public Movement(Opponent opponent, Piece pieceToKill){
		super();
		this.opponent = opponent;
		this.pieceToKill = pieceToKill;
	}


	/**
	 * @return the nodeFrom
	 */
	public Node getNodeFrom() {
		return nodeFrom;
	}

	/**
	 * @param nodeFrom
	 *            the nodeFrom to set
	 */
	public void setNodeFrom(Node nodeFrom) {
		this.nodeFrom = nodeFrom;
	}

	/**
	 * @return the nodeTo
	 */
	public Node getNodeTo() {
		return nodeTo;
	}

	/**
	 * @param nodeTo
	 *            the nodeTo to set
	 */
	public void setNodeTo(Node nodeTo) {
		this.nodeTo = nodeTo;
	}

	/**
	 * @return the piece
	 */
	public Piece getPiece() {
		return piece;
	}

	/**
	 * @param piece
	 *            the piece to set
	 */
	public void setPiece(Piece piece) {
		this.piece = piece;
	}

	/**
	 * @return the pieceToKill
	 */
	public Piece getPieceToKill() {
		return pieceToKill;
	}

	/**
	 * @param pieceToKill
	 *            the pieceToKill to set
	 */
	public void setPieceToKill(Piece pieceToKill) {
		this.pieceToKill = pieceToKill;
	}


	/**
	 * @return the opponent
	 */
	public Opponent getOpponent() {
		return opponent;
	}


	/**
	 * @param opponent the opponent to set
	 */
	public void setOpponent(Opponent opponent) {
		this.opponent = opponent;
	}

}
