package com.lpi.ninemensmorris.game;

import com.badlogic.gdx.math.Vector2;

/**
 * @author Sandro Panighetti - 16-gen-2013 NineMensMorris
 */
public class Piece extends GameObject {

	private Node node;

	private Opponent opponent;

	public Vector2 startPosition;

	/**
	 * @param opponent
	 */
	public Piece(Opponent opponent) {
		super();
		this.opponent = opponent;
		this.startPosition = position.cpy();
	}

	/**
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public Piece(Opponent opponent, float x, float y, float width, float height) {
		super(x, y, width, height);
		this.opponent = opponent;
		this.startPosition = position.cpy();
	}

	public void setStartCoordinates(float x, float y) {
		startPosition.set(x, y);
	}

	public void setStartCoordinates() {
		startPosition.set(position.x, position.y);
	}

	/**
	 * @return the node
	 */
	public Node getNode() {
		return node;
	}

	/**
	 * @param node
	 *            the node to set
	 */
	public void setNode(Node node) {
		this.node = node;
	}

	/**
	 * @return the opponent
	 */
	public Opponent getOpponent() {
		return opponent;
	}

	/**
	 * @param opponent
	 *            the opponent to set
	 */
	public void setOpponent(Opponent opponent) {
		this.opponent = opponent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Piece [node=");
		builder.append(node);
		builder.append(", opponent=");
		builder.append(opponent);
		builder.append(", bounds=");
		builder.append(bounds);
		builder.append(", position=");
		builder.append(position);
		builder.append("]");
		return builder.toString();
	}

}
