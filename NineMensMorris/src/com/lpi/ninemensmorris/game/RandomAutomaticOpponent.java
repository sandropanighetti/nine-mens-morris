package com.lpi.ninemensmorris.game;

import com.lpi.ninemensmorris.Utility;
import com.lpi.ninemensmorris.Utility.Direction;

public class RandomAutomaticOpponent extends AutomaticOpponent {

	public RandomAutomaticOpponent(World world, OpponentColor opponentColor) {
		super(world, opponentColor);
		movingSpeed = 25.0f;
		killingSpeed = movingSpeed;
	}

	protected Movement firstPhase() {
		Node node;
		do {
			node = world
					.getScheme()
					.getNodes()
					.get((int) (Math.random() * (double) world.getScheme()
							.getNodes().size()));
		} while (null != node.getPiece());
		world.getScheme().getNodes().size();

		Piece piece = world.getScheme().getPiecesOfOpponent(this)
				.getFirstPieceInBase();

		return new Movement(this, node, piece);

	}

	protected Movement secondPhase() {

		Piece piece;
		do {
			piece = world
					.getScheme()
					.getPiecesOfOpponent(this)
					.get((int) (Math.random() * (double) world.getScheme()
							.getPiecesOfOpponent(this).size()));
		} while (!enableToMovePiece(piece));
		Direction direction;
		do {
			direction = Utility.getRandomDirection();
		} while (!isFreeNearAPiece(piece, direction));
		return new Movement(this, piece.getNode(), piece.getNode().getNode(
				direction), piece);

		// piece.setCoordinates(node.position.x, node.position.y);
		// piece.setStartCoordinates();
//		world.move(movement);
//		prepareToMove();
//		if (!(countMaxNeighbours(movement.getNodeTo(), this) < 2 || !enableToKill(this)))
//			prepareToKill();
	}

	protected Movement thirdPhase() {

		Node node;
		do {
			node = world
					.getScheme()
					.getNodes()
					.get((int) (Math.random() * (double) world.getScheme()
							.getNodes().size()));
		} while (null != node.getPiece());
		world.getScheme().getNodes().size();

		Piece piece = world
				.getScheme()
				.getPiecesOfOpponent(this)
				.get((int) (Math.random() * world.getScheme()
						.getPiecesOfOpponent(this).size()));

		// piece.setCoordinates(node.position.x, node.position.y);
		//
		// piece.setStartCoordinates();

		return new Movement(this, piece.getNode(), node, piece);
//		world.move(movement);
//
//		prepareToMove();
//		if (!(countMaxNeighbours(movement.getNodeTo(), this) < 2 || !enableToKill(this)))
//			prepareToKill();
	}

	@Override
	protected Piece getPieceToKill() {

		Piece piece;

		do {
			piece = world
					.getScheme()
					.getPiecesOfReverseOpponent(this)
					.get((int) (Math.random() * (double) world.getScheme()
							.getPiecesOfReverseOpponent(this).size()));
		} while (!enableToKill(piece));

		return piece;
		
//		movement.setPieceToKill(piece);
//
//		killVector = new Vector2(killZone.x + killZone.width / 2.0f
//				- piece.position.x, killZone.y - killZone.height / 2.0f
//				- piece.position.y);
//		killVector.div(killingSpeed);
	}
}
