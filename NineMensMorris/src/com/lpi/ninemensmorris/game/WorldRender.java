package com.lpi.ninemensmorris.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.lpi.ninemensmorris.Assets;
import com.lpi.ninemensmorris.game.Opponent.OpponentColor;
import com.lpi.ninemensmorris.screen.AbstractScreen;

/**
 * @author Sandro Panighetti - 16-gen-2013 NineMensMorris
 */
public class WorldRender {

	public OrthographicCamera camera;
	private SpriteBatch batch;
	private ShapeRenderer shapeRenderer;

	private World world;

	/**
	 * @param world
	 */
	public WorldRender(SpriteBatch batch, World world) {
		super();
		this.world = world;

		this.camera = new OrthographicCamera(
				AbstractScreen.SCREEN_WIDTH,
				AbstractScreen.SCREEN_HEIGHT);
		this.camera.position.set(AbstractScreen.SCREEN_WIDTH / 2.0f,
				AbstractScreen.SCREEN_HEIGHT / 2.0f, 0);
		this.batch = batch;
		this.shapeRenderer = new ShapeRenderer();
	}

	public void render() {
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		renderBackground();
		renderObjects();
	}

	private void renderBackground() {
		batch.disableBlending();
		batch.begin();

		batch.draw(Assets.woodBackgroundTexture,
				camera.position.x - AbstractScreen.SCREEN_WIDTH / 2,
				camera.position.y - AbstractScreen.SCREEN_HEIGHT / 2,
				AbstractScreen.SCREEN_WIDTH, AbstractScreen.SCREEN_HEIGHT);

		// Batch.draw background

		batch.end();
	}

	private void renderObjects() {
		// Debugging
		if (true) {
			camera.update();
			shapeRenderer.setProjectionMatrix(camera.combined);
			shapeRenderer.begin(ShapeType.Rectangle);
			shapeRenderer.setColor(Color.WHITE);
			// renderNodesBounds();
			shapeRenderer.end();

			shapeRenderer.begin(ShapeType.Line);
			shapeRenderer.setColor(Color.WHITE);
			renderNodesLinks();
			shapeRenderer.end();

			shapeRenderer.begin(ShapeType.Circle);
			// renderPiecesBorder();
			shapeRenderer.end();
		}
		batch.enableBlending();
		batch.begin();

		renderPieces();

		batch.end();

	}

	private void renderPieces() {
		for (Piece piece : world.getScheme().getWhitePieces()) {
			renderPiece(piece);
		}

		for (Piece piece : world.getScheme().getBlackPieces()) {
			renderPiece(piece);
		}
	}

	private void renderPiece(Piece piece) {
		Opponent opponent = piece.getOpponent();
		OpponentColor opponentColor = opponent.getOpponentColor();
		// opponentColor = OpponentColor.WHITE;

		if (opponentColor == OpponentColor.WHITE) {
			batch.draw(Assets.whiteManTexture, piece.bounds.x, piece.bounds.y,
					piece.position.x, piece.position.y, piece.bounds.width,
					piece.bounds.height, 1, 1, 0);
		} else {
			batch.draw(Assets.blackManTexture, piece.bounds.x, piece.bounds.y,
					piece.position.x, piece.position.y, piece.bounds.width,
					piece.bounds.height, 1, 1, 0);
		}
	}

	// Debugging
	@SuppressWarnings("unused")
	private void renderNodesBounds() {
		for (GameObject go : world.getScheme().getNodes()) {
			renderNodeBound(go);
		}
	}

	private void renderNodeBound(GameObject gameObject) {
		shapeRenderer.rect(gameObject.bounds.x, gameObject.bounds.y,
				gameObject.bounds.width, gameObject.bounds.height);
	}

	private void renderNodesLinks() {
		for (Node n : world.getScheme().getNodes()) {
			renderNodeLink(n);
		}
	}

	private void renderNodeLink(Node n) {
		if (null != n.getUp())
			shapeRenderer.line(n.position.x, n.position.y,
					n.getUp().position.x, n.getUp().position.y);
		if (null != n.getRight())
			shapeRenderer.line(n.position.x, n.position.y,
					n.getRight().position.x, n.getRight().position.y);
		// if (null != n.getDown())
		// shapeRenderer.line(n.position.x, n.position.y,
		// n.getDown().position.x, n.getDown().position.y);
		// if (null != n.getLeft())
		// shapeRenderer.line(n.position.x, n.position.y,
		// n.getLeft().position.x, n.getLeft().position.y);
	}

	/*
	 * Debugging
	 */
	@SuppressWarnings("unused")
	private void renderPiecesBorder() {
		shapeRenderer.setColor(Color.YELLOW);
		for (GameObject go : world.getScheme().getWhitePieces()) {
			renderPieceBorder(go);
		}

		shapeRenderer.setColor(Color.MAGENTA);
		for (GameObject go : world.getScheme().getBlackPieces()) {
			renderPieceBorder(go);
		}
	}

	private void renderPieceBorder(GameObject gameObject) {
		shapeRenderer.circle(gameObject.position.x, gameObject.position.y,
				gameObject.bounds.width / 2.0f);
	}
}
