package com.lpi.ninemensmorris;

import com.lpi.ninemensmorris.game.Opponent.OpponentType;

/**
 * @author Sandro Panighetti - 16-gen-2013 NineMensMorris
 */
public class MatchConfiguration {


	private OpponentType opponentTypeA;
	private OpponentType opponentTypeB;

	/**
	 * 
	 */
	// public MatchConfiguration() {
	// super();
	// }

	/**
	 * @param opponentA
	 * @param opponentB
	 */
	public MatchConfiguration(OpponentType opponentA, OpponentType opponentB) {
		super();
		this.opponentTypeA = opponentA;
		this.opponentTypeB = opponentB;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MatchConfiguration [opponentA=" + opponentTypeA
				+ ", opponentB=" + opponentTypeB + "]";
	}

	/**
	 * @return the opponentTypeA
	 */
	public OpponentType getOpponentTypeA() {
		return opponentTypeA;
	}

	/**
	 * @param opponentTypeA
	 *            the opponentTypeA to set
	 */
	public void setOpponentTypeA(OpponentType opponentTypeA) {
		this.opponentTypeA = opponentTypeA;
	}

	/**
	 * @return the opponentTypeB
	 */
	public OpponentType getOpponentTypeB() {
		return opponentTypeB;
	}

	/**
	 * @param opponentTypeB
	 *            the opponentTypeB to set
	 */
	public void setOpponentTypeB(OpponentType opponentTypeB) {
		this.opponentTypeB = opponentTypeB;
	}

}
