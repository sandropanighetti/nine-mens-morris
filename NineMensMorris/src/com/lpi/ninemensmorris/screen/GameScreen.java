package com.lpi.ninemensmorris.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.lpi.ninemensmorris.Assets;
import com.lpi.ninemensmorris.MatchConfiguration;
import com.lpi.ninemensmorris.Messages;
import com.lpi.ninemensmorris.NineMensMorris;
import com.lpi.ninemensmorris.game.MatchInfo;
import com.lpi.ninemensmorris.game.World;
import com.lpi.ninemensmorris.game.World.GameState;
import com.lpi.ninemensmorris.game.WorldRender;
import com.lpi.ninemensmorris.gui.ClickableImageItem;
import com.lpi.ninemensmorris.gui.TextItem;

/**
 * @author Sandro Panighetti - 16-gen-2013 NineMensMorris
 */
public class GameScreen extends AbstractScreen {
	//
	@SuppressWarnings("unused")
	private MatchConfiguration matchConfiguration;

	public MatchInfo matchInfo;

	private World world;

	private WorldRender worldRender;

	// Gui
	private TextItem titleText;
	private ClickableImageItem menuButton;
//	private ClickableImageItem restartButton;

	/**
	 * @param nineMensMorris
	 * @param matchConfiguration
	 */
	public GameScreen(NineMensMorris nineMensMorris,
			MatchConfiguration matchConfiguration) {
		super(nineMensMorris);
		// this.matchConfiguration = matchConfiguration;
		this.world = new World(this, matchConfiguration);
		this.worldRender = new WorldRender(batcher, world);
		this.matchInfo = new MatchInfo();
		this.matchInfo.setOpponentTypeA(matchConfiguration.getOpponentTypeA());
		this.matchInfo.setOpponentTypeB(matchConfiguration.getOpponentTypeB());
		
		initTexts();
		initButtons();

		// Gdx.app.debug("MatchConfiguration", matchConfiguration.toString());

		// Gdx.app.debug("GameScreen", "finished");

		world.gameState = GameState.RUNNING;
	}

	private void initTexts() {
		titleText = new TextItem(Messages.Game.WHITE_TURN, Color.WHITE, Assets.font,
				0.7f, SCREEN_WIDTH / 2, SCREEN_HEIGHT - 50);
	}

	private void initButtons() {
		menuButton = new ClickableImageItem(Assets.leftArrowTexture,
				SCREEN_WIDTH * 2.5f / 16.0f, SCREEN_HEIGHT * 2.5f / 23.0f,
				SCREEN_WIDTH * 3.0f / 16.0f, SCREEN_HEIGHT * 3.0f / 23.0f) {

			@Override
			public void action() {
				nineMensMorris.setScreen(new MenuScreen(nineMensMorris));
			}
		};

//		restartButton = new ClickableImageItem(Assets.rightArrowTexture,
//				SCREEN_WIDTH * 2.5f / 8f, SCREEN_HEIGHT
//						- (SCREEN_HEIGHT * 2.0f / 11.0f),
//				SCREEN_WIDTH * 2.5f / 16.0f, SCREEN_HEIGHT * 2.5f / 23.0f) {
//
//			@Override
//			public void action() {
//				nineMensMorris.setScreen(new GameScreen(nineMensMorris,
//						matchConfiguration));
//			}
//		};
	}

	@Override
	public void render(float delta) {
		update(delta);
		draw();
	}

	private void update(float delta) {

		switch (world.gameState) {
		case RUNNING:
			world.update(delta);
			updateRunning(delta);
			break;
		case PAUSED:
			updatePaused(delta);
			break;
		case GAME_OVER:
			updateGameOver(delta);
			break;
		}
	}

	private void draw() {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		worldRender.render();

		// Gdx.app.debug("state", state.name());
		camera.update();
		batcher.setProjectionMatrix(camera.combined);
		batcher.enableBlending();
		batcher.begin();

		switch (world.gameState) {
		case RUNNING:
			presentRunning();
			break;
		case PAUSED:
			presentPaused();
			break;
		case GAME_OVER:
			presentGameOver();
			break;
		}

		drawFPS();
		drawMouseCoordinates();
		batcher.end();
	}

	// Present
	private void presentRunning() {
		drawText(titleText);
		drawImage(menuButton);
//		drawImage(restartButton);
		// Gdx.app.debug("present", "running");
	}

	private void presentPaused() {

	}

	private void presentGameOver() {
		drawText(titleText);
		drawImage(menuButton);
		
//		drawImage(restartButton);
	}

	// Update
	private void updateRunning(float delta) {
		if (Gdx.input.justTouched()) {
			updateTouchPoint();

			menuButton.onTouchDown(touchPoint.x, touchPoint.y);

		}
	}

	private void updatePaused(float delta) {

	}

	private void updateGameOver(float delta) {

		if (Gdx.input.justTouched()) {
			updateTouchPoint();

			menuButton.onTouchDown(touchPoint.x, touchPoint.y);

		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
//		titleText.text=Messages.Game.
	}

	@Override
	public void hide() {
//		Gdx.app.debug("exit","gamescreen");
	}

	/**
	 * @return the titleText
	 */
	public TextItem getTitleText() {
		return titleText;
	}

	/**
	 * @param titleText
	 *            the titleText to set
	 */
	public void setTitleText(TextItem titleText) {
		this.titleText = titleText;
	}

	/**
	 * @param text
	 *            to set
	 */
	public void setTitleText(String text) {
		titleText.text = text;
	}

}
