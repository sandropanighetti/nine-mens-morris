package com.lpi.ninemensmorris.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.lpi.ninemensmorris.Assets;
import com.lpi.ninemensmorris.NineMensMorris;
import com.lpi.ninemensmorris.Settings;
import com.lpi.ninemensmorris.gui.ImageItem;
import com.lpi.ninemensmorris.gui.TextItem;

/**
 * @author Sandro Panighetti - 16-gen-2013 NineMensMorris
 */
public abstract class AbstractScreen implements Screen {

	public static final float SCREEN_WIDTH = 640.0f;
	public static final float SCREEN_HEIGHT = 960.0f;

	protected NineMensMorris nineMensMorris;

	protected OrthographicCamera camera;
	protected SpriteBatch batcher;
	public Vector3 touchPoint;

	private TextBounds textBounds;

	/**
	 * @param nineMensMorris
	 */
	public AbstractScreen(NineMensMorris nineMensMorris) {
		this.nineMensMorris = nineMensMorris;

		camera = new OrthographicCamera(SCREEN_WIDTH, SCREEN_HEIGHT);
		camera.position.set(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 0);
		batcher = new SpriteBatch();
		touchPoint = new Vector3();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

	/**
	 * @param text
	 */
	protected void drawText(TextItem text) {
		if(!text.isVisible())
			return;
		text.getFont().setColor(text.getColor());
		text.getFont().setScale(text.textScale);
//		text.getFont().draw(batcher, text.text, text.bounds.x,
//				text.bounds.y + text.bounds.height);
		text.getFont().drawWrapped(batcher, text.text, text.bounds.x, text.bounds.y + text.bounds.height, text.bounds.width);

	}

	/**
	 * @param image
	 */
	protected void drawImage(ImageItem image) {
		batcher.draw(image.getTexture(), image.bounds.x, image.bounds.y,
				image.bounds.width / 2, image.bounds.height / 2,
				image.bounds.width, image.bounds.height, 1, 1, 0);
	}

	/**
	 * Draw FPS to the bottom right of the screen
	 */
	protected void drawFPS() {
		if(!nineMensMorris.getSettings().isDebuggingEnabled())
			return;
		float scale = 0.2f;
		Assets.font.setScale(scale);
		textBounds = Assets.font.getBounds(Integer.toString(Gdx.graphics
				.getFramesPerSecond()));
		Assets.font.setColor(Color.WHITE);
		Assets.font.draw(batcher,
				Integer.toString(Gdx.graphics.getFramesPerSecond()),
				SCREEN_WIDTH - textBounds.width, textBounds.height);

		// Gdx.app.debug("fps draw", "ok");
	}
	
	/**
	 * Draw MouseCoordinates 
	 */
	protected void drawMouseCoordinates() {
		if(!nineMensMorris.getSettings().isDebuggingEnabled())
			return;
		float scale = 0.2f;
		Assets.font.setScale(scale);
//		textBounds = Assets.font.getBounds(Integer.toString(Gdx.graphics
//				.getFramesPerSecond()));
		updateTouchPoint();
		Assets.font.drawMultiLine(batcher, "X: " + touchPoint.x+"\nY: " + touchPoint.y, 10, 50);

		// Gdx.app.debug("fps draw", "ok");
	}	
	/**
	 * Draw the default background
	 */
	protected void drawBackground(){
		batcher.draw(Assets.woodBackgroundTexture,
				camera.position.x - SCREEN_WIDTH / 2,
				camera.position.y - SCREEN_HEIGHT / 2,
				SCREEN_WIDTH, SCREEN_HEIGHT);
	}
	

	public void updateTouchPoint() {
		camera.unproject(touchPoint.set(Gdx.input.getX(0), Gdx.input.getY(0), 0));
	}

	/**
	 * @return the nineMensMorris
	 */
	public NineMensMorris getNineMensMorris() {
		return nineMensMorris;
	}

	/**
	 * @param nineMensMorris the nineMensMorris to set
	 */
	public void setNineMensMorris(NineMensMorris nineMensMorris) {
		this.nineMensMorris = nineMensMorris;
	}

}
