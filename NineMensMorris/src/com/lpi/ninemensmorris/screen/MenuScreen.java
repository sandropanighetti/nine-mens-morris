package com.lpi.ninemensmorris.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.lpi.ninemensmorris.Assets;
import com.lpi.ninemensmorris.Messages;
import com.lpi.ninemensmorris.NineMensMorris;
import com.lpi.ninemensmorris.gui.ClickableTextItem;
import com.lpi.ninemensmorris.gui.TextItem;

/**
 * @author Sandro Panighetti - 16-gen-2013 NineMensMorris
 */
public class MenuScreen extends AbstractScreen {

	private TextItem title;
	
	private ClickableTextItem restoreMatchButton;
	private ClickableTextItem playButton;
	private ClickableTextItem settingsButton;
	private ClickableTextItem helpButton;

	/**
	 * @param nineMensMorris
	 */
	public MenuScreen(NineMensMorris nineMensMorris) {
		super(nineMensMorris);

		initTexts();
		initButtons();
		if (nineMensMorris.getGameScreen() == null)
			restoreMatchButton.setVisible(false);
	}


	@Override
	public void show() {

	}

	@Override
	public void render(float delta) {
		update(delta);
		draw();
	}

	/**
	 * @param delta
	 */
	private void update(float delta) {
		if (Gdx.input.justTouched()) {
			 updateTouchPoint();

			// Buttons
			restoreMatchButton.onTouchDown(touchPoint.x, touchPoint.y);
			playButton.onTouchDown(touchPoint.x, touchPoint.y);
			settingsButton.onTouchDown(touchPoint.x, touchPoint.y);
			helpButton.onTouchDown(touchPoint.x, touchPoint.y);

		} else {

			updateTouchPoint();
			restoreMatchButton.onMouseMove(touchPoint.x, touchPoint.y);
			playButton.onMouseMove(touchPoint.x, touchPoint.y);
			settingsButton.onMouseMove(touchPoint.x, touchPoint.y);
			helpButton.onMouseMove(touchPoint.x, touchPoint.y);
		}
	}

	/**
	 * 
	 */
	private void draw() {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		camera.update();
		batcher.setProjectionMatrix(camera.combined);

		batcher.enableBlending();
		batcher.begin();

		drawBackground();

		drawTexts();
		drawFPS();

		batcher.end();
	}

	private void initTexts() {
		title = new TextItem(Messages.Home.TITLE, Color.WHITE, Assets.font,
				0.9f, SCREEN_WIDTH / 2, SCREEN_HEIGHT - 150);
	}
	/**
	 * 
	 */
	private void initButtons() {
		float textScale = 0.9f;
		float spacing = 80.0f;
		Assets.font.setScale(textScale);

		// Buttons
		restoreMatchButton = new ClickableTextItem(
				Messages.Home.RESTORE_MATCH,
				Color.WHITE,
				Color.ORANGE,
				Assets.font,
				textScale,
				SCREEN_WIDTH / 2.0f,
				SCREEN_HEIGHT
						- (SCREEN_HEIGHT / 7.0f * (2.0f + 0.9f * 0.0f) + spacing)) {

			@Override
			public void action() {
				if (nineMensMorris.getGameScreen() != null)
					nineMensMorris.setScreen(nineMensMorris.getGameScreen());
			}
		};
		
		textScale = restoreMatchButton.textScale;
		playButton = new ClickableTextItem(Messages.Home.PLAY, Color.WHITE,
				Color.ORANGE, Assets.font, textScale, SCREEN_WIDTH / 2.0f,
				SCREEN_HEIGHT
						- (SCREEN_HEIGHT / 7.0f * (2.0f + 0.9f) + spacing)) {

			@Override
			public void action() {
				nineMensMorris
						.setScreen(nineMensMorris.getSettingMatchScreen());
			}
		};

		settingsButton = new ClickableTextItem(
				Messages.Home.SETTINGS,
				Color.WHITE,
				Color.ORANGE,
				Assets.font,
				textScale,
				SCREEN_WIDTH / 2.0f,
				SCREEN_HEIGHT
						- (SCREEN_HEIGHT / 7.0f * (2.0f + 0.9f * 2.0f) + spacing)) {

			@Override
			public void action() {
				nineMensMorris.setScreen(new OptionScreen(nineMensMorris));
			}
		};

		helpButton = new ClickableTextItem(
				Messages.Home.HELP,
				Color.WHITE,
				Color.ORANGE,
				Assets.font,
				textScale,
				SCREEN_WIDTH / 2.0f,
				SCREEN_HEIGHT
						- (SCREEN_HEIGHT / 7.0f * (2.0f + 0.9f * 3.0f) + spacing)) {

			@Override
			public void action() {
				nineMensMorris.setScreen(new HelpScreen(nineMensMorris));
			}
		};
	}

	/**
	 * 
	 */
	private void drawTexts() {
		
		drawText(title);
		
		drawText(playButton);
		drawText(settingsButton);
		drawText(helpButton);
		drawText(restoreMatchButton);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

}
