package com.lpi.ninemensmorris.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.lpi.ninemensmorris.Assets;
import com.lpi.ninemensmorris.MatchConfiguration;
import com.lpi.ninemensmorris.Messages;
import com.lpi.ninemensmorris.NineMensMorris;
import com.lpi.ninemensmorris.game.Opponent.OpponentType;
import com.lpi.ninemensmorris.gui.ClickableImageItem;
import com.lpi.ninemensmorris.gui.ImageItem;
import com.lpi.ninemensmorris.gui.TextItem;

/**
 * @author Sandro Panighetti - 16-gen-2013 NineMensMorris
 */
public class SettingMatchScreen extends AbstractScreen {

	private TextItem title;

	private ClickableImageItem backButton;
	private ClickableImageItem nextButton;

	private ClickableImageItem userAHuman;
	private ClickableImageItem userAComputer;
	private ClickableImageItem userBHuman;
	private ClickableImageItem userBComputer;

	private ImageItem userASelect;
	private ImageItem userBSelect;

	private MatchConfiguration matchConfiguration;

	/**
	 * @param nineMensMorris
	 */
	public SettingMatchScreen(NineMensMorris nineMensMorris) {
		super(nineMensMorris);

		matchConfiguration = new MatchConfiguration(OpponentType.HUMAN,
				OpponentType.COMPUTER);

		initTexts();
		initButtons();
		initImages();

	}

	@Override
	public void show() {
		title.text = Messages.SettingMatch.TITLE;
		title.resetPosition();
		
	}

	@Override
	public void render(float delta) {
		update(delta);
		draw();
	}

	private void update(float delta) {
		if (Gdx.input.justTouched()) {
			updateTouchPoint();

			// Buttons
			backButton.onTouchDown(touchPoint.x, touchPoint.y);
			nextButton.onTouchDown(touchPoint.x, touchPoint.y);

			userAHuman.onTouchDown(touchPoint.x, touchPoint.y);
			userAComputer.onTouchDown(touchPoint.x, touchPoint.y);
			userBHuman.onTouchDown(touchPoint.x, touchPoint.y);
			userBComputer.onTouchDown(touchPoint.x, touchPoint.y);
		}
	}

	/**
	 * 
	 */
	private void draw() {

		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		camera.update();
		batcher.setProjectionMatrix(camera.combined);
		batcher.enableBlending();
		batcher.begin();

		drawBackground();
		drawTexts();
		drawImages();

		// batcher.draw(Assets.leftArrowTexture, 20, 50, 0, 0, 40, 40, 1, 1, 0);

		batcher.end();
	}

	private void drawTexts() {
		drawText(title);
	}

	/**
	 * 
	 */
	private void drawImages() {

		drawImage(userASelect);
		drawImage(userBSelect);

		drawImage(backButton);
		drawImage(nextButton);

		drawImage(userAHuman);
		drawImage(userAComputer);
		drawImage(userBHuman);
		drawImage(userBComputer);
		drawFPS();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	private void initImages() {
		userASelect = new ImageItem(Assets.selectTexture, userAHuman.bounds.x
				+ userAHuman.bounds.width / 2, userAHuman.bounds.y
				+ userAHuman.bounds.height / 2,
				userAHuman.bounds.width * 1.3f, userAHuman.bounds.height * 1.3f);

		userBSelect = new ImageItem(Assets.selectTexture,
				userBComputer.bounds.x + userBComputer.bounds.width / 2,
				userBComputer.bounds.y + userBComputer.bounds.height / 2,
				userBComputer.bounds.width * 1.3f,
				userBComputer.bounds.height * 1.3f);

	}

	private void initTexts() {

		title = new TextItem(Messages.SettingMatch.TITLE, Color.WHITE, Assets.font,
				0.7f, SCREEN_WIDTH / 2.0f, SCREEN_HEIGHT - 50.0f);
	}

	private void initButtons() {
		backButton = new ClickableImageItem(Assets.leftArrowTexture,
				SCREEN_WIDTH * 2.5f / 16.0f, SCREEN_HEIGHT * 2.5f / 23.0f,
				SCREEN_WIDTH * 3.0f / 16.0f, SCREEN_HEIGHT * 3.0f / 23.0f) {

			@Override
			public void action() {
				nineMensMorris.setScreen(new MenuScreen(nineMensMorris));
			}
		};
		nextButton = new ClickableImageItem(Assets.rightArrowTexture,
				SCREEN_WIDTH * 13.5f / 16.0f, SCREEN_HEIGHT * 2.5f / 23.0f,
				SCREEN_WIDTH * 3.0f / 16.0f, SCREEN_HEIGHT * 3.0f / 23.0f) {

			@Override
			public void action() {
				nineMensMorris.setGameScreen(new GameScreen(nineMensMorris,
						matchConfiguration));
				
				nineMensMorris.setScreen(nineMensMorris.getGameScreen());
			}
		};
		userAHuman = new ClickableImageItem(Assets.humanTexture,
				SCREEN_WIDTH * 5.5f / 16.0f, SCREEN_HEIGHT * 16.0f / 23.0f,
				SCREEN_WIDTH * 5.0f / 16.0f, SCREEN_HEIGHT * 5.0f / 23.0f) {

			@Override
			public void action() {
				userASelect.bounds.set(userAHuman.bounds.x
						+ userAHuman.bounds.width / 2
						- userASelect.bounds.width / 2, userAHuman.bounds.y
						+ userAHuman.bounds.height / 2
						- userASelect.bounds.height / 2,
						userASelect.bounds.width, userASelect.bounds.height);
				matchConfiguration.setOpponentTypeA(OpponentType.HUMAN);
			}
		};

		userAComputer = new ClickableImageItem(Assets.computerTexture,
				SCREEN_WIDTH * 11.5f / 16.0f, SCREEN_HEIGHT * 16.0f / 23.0f,
				SCREEN_WIDTH * 5.0f / 16.0f, SCREEN_HEIGHT * 5.0f / 23.0f) {

			@Override
			public void action() {
				userASelect.bounds.set(userAComputer.bounds.x
						+ userAComputer.bounds.width / 2
						- userASelect.bounds.width / 2, userAComputer.bounds.y
						+ userAComputer.bounds.height / 2
						- userASelect.bounds.height / 2,
						userASelect.bounds.width, userASelect.bounds.height);
				matchConfiguration.setOpponentTypeA(OpponentType.COMPUTER);
			}
		};

		userBHuman = new ClickableImageItem(Assets.humanTexture,
				SCREEN_WIDTH * 5.5f / 16.0f, SCREEN_HEIGHT * 8.5f / 23.0f,
				SCREEN_WIDTH * 5.0f / 16.0f, SCREEN_HEIGHT * 5.0f / 23.0f) {

			@Override
			public void action() {
				userBSelect.bounds.set(userBHuman.bounds.x
						+ userBHuman.bounds.width / 2
						- userBSelect.bounds.width / 2, userBHuman.bounds.y
						+ userBHuman.bounds.height / 2
						- userBSelect.bounds.height / 2,
						userBSelect.bounds.width, userBSelect.bounds.height);
				matchConfiguration.setOpponentTypeB(OpponentType.HUMAN);
			}
		};
		userBComputer = new ClickableImageItem(Assets.computerTexture,
				SCREEN_WIDTH * 11.5f / 16.0f, SCREEN_HEIGHT * 8.5f / 23.0f,
				SCREEN_WIDTH * 5.0f / 16.0f, SCREEN_HEIGHT * 5.0f / 23.0f) {

			@Override
			public void action() {
				userBSelect.bounds.set(userBComputer.bounds.x
						+ userBComputer.bounds.width / 2
						- userBSelect.bounds.width / 2, userBComputer.bounds.y
						+ userBComputer.bounds.height / 2
						- userBSelect.bounds.height / 2,
						userBSelect.bounds.width, userBSelect.bounds.height);
				matchConfiguration.setOpponentTypeB(OpponentType.COMPUTER);
			}
		};
	}

}
