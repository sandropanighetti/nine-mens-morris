package com.lpi.ninemensmorris.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.math.Rectangle;
import com.lpi.ninemensmorris.Assets;
import com.lpi.ninemensmorris.Messages;
import com.lpi.ninemensmorris.NineMensMorris;
import com.lpi.ninemensmorris.Statistics;
import com.lpi.ninemensmorris.game.MatchInfo;
import com.lpi.ninemensmorris.game.Opponent.OpponentColor;
import com.lpi.ninemensmorris.game.Opponent.OpponentType;
import com.lpi.ninemensmorris.gui.ClickableImageItem;
import com.lpi.ninemensmorris.gui.ClickableTextItem;
import com.lpi.ninemensmorris.gui.ImageItem;
import com.lpi.ninemensmorris.gui.TextItem;

public class MatchInfoScreen extends AbstractScreen {

	private MatchInfo matchInfo;

	private TextItem titleLabel;
	private TextItem timeLabel;
	private TextItem timeDataLabel;
	private TextItem movesLabel;
	private TextItem movesDataLabel;

	private ImageItem whiteOpponentDataImage;
	private ImageItem blackOpponentDataImage;
	

	private ImageItem whiteImage;
	private ImageItem blackImage;

	private ClickableTextItem menuButton;
	private ClickableTextItem restartButton;

	public MatchInfoScreen(NineMensMorris nineMensMorris, MatchInfo matchInfo) {
		super(nineMensMorris);
		this.matchInfo = matchInfo;
		Statistics.addMatchInfo(matchInfo);
		initTexts();
		initImages();
		initButtons();

		if (nineMensMorris.getSettings().isMusicEnabled()){
			Assets.soundTrackMusic.stop();
			Assets.finalMusic.play();
		}

	}
	
	@Override
	public void show() {
//		titleLabel = Messages.Game.MatchInfo.
		timeLabel.text = Messages.Game.MatchInfo.TIME_LABEL;
		movesLabel.text = Messages.Game.MatchInfo.MOVES_LABEL;
		
		timeLabel.resetPosition();
		movesLabel.resetPosition();
	}

	@Override
	public void render(float delta) {
		update(delta);
		draw();
	}

	private void update(float delta) {

		if (Gdx.input.justTouched()) {
			updateTouchPoint();

			// Buttons
			menuButton.onTouchDown(touchPoint.x, touchPoint.y);
			restartButton.onTouchDown(touchPoint.x, touchPoint.y);
			// nextButton.onTouchDown(touchPoint.x, touchPoint.y);
		} else {
			updateTouchPoint();

			// Buttons
			menuButton.onMouseMove(touchPoint.x, touchPoint.y);
			restartButton.onMouseMove(touchPoint.x, touchPoint.y);
		}
	}

	/**
	 * 
	 */
	private void draw() {

		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		camera.update();
		batcher.setProjectionMatrix(camera.combined);
		batcher.enableBlending();
		batcher.begin();

		drawBackground();
		drawTexts();
		drawImages();

		// batcher.draw(Assets.leftArrowTexture, 20, 50, 0, 0, 40, 40, 1, 1, 0);

		batcher.end();
	}

	private void drawTexts() {
		drawText(titleLabel);
		drawText(timeLabel);
		drawText(timeDataLabel);
		drawText(movesLabel);
		drawText(movesDataLabel);
//		drawText(whiteOpponentDataLabel);
//		drawText(blackOpponentDataLabel);

		drawText(menuButton);
		drawText(restartButton);
	}

	/**
	 * 
	 */
	private void drawImages() {

		drawImage(whiteImage);
		drawImage(blackImage);
		drawImage(whiteOpponentDataImage);
		drawImage(blackOpponentDataImage);
		drawFPS();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	float spacing = 90.0f;

	private void initImages() {

		whiteImage = new ImageItem(Assets.whiteManTexture, SCREEN_WIDTH / 4,
				SCREEN_HEIGHT * 2.0f / 3.0f - spacing * 3.0f,
				SCREEN_WIDTH / 8.0f, SCREEN_WIDTH / 8.0f);

		blackImage = new ImageItem(Assets.blackManTexture, SCREEN_WIDTH / 4,
				SCREEN_HEIGHT * 2.0f / 3.0f - spacing * 4.0f,
				SCREEN_WIDTH / 8.0f, SCREEN_WIDTH / 8.0f);

		whiteOpponentDataImage = new ImageItem(null, 
				SCREEN_WIDTH * 3.0f / 4, SCREEN_HEIGHT * 2.0f / 3.0f - spacing
						* 3.0f, 100, 100);
		
		blackOpponentDataImage = new ImageItem(null, 
				SCREEN_WIDTH * 3.0f / 4, SCREEN_HEIGHT * 2.0f / 3.0f - spacing
						* 4.0f, 100, 100);
		
		if(matchInfo.getOpponentTypeA()==OpponentType.HUMAN)
			whiteOpponentDataImage.setTexture(Assets.humanTexture);
		else
			whiteOpponentDataImage.setTexture(Assets.computerTexture);
		
		if(matchInfo.getOpponentTypeB()==OpponentType.HUMAN)
			blackOpponentDataImage.setTexture(Assets.humanTexture);
		else
			blackOpponentDataImage.setTexture(Assets.computerTexture);

		// userASelect = new ImageItem(Assets.selectTexture, userAHuman.bounds.x
		// + userAHuman.bounds.width / 2, userAHuman.bounds.y
		// + userAHuman.bounds.height / 2,
		// userAHuman.bounds.width * 1.3f, userAHuman.bounds.height * 1.3f);

	}

	private void initTexts() {
		String titleText = "";
		if (matchInfo.getOpponentColorWinner() == OpponentColor.WHITE)
			titleText = Messages.Game.MatchInfo.WHITE_WIN;
		else
			titleText = Messages.Game.MatchInfo.BLACK_WIN;

		titleLabel = new TextItem(titleText, Color.WHITE, Assets.font, 0.7f,
				SCREEN_WIDTH / 2.0f, SCREEN_HEIGHT - 80.0f);

		timeLabel = new TextItem(Messages.Game.MatchInfo.TIME_LABEL,
				Color.WHITE, Assets.font, 0.5f, SCREEN_WIDTH / 4,
				SCREEN_HEIGHT * 2.0f / 3.0f);

		String timeData = matchInfo.getTime() + "";
		timeData = timeData.substring(0, 5);
		timeData += " s";
		timeDataLabel = new TextItem(timeData, Color.WHITE, Assets.font, 0.5f,
				SCREEN_WIDTH * 3.0f / 4, SCREEN_HEIGHT * 2.0f / 3.0f);

		movesLabel = new TextItem(Messages.Game.MatchInfo.MOVES_LABEL,
				Color.WHITE, Assets.font, 0.5f, SCREEN_WIDTH / 4, SCREEN_HEIGHT
						* 2.0f / 3.0f - spacing);

		movesDataLabel = new TextItem(matchInfo.getMoves() + "", Color.WHITE,
				Assets.font, 0.5f, SCREEN_WIDTH * 3.0f / 4, SCREEN_HEIGHT
						* 2.0f / 3.0f - spacing);

//		whiteOpponentDataLabel = new TextItem(matchInfo.getOpponentTypeA()
//				.toString(), Color.WHITE, Assets.font, 0.5f,
//				SCREEN_WIDTH * 2.5f / 4, SCREEN_HEIGHT * 2.0f / 3.0f - spacing
//						* 3.0f);
//
//		blackOpponentDataLabel = new TextItem(matchInfo.getOpponentTypeB()
//				.toString(), Color.WHITE, Assets.font, 0.5f,
//				SCREEN_WIDTH * 2.5f / 4, SCREEN_HEIGHT * 2.0f / 3.0f - spacing
//						* 4.0f);

	}

	private void initButtons() {

		menuButton = new ClickableTextItem(Messages.Game.MatchInfo.MENU_BUTTON,
				Color.WHITE, Color.ORANGE, Assets.font, 0.6f,
				SCREEN_WIDTH * 4.0f / 16.0f, SCREEN_HEIGHT * 2.5f / 23.0f) {

			@Override
			public void action() {
				
				if (nineMensMorris.getSettings().isMusicEnabled()){
					Assets.soundTrackMusic.play();
					Assets.finalMusic.stop();
				}
				nineMensMorris.setGameScreen(null);
				nineMensMorris.setScreen(new MenuScreen(nineMensMorris));
			}
		};

		restartButton = new ClickableTextItem(
				Messages.Game.MatchInfo.RESTART_BUTTON, Color.WHITE,
				Color.ORANGE, Assets.font, 0.6f, SCREEN_WIDTH * 12.0f / 16.0f,
				SCREEN_HEIGHT * 2.5f / 23.0f) {

			@Override
			public void action() {
				if (nineMensMorris.getSettings().isMusicEnabled()){
					Assets.soundTrackMusic.play();
					Assets.finalMusic.stop();
				}
				nineMensMorris.setGameScreen(null);
				nineMensMorris
						.setScreen(nineMensMorris.getSettingMatchScreen());
			}
		};

	}


}
