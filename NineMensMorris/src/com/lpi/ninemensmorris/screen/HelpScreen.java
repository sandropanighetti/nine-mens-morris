package com.lpi.ninemensmorris.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.lpi.ninemensmorris.Assets;
import com.lpi.ninemensmorris.Messages;
import com.lpi.ninemensmorris.NineMensMorris;
import com.lpi.ninemensmorris.gui.ClickableImageItem;
import com.lpi.ninemensmorris.gui.TextItem;

public class HelpScreen extends AbstractScreen{

	private TextItem title;
	
	private TextItem context;

	private ClickableImageItem backButton;
	
	
	public HelpScreen(NineMensMorris nineMensMorris) {
		super(nineMensMorris);
		
		initTexts();
		initButtons();
	}

	private void initTexts(){
		title = new TextItem(Messages.Help.TITLE, Color.WHITE, Assets.font,
				0.7f, SCREEN_WIDTH / 2.0f, SCREEN_HEIGHT - 50.0f);
		
		context = new TextItem(Messages.Help.CONTEXT, Color.WHITE, Assets.font,
				0.35f, 50, SCREEN_HEIGHT-150,false,false);
		context.bounds.width=SCREEN_WIDTH-50;
	}
	
	private void initButtons(){
		backButton = new ClickableImageItem(Assets.leftArrowTexture,
				SCREEN_WIDTH * 3.5f / 16.0f, SCREEN_HEIGHT * 2.5f / 23.0f,
				SCREEN_WIDTH * 3.0f / 16.0f, SCREEN_HEIGHT * 3.0f / 23.0f) {

			@Override
			public void action() {
				nineMensMorris.setScreen(new MenuScreen(nineMensMorris));
			}
		};
	}
	
	@Override
	public void render(float delta) {
		update(delta);
		draw();
	}

	private void draw() {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		camera.update();
		batcher.setProjectionMatrix(camera.combined);
		batcher.enableBlending();
		batcher.begin();

		drawBackground();
		drawTexts();
		drawImages();

		// batcher.draw(Assets.leftArrowTexture, 20, 50, 0, 0, 40, 40, 1, 1, 0);

		batcher.end();
	}

	private void drawImages() {
		drawImage(backButton);
	}

	private void drawTexts() {
		drawText(title);
		drawText(context);
	}

	private void update(float delta) {
		if (Gdx.input.justTouched()) {
			updateTouchPoint();

			// Buttons
			backButton.onTouchDown(touchPoint.x, touchPoint.y);

		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

}
