package com.lpi.ninemensmorris.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.lpi.ninemensmorris.Assets;
import com.lpi.ninemensmorris.Messages;
import com.lpi.ninemensmorris.Messages.Language;
import com.lpi.ninemensmorris.NineMensMorris;
import com.lpi.ninemensmorris.Settings;
import com.lpi.ninemensmorris.gui.ClickableImageItem;
import com.lpi.ninemensmorris.gui.ClickableTextItem;
import com.lpi.ninemensmorris.gui.TextItem;

public class OptionScreen extends AbstractScreen {

	private TextItem title;

	private TextItem soundLabel;
	private ClickableImageItem soundImage;

	private TextItem musicLabel;
	private ClickableImageItem musicImage;

	private TextItem allTrisKillLabel;
	private ClickableImageItem allTrisKillImage;

	private TextItem languageLabel;
	private ClickableTextItem languageText;

	private ClickableImageItem backButton;

	public OptionScreen(NineMensMorris nineMensMorris) {
		super(nineMensMorris);
		// TODO Auto-generated constructor stub

		initTexts();
		initButtons();
		initImages();
	}

	@Override
	public void show() {
		title.text = Messages.Options.TITLE;
		soundLabel.text = Messages.Options.SOUND_LABEL;
		musicLabel.text = Messages.Options.MUSIC_LABEL;
		allTrisKillLabel.text = Messages.Options.ALL_TRIS_KILL_ENABLED_LABEL;
		languageLabel.text = Messages.Options.LANGUAGE_LABEL;
		
		title.resetPosition();
		soundLabel.resetPosition();
		musicLabel.resetPosition();
		allTrisKillLabel.resetPosition();
		languageLabel.resetPosition();
	}

	@Override
	public void render(float delta) {
		update(delta);
		draw();
	}

	private void update(float delta) {
		if (Gdx.input.justTouched()) {
			updateTouchPoint();

			// Buttons
			backButton.onTouchDown(touchPoint.x, touchPoint.y);

			soundImage.onTouchDown(touchPoint.x, touchPoint.y);
			musicImage.onTouchDown(touchPoint.x, touchPoint.y);

			allTrisKillImage.onTouchDown(touchPoint.x, touchPoint.y);

			languageText.onTouchDown(touchPoint.x, touchPoint.y);
			// nextButton.onTouchDown(touchPoint.x, touchPoint.y);
			//
			// userAHuman.onTouchDown(touchPoint.x, touchPoint.y);
			// userAComputer.onTouchDown(touchPoint.x, touchPoint.y);
			// userBHuman.onTouchDown(touchPoint.x, touchPoint.y);
			// userBComputer.onTouchDown(touchPoint.x, touchPoint.y);
		}
	}

	/**
 * 
 */
	private void draw() {

		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		camera.update();
		batcher.setProjectionMatrix(camera.combined);
		batcher.enableBlending();
		batcher.begin();

		drawBackground();
		drawTexts();
		drawImages();

		// batcher.draw(Assets.leftArrowTexture, 20, 50, 0, 0, 40, 40, 1, 1, 0);

		batcher.end();
	}

	private void drawTexts() {
		drawText(title);
		drawText(soundLabel);
		drawText(allTrisKillLabel);
		drawText(musicLabel);

		drawText(languageLabel);
		drawText(languageText);
	}

	/**
 * 
 */
	private void drawImages() {

		drawImage(backButton);
		drawImage(soundImage);
		drawImage(allTrisKillImage);
		drawImage(musicImage);

		drawFPS();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	private void initImages() {
		// userASelect = new ImageItem(Assets.selectTexture, userAHuman.bounds.x
		// + userAHuman.bounds.width / 2, userAHuman.bounds.y
		// + userAHuman.bounds.height / 2,
		// userAHuman.bounds.width * 1.3f, userAHuman.bounds.height * 1.3f);
		//
		// userBSelect = new ImageItem(Assets.selectTexture,
		// userBComputer.bounds.x + userBComputer.bounds.width / 2,
		// userBComputer.bounds.y + userBComputer.bounds.height / 2,
		// userBComputer.bounds.width * 1.3f,
		// userBComputer.bounds.height * 1.3f);

	}

	private void initTexts() {

		title = new TextItem(Messages.Options.TITLE, Color.WHITE, Assets.font,
				0.7f, SCREEN_WIDTH / 2.0f, SCREEN_HEIGHT - 50.0f);

		soundLabel = new TextItem(Messages.Options.SOUND_LABEL, Color.WHITE,
				Assets.font, 0.5f, SCREEN_WIDTH * 0.4f / 4.0f,
				SCREEN_HEIGHT * 2.0f / 3.0f, false);

		musicLabel = new TextItem(Messages.Options.MUSIC_LABEL, Color.WHITE,
				Assets.font, 0.5f, SCREEN_WIDTH * 0.4f / 4.0f,
				SCREEN_HEIGHT * 2.0f / 3.0f - 90.0f, false);

		allTrisKillLabel = new TextItem(
				Messages.Options.ALL_TRIS_KILL_ENABLED_LABEL, Color.WHITE,
				Assets.font, 0.5f, SCREEN_WIDTH * 0.4f / 4.0f, SCREEN_HEIGHT
						* 2.0f / 3.0f - 90.0f * 2.0f, false);

		languageLabel = new TextItem(Messages.Options.LANGUAGE_LABEL,
				Color.WHITE, Assets.font, 0.5f, SCREEN_WIDTH * 0.4f / 4.0f,
				SCREEN_HEIGHT * 2.0f / 3.0f - 90.0f * 3.0f, false);

		
		languageText = new ClickableTextItem(nineMensMorris.getSettings().getLanguage().name(),
				Color.WHITE, Color.ORANGE, Assets.font, 0.5f,
				SCREEN_WIDTH * 2.3f / 3.0f, SCREEN_HEIGHT * 2.0f / 3.0f - 90.0f
						* 3.0f,true) {
			

			Language[] languages = Messages.getLanguages();
			
			@Override
			public void action() {
//				Gdx.app.debug("languageText", nineMensMorris.getSettings().getLanguage().name());
				mainLoop: for(int i=0;i<languages.length;i++){
					if(languages[i]==nineMensMorris.getSettings().getLanguage()){
						Language lang=languages[(i+1)%languages.length]; 
						this.text =lang.name();

//						Gdx.app.debug("lang", lang.name());
						nineMensMorris.getSettings().setLanguage(lang);
						nineMensMorris.getSettings().save();
						Messages.load(lang);
						break mainLoop;
					}
				}
//				show();
				nineMensMorris.setScreen(new OptionScreen(nineMensMorris));
//				this.text = languages[b].name();
//				Gdx.app.debug("languageText", nineMensMorris.getSettings().getLanguage().name());
			}
		};
		// Messages.Options.LANGUAGE_LABEL, Color.WHITE,
		// Assets.font, 0.5f, SCREEN_WIDTH * 0.4f / 4.0f, SCREEN_HEIGHT
		// * 2.0f / 3.0f - 90.0f * 3.0f, false);
	}

	private void initButtons() {
		backButton = new ClickableImageItem(Assets.leftArrowTexture,
				SCREEN_WIDTH * 3.5f / 16.0f, SCREEN_HEIGHT * 2.5f / 23.0f,
				SCREEN_WIDTH * 3.0f / 16.0f, SCREEN_HEIGHT * 3.0f / 23.0f) {

			@Override
			public void action() {
				nineMensMorris.setScreen(new MenuScreen(nineMensMorris));
			}
		};

		soundImage = new ClickableImageItem(null, SCREEN_WIDTH * 6.15f / 8.0f,
				SCREEN_HEIGHT * 2.0f / 3.0f, SCREEN_WIDTH / 8.1f,
				SCREEN_WIDTH / 8.1f) {

			boolean soundEnabled = nineMensMorris.getSettings()
					.isSoundEnabled();

			@Override
			public void action() {
				soundEnabled = !soundEnabled;
				nineMensMorris.getSettings().setSoundEnabled(soundEnabled);
				nineMensMorris.getSettings().save();
				if (soundEnabled) {
					this.setTexture(Assets.buttonVTexture);
				} else {
					this.setTexture(Assets.buttonXTexture);
				}

			}
		};
		if (nineMensMorris.getSettings().isSoundEnabled())
			soundImage.setTexture(Assets.buttonVTexture);
		else
			soundImage.setTexture(Assets.buttonXTexture);

		musicImage = new ClickableImageItem(null, SCREEN_WIDTH * 6.15f / 8.0f,
				SCREEN_HEIGHT * 2.0f / 3.0f - 90.0f, SCREEN_WIDTH / 8.1f,
				SCREEN_WIDTH / 8.1f) {

			boolean musicEnabled = nineMensMorris.getSettings()
					.isMusicEnabled();

			@Override
			public void action() {
				musicEnabled = !musicEnabled;
				nineMensMorris.getSettings().setMusicEnabled(musicEnabled);
				nineMensMorris.getSettings().save();
				if (musicEnabled) {
					this.setTexture(Assets.buttonVTexture);
				} else {
					this.setTexture(Assets.buttonXTexture);
				}

			}
		};
		if (nineMensMorris.getSettings().isMusicEnabled())
			musicImage.setTexture(Assets.buttonVTexture);
		else
			musicImage.setTexture(Assets.buttonXTexture);

		allTrisKillImage = new ClickableImageItem(null,
				SCREEN_WIDTH * 6.15f / 8.0f, SCREEN_HEIGHT * 2.0f / 3.0f
						- 90.0f * 2.0f, SCREEN_WIDTH / 8.1f,
				SCREEN_WIDTH / 8.1f) {

			boolean allTrisKill = nineMensMorris.getSettings()
					.isAllTrisKillEnabled();

			@Override
			public void action() {
				allTrisKill = !allTrisKill;
				nineMensMorris.getSettings().setAllTrisKillEnabled(allTrisKill);
				nineMensMorris.getSettings().save();
				if (allTrisKill) {
					this.setTexture(Assets.buttonVTexture);
				} else {
					this.setTexture(Assets.buttonXTexture);
				}

			}
		};

		if (nineMensMorris.getSettings().isAllTrisKillEnabled())
			allTrisKillImage.setTexture(Assets.buttonVTexture);
		else
			allTrisKillImage.setTexture(Assets.buttonXTexture);

	}
}
