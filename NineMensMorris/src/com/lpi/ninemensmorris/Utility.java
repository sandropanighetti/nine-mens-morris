package com.lpi.ninemensmorris;

/**
 * @author Sandro Panighetti - 30-gen-2013 NineMensMorris
 */
public class Utility {

	public enum Direction {
		UP, RIGHT, DOWN, LEFT;
	}

	public static Direction getRandomDirection() {		
		double r = Math.random()*4.0;
		if(r<1)
			return Direction.UP;
		if(r<2)
			return Direction.RIGHT;
		if(r<3)
			return Direction.DOWN;
		if(r<=4)
			return Direction.LEFT;

		return null;
	}

	public enum Orientation {
		HORIZONTAL, VERTICAL;
	}
	
}
