package com.lpi.ninemensmorris;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;
//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;
import com.lpi.ninemensmorris.Messages.Language;

public class Settings {

	private boolean debuggingEnabled;
	private boolean allTrisKillEnabled;
	private boolean soundEnabled;
	private boolean musicEnabled;
	private Language  language;

	/**
	 * @param debuggingEnabled
	 * @param allTrisKillEnabled
	 * @param soundEnabled
	 * @param musicEnabled
	 */
	public Settings(boolean debuggingEnabled, boolean allTrisKillEnabled,
			boolean soundEnabled, boolean musicEnabled, Language language) {
		super();
		this.setDebuggingEnabled(debuggingEnabled);
		this.setSoundEnabled(allTrisKillEnabled);
		this.setAllTrisKillEnabled(soundEnabled);
		this.setMusicEnabled(musicEnabled);
		this.setLanguage(language);
	}

	/**
	 * 
	 */
	public Settings() {
		super();
	}

	public Settings(Settings settings) {
		super();
		this.setDebuggingEnabled(settings.isDebuggingEnabled());
		this.setSoundEnabled(settings.isSoundEnabled());
		this.setAllTrisKillEnabled(settings.isAllTrisKillEnabled());
		this.setMusicEnabled(settings.isMusicEnabled());
		this.setLanguage(settings.getLanguage());

	}

	public void loadSettings() {
			try {
				FileHandle file = Gdx.files.local("settings.conf");

				Json gson = new Json();
				Settings settings = gson.fromJson(Settings.class, file);
//				Settings settings = gson.fromJson(file.readString(),
//						new TypeToken<Settings>() {
//						}.getType());

				this.setDebuggingEnabled(settings.isDebuggingEnabled());
				this.setSoundEnabled(settings.isSoundEnabled());
				this.setAllTrisKillEnabled(settings.isAllTrisKillEnabled());
				this.setMusicEnabled(settings.isMusicEnabled());
				this.setLanguage(settings.getLanguage());

				// if (file.exists()) {
				// String string = file.readString();
				// String[] rows = string.split("[\t\n]");
				// String[] data = new String[2];
				// // Gdx.app.debug("row", "\n" + Arrays.toString(data));
				//
				// for (int i = 0; i < data.length; i++) {
				// data[i] = rows[i * 2 + 1];
				// }
				//
				// if (data[0].equals("true"))
				// soundEnabled = true;
				// else
				// soundEnabled = false;
				//
				// if (data[1].equals("true"))
				// setDebuggingEnabled(true);
				// else
				// setDebuggingEnabled(false);
				//
				// } else {
				// initDefault();
				// }
			} catch (Exception e) {
				Settings settings = getDefault();
				this.setDebuggingEnabled(settings.isDebuggingEnabled());
				this.setSoundEnabled(settings.isSoundEnabled());
				this.setMusicEnabled(settings.isMusicEnabled());
				this.setAllTrisKillEnabled(settings.isAllTrisKillEnabled());
				this.setLanguage(settings.getLanguage());
			}
	}

	public static Settings getDefault() {
		return new Settings(false, true, false, false, Language.EN);
	}

	public void save() {
		FileHandle file = Gdx.files.local("settings.conf");

		Json gson = new Json();

//		String result = gson.toJson(this, new TypeToken<Settings>() {
//		}.getType());
		String result = gson.toJson(this);
		file.writeString(result, false);
		// file.writeString("soundEnabled\t" + soundEnabled + "\n", true);
		// file.writeString("debuggingEnabled\t" + debuggingEnabled + "\n",
		// true);
	}

	/**
	 * @return the soundEnabled
	 */
	public boolean isSoundEnabled() {
		return soundEnabled;
	}

	/**
	 * @param soundEnabled
	 *            the soundEnabled to set
	 */
	public void setSoundEnabled(boolean soundEnabled) {
		this.soundEnabled = soundEnabled;
	}

	/**
	 * @return the debuggingEnabled
	 */
	public boolean isDebuggingEnabled() {
		return debuggingEnabled;
	}

	/**
	 * @param debuggingEnabled
	 *            the debuggingEnabled to set
	 */
	public void setDebuggingEnabled(boolean debuggingEnabled) {
		// Gdx.app.debug("setDebugEnabled",debuggingEnabled+"");
		if (debuggingEnabled)
			Gdx.app.setLogLevel(Application.LOG_DEBUG);

		this.debuggingEnabled = debuggingEnabled;
	}

	/**
	 * @return the allTrisKillEnabled
	 */
	public boolean isAllTrisKillEnabled() {
		return allTrisKillEnabled;
	}

	/**
	 * @param allTrisKillEnabled
	 *            the allTrisKillEnabled to set
	 */
	public void setAllTrisKillEnabled(boolean allTrisKillEnabled) {
		this.allTrisKillEnabled = allTrisKillEnabled;
	}

	/**
	 * @return the musicEnabled
	 */
	public boolean isMusicEnabled() {
		return musicEnabled;
	}

	/**
	 * @param musicEnabled
	 *            the musicEnabled to set
	 */
	public void setMusicEnabled(boolean musicEnabled) {
		this.musicEnabled = musicEnabled;
		if (musicEnabled)
			Assets.soundTrackMusic.play();
		else
			Assets.soundTrackMusic.stop();
	}

	/**
	 * @return the language
	 */
	public Language getLanguage() {
		return language;
	}

	/**
	 * @param language the language to set
	 */
	public void setLanguage(Language language) {
		this.language = language;
	}

}
