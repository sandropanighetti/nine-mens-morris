package com.lpi.ninemensmorris;

/**
 * @author Sandro Panighetti - 29-gen-2013 NineMensMorris
 */
public class Messages {

	public enum Language {
		EN, IT, FR;
	}

	public static Language[] getLanguages() {
		return Language.values();
	}

	public static class Home {
		public static String TITLE;
		public static String PLAY;
		public static String RESTORE_MATCH;
		public static String SETTINGS;
		public static String HELP;

		public static void load(Language lang) {
			switch (lang) {
			case EN:
				TITLE = "Nine Men's Morris";
				PLAY = "NEW GAME";
				RESTORE_MATCH = "PLAY";
				SETTINGS = "SETTINGS";
				HELP = "HELP";
				// Language.values();
				break;
			case IT:
				TITLE = "Mulino";
				PLAY = "NUOVA PARTITA";
				RESTORE_MATCH = "CONTINUA PARTITA";
				SETTINGS = "OPZIONI";
				HELP = "HELP";
				break;
			case FR:
				TITLE = "Jeu du moulin";
				PLAY = "NOUVEAU JEU";
				RESTORE_MATCH = "JOUER";
				SETTINGS = "OPTIONS";
				HELP = "HELP";
				break;
			default:
				break;
			}
		}

	}

	public static class SettingMatch {
		public static String TITLE;

		public static void load(Language lang) {
			switch (lang) {
			case EN:
				TITLE = "Select opponent";
				break;
			case IT:
				TITLE = "Scelta avversari";
				// TITLE = "AA";
				break;
			case FR:
				TITLE = "S�lectionnez adversaire";
				// TITLE = "AA";
				break;
			default:
				break;
			}
		}
	}

	public static class Game {
		public static class MatchInfo {
			public static String TIME_LABEL;
			public static String MOVES_LABEL;
			public static String MENU_BUTTON;
			public static String RESTART_BUTTON;
			public static String WHITE_WIN;
			public static String BLACK_WIN;

			public static void load(Language lang) {
				switch (lang) {
				case EN:
					TIME_LABEL = "Time";
					MOVES_LABEL = "Moves";
					MENU_BUTTON = "Menu";
					RESTART_BUTTON = "Restart";
					WHITE_WIN = "White win";
					BLACK_WIN = "Black win";
					break;
				case IT:
					TIME_LABEL = "Tempo";
					MOVES_LABEL = "Mosse";
					MENU_BUTTON = "Menu";
					RESTART_BUTTON = "Rigioca";
					WHITE_WIN = "Il bianco vince";
					BLACK_WIN = "Il nero vince";
					break;
				case FR:
					TIME_LABEL = "Temps";
					MOVES_LABEL = "Actions";
					MENU_BUTTON = "Menu";
					RESTART_BUTTON = "Recommencer";
					WHITE_WIN = "Blanc victoire";
					BLACK_WIN = "Noir win";
					break;
				default:
					break;
				}
			}
		}

		public static String WHITE_TURN;
		public static String BLACK_TURN;

		public static void load(Language lang) {
			switch (lang) {
			case EN:
				WHITE_TURN = "WHITE TURN";
				BLACK_TURN = "BLACK TURN";
				break;
			case IT:
				WHITE_TURN = "TURNO DEL BIANCO";
				BLACK_TURN = "TURNO DEL NERO";
				break;
			case FR:
				WHITE_TURN = "BLANC";
				BLACK_TURN = "NOIR";
				break;
			default:
				break;
			}
		}
	}

	public static class Options {
		public static String TITLE;
		public static String SOUND_LABEL;
		public static String MUSIC_LABEL;
		public static String ALL_TRIS_KILL_ENABLED_LABEL;
		public static String LANGUAGE_LABEL;

		public static void load(Language lang) {
			switch (lang) {
			case EN:
				TITLE = "Options";
				SOUND_LABEL = "Sound";
				ALL_TRIS_KILL_ENABLED_LABEL = "Kill in a tris";
				MUSIC_LABEL = "Music";
				LANGUAGE_LABEL = "Language";
				break;
			case IT:
				TITLE = "Opzioni";
				SOUND_LABEL = "Suoni";
				MUSIC_LABEL = "Musica";
				ALL_TRIS_KILL_ENABLED_LABEL = "Mangia se in tris";
				LANGUAGE_LABEL = "Lingua";
				break;
			case FR:
				TITLE = "Options";
				SOUND_LABEL = "Sons";
				MUSIC_LABEL = "Musique";
				ALL_TRIS_KILL_ENABLED_LABEL = "tris?";
				LANGUAGE_LABEL = "Langue";
				break;
			default:
				break;
			}

		}
	}

	public static class Help {
		public static String TITLE;
		public static String CONTEXT;

		public static void load(Language lang) {
			switch (lang) {
			case EN:
				TITLE = "Help";
				CONTEXT = "1� Phase:\n"
						+ "Move pieces to empty spaces\n"
						+ "2� Phase:\n"
						+ "Move pieces to the nearest space\n"
						+ "3� Phase (3 pieces left):\n"
						+ "Move pieces in every empty spaces\n"
						+ "--- \n"
						+ "After forming a mills, click on the piece to eat it";
				break;
			case IT:
				TITLE = "Help";
				CONTEXT = "1� Fase:\n"
						+ "Spostare le pedine su nodi vuoti\n"
						+ "2� Fase:\n"
						+ "Trascinare le pedine su nodi adiacenti vuoti\n"
						+ "3� Fase (3 ped. rimanenti):\n"
						+ "Possibilit� di spostarsi dove si vuole\n"
						+ "--- \n"
						+ "In caso di tris, cliccare su una pedina per mangiarla";
				// CONTEXT = "aaaaaaaaaaaaaaaaaaa";
				break;
			case FR:
				TITLE = "Help";
				CONTEXT = "1� Phase:\n"
						+ "D�placez les pi�ces de noeuds vides\n"
						+ "2� Phase:\n"
						+ "Faites glisser les morceaux de n�uds adjacents vider\n"
						+ "3� Phase (3 pi�ces gauche):\n"
						+ "Capacit� de se d�placer o� vous voulez\n"
						+ "--- \n"
						+ "Apr�s la formation d'une moulins, cliquez sur la pi�ce pour le manger";
				break;
			default:
				break;
			}

		}
	}

	public static void load(Language lang) {

		Home.load(lang);
		SettingMatch.load(lang);
		Game.load(lang);
		Game.MatchInfo.load(lang);
		Options.load(lang);
		Help.load(lang);
	}

}
