package com.lpi.ninemensmorris;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Assets {

	// Texture atlas
	public static TextureAtlas textureAtlas;
	
	//Sounda
	public static Sound tocSound;
	public static Music finalMusic;
	public static Music soundTrackMusic;

	// Imm
	public static TextureRegion woodBackgroundTexture;
	
	public static TextureRegion humanTexture;
	public static TextureRegion computerTexture;
	public static TextureRegion leftArrowTexture;
	public static TextureRegion rightArrowTexture;
	
	public static TextureRegion buttonVTexture;
	public static TextureRegion buttonXTexture;
	
	
	public static TextureRegion selectTexture;

	public static TextureRegion whiteManTexture;
	public static TextureRegion blackManTexture;

	// Nodes
	public static int[][] nodesNeighbors;
	public static int[][] nodesCoordinates;

	// Utility
	public static BitmapFont font;

	public static void load() {
		// Loading Atlas
		textureAtlas = new TextureAtlas(Gdx.files.internal("data/imgs2.pack"),
				Gdx.files.internal("data/"));

		
		//Sounds
		tocSound = Gdx.audio.newSound(Gdx.files.internal("data/sounds/toc.wav"));
		finalMusic = Gdx.audio.newMusic(Gdx.files.internal("data/sounds/Egmont Overture Finale.mp3"));
		finalMusic.setLooping(false);
		finalMusic.setVolume(1.0f);
		
		soundTrackMusic = Gdx.audio.newMusic(Gdx.files.internal("data/sounds/Eternal Wanderer.mp3"));
		soundTrackMusic.setLooping(true);
		soundTrackMusic.setVolume(1.0f);
		
		// Imm
		woodBackgroundTexture = textureAtlas.findRegion("wooden-floor-texture");
		
		humanTexture = textureAtlas.findRegion("user-human");
		computerTexture = textureAtlas.findRegion("user-computer");
		leftArrowTexture = textureAtlas.findRegion("arrow-left");
		rightArrowTexture = textureAtlas.findRegion("arrow-right");

		buttonVTexture = textureAtlas.findRegion("button-v");
		buttonXTexture = textureAtlas.findRegion("button-x");

		selectTexture = textureAtlas.findRegion("neon");
		
		whiteManTexture = textureAtlas.findRegion("ball-white");
		blackManTexture = textureAtlas.findRegion("ball-black");

		// Nodes
		nodesNeighbors = new int[][] {
				// 0
				{ -1, 1, 7, -1 },
				{ -1, 2, 9, 0 },
				{ -1, -1, 3, 1 },
				{ 2, -1, 4, 11 },
				{ 3, -1, -1, 5 },
				// 5
				{ 13, 4, -1, 6 }, { 7, 5, -1, -1 },
				{ 0, 15, 6, -1 },
				{ -1, 9, 15, -1 },
				{ 1, 10, 17, 8 },
				// 10
				{ -1, -1, 11, 9 }, { 10, 3, 12, 19 }, { 11, -1, -1, 13 },
				{ 21, 12, 5, 14 },
				{ 15, 13, -1, -1 },
				// 15
				{ 8, 23, 14, 7 }, { -1, 17, 23, -1 }, { 9, 18, -1, 16 },
				{ -1, -1, 19, 17 }, { 18, 11, 20, -1 },
				// 20
				{ 19, -1, -1, 21 }, { -1, 20, 13, 22 }, { 23, 21, -1, -1 },
				{ 16, -1, 22, 15 } };

		nodesCoordinates = new int[][] {
				// 0
				{ 0, 0 }, { 3, 0 }, { 6, 0 }, { 6, 3 }, { 6, 6 },
				// 5
				{ 3, 6 }, { 0, 6 }, { 0, 3 }, { 1, 1 }, { 3, 1 },
				// 10
				{ 5, 1 }, { 5, 3 }, { 5, 5 }, { 3, 5 }, { 1, 5 },
				// 15
				{ 1, 3 }, { 2, 2 }, { 3, 2 }, { 4, 2 }, { 4, 3 },
				// 20
				{ 4, 4 }, { 3, 4 }, { 2, 4 }, { 2, 3 } };

		// Utility		
		Texture fontTexture = new Texture(Gdx.files.internal("data/font/calibri_white_120.png"), true);
		fontTexture.setFilter(TextureFilter.MipMapLinearNearest, TextureFilter.Linear);
		font = new BitmapFont(Gdx.files.internal("data/font/calibri_white_120.fnt"), new TextureRegion(fontTexture), false);
//		font = new BitmapFont(
//				Gdx.files.internal("data/font/calibri_white_120.fnt"),
//				Gdx.files.internal("data/font/calibri_white_120.png"), false);
		
//		font.getRegion().getTexture().setFilter(TextureFilter.MipMapLinearNearest, TextureFilter.Linear);
		
		

	}

}
