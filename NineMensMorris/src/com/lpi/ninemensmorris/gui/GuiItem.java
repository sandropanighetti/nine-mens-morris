package com.lpi.ninemensmorris.gui;

import com.badlogic.gdx.math.Rectangle;

/**
 * @author Sandro Panighetti - 16-gen-2013
 * NineMensMorris
 */
public class GuiItem {
	
	private boolean visible=true;
	public Rectangle bounds;
	
	/**
	 * 
	 */
	public GuiItem(){
		
	}
	/**
	 * @param bounds
	 */
	public GuiItem(Rectangle bounds){
		this.bounds = bounds;
	}
	
	/**
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public GuiItem(float x, float y, float width, float height){
		bounds = new Rectangle(x-width/2, y-height/2, width, height);
	}
	
	public void setPosition(float x, float y){
		bounds.x = x;
		bounds.y = y;
	}
	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}
	/**
	 * @param visible the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
}
