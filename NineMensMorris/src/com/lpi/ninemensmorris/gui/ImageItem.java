package com.lpi.ninemensmorris.gui;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * @author Sandro Panighetti - 16-gen-2013
 * NineMensMorris
 */
public class ImageItem extends GuiItem {

	private TextureRegion texture;

	public ImageItem() {
		super();
	}

	/**
	 * @param texture
	 * @param x
	 *            (center of the image)
	 * @param y
	 *            (center of the image)
	 * @param width
	 * @param height
	 */
	public ImageItem(TextureRegion texture, float x, float y, float width,
			float height) {
		super(x, y, width, height);
		this.setTexture(texture);
	}

	/**
	 * @return the texture
	 */
	public TextureRegion getTexture() {
		return texture;
	}

	/**
	 * @param texture
	 *            the texture to set
	 */
	public void setTexture(TextureRegion texture) {
		this.texture = texture;
	}

}
