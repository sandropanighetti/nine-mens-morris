package com.lpi.ninemensmorris.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.math.Rectangle;
import com.lpi.ninemensmorris.Assets;
import com.lpi.ninemensmorris.screen.AbstractScreen;

/**
 * @author Sandro Panighetti - 16-gen-2013 NineMensMorris
 */
public class TextItem extends GuiItem {

	public String text;

	private BitmapFont font;

	protected Color color;

	public float textScale;

	public boolean center;

	/**
	 * 
	 */
	public TextItem() {
		super();
	}

	/**
	 * @param text
	 * @param textScale
	 * @param x
	 * @param y
	 */
	public TextItem(String text, Color color, BitmapFont font, float textScale,
			float x, float y) {
		super(x, y, 0, 0);
		this.text = text;
		this.textScale = textScale;
		this.font = font;
		this.color = color;
		this.center = true;

		font.setScale(textScale);
		TextBounds textBounds;

		float cnt = 0.0f;
		do {
			this.textScale -= cnt;
			font.setScale(this.textScale);
			textBounds = font.getBounds(text);
			cnt += 0.01f;
		} while (textBounds.width > AbstractScreen.SCREEN_WIDTH);
		// } while (false);
		bounds = new Rectangle(x - textBounds.width / 2, y - textBounds.height
				/ 2, textBounds.width, textBounds.height);
	}

	/**
	 * @param text
	 * @param textScale
	 * @param x
	 * @param y
	 */

	public TextItem(String text, Color color, BitmapFont font, float textScale,
			float x, float y, boolean center) {
		super(x, y, 0, 0);
		this.center = center;
		if (center) {
			this.text = text;
			this.textScale = textScale;
			this.font = font;
			this.color = color;

			font.setScale(textScale);
			TextBounds textBounds;

			float cnt = 0.0f;
			do {
				this.textScale -= cnt;
				font.setScale(this.textScale);
				textBounds = font.getBounds(text);
				cnt += 0.01f;
			} while (textBounds.width > AbstractScreen.SCREEN_WIDTH);
			// } while (false);
			bounds = new Rectangle(x - textBounds.width / 2, y
					- textBounds.height / 2, textBounds.width,
					textBounds.height);
		} else {

			this.text = text;
			this.textScale = textScale;
			this.font = font;
			this.color = color;

			font.setScale(textScale);
			TextBounds textBounds;

			float cnt = 0.0f;
			do {
				this.textScale -= cnt;
				font.setScale(this.textScale);
				textBounds = font.getBounds(text);
				cnt += 0.01f;
			} while (textBounds.width > AbstractScreen.SCREEN_WIDTH);
			bounds = new Rectangle(x, y - textBounds.height / 2,
					textBounds.width, textBounds.height);
		}
	}

	public TextItem(String text, Color color, BitmapFont font, float textScale,
			float x, float y, boolean center, boolean autoresize) {
		super(x, y, 0, 0);
		this.center = center;
		if (center) {
			this.text = text;
			this.textScale = textScale;
			this.font = font;
			this.color = color;

			font.setScale(textScale);
			TextBounds textBounds;

			float cnt = 0.0f;
			do {
				this.textScale -= cnt;
				font.setScale(this.textScale);
				textBounds = font.getBounds(text);
				cnt += 0.01f;
			} while (textBounds.width > AbstractScreen.SCREEN_WIDTH
					&& autoresize);
			// } while (false);
			bounds = new Rectangle(x - textBounds.width / 2, y
					- textBounds.height / 2, textBounds.width,
					textBounds.height);
		} else {

			this.text = text;
			this.textScale = textScale;
			this.font = font;
			this.color = color;

			font.setScale(textScale);
			TextBounds textBounds;

			float cnt = 0.0f;
			do {
				this.textScale -= cnt;
				font.setScale(this.textScale);
				textBounds = font.getBounds(text);
				cnt += 0.01f;
			} while (textBounds.width > AbstractScreen.SCREEN_WIDTH
					&& autoresize);
			bounds = new Rectangle(x, y - textBounds.height / 2,
					textBounds.width, textBounds.height);
		}

	}

	public void resetPosition() {
		if (center) {

			TextBounds textBounds;
			float cnt = 0.0f;
			do {
				this.textScale -= cnt;
				font.setScale(this.textScale);
				textBounds = font.getBounds(text);
				cnt += 0.01f;
			} while (textBounds.width > AbstractScreen.SCREEN_WIDTH);
			// bounds = new Rectangle(x - textBounds.width / 2, y
			// - textBounds.height / 2, textBounds.width,
			// textBounds.height);

			bounds = new Rectangle(bounds.x + bounds.width / 2
					- textBounds.width / 2, bounds.y + bounds.height / 2
					- textBounds.height / 2, textBounds.width,
					textBounds.height);

			// TextBounds textBounds;
			//
			// float cnt = 0.0f;
			// do {
			// this.textScale -= cnt;
			// font.setScale(this.textScale);
			// textBounds = font.getBounds(text);
			// cnt += 0.1f;
			// } while (textBounds.width > AbstractScreen.SCREEN_WIDTH);
			// bounds = new Rectangle(.x - textBounds.width / 2, y
			// - textBounds.height / 2, textBounds.width,
			// textBounds.height);
		}
	}

	/**
	 * @return the font
	 */
	public BitmapFont getFont() {
		return font;
	}

	/**
	 * @param font
	 *            the font to set
	 */
	public void setFont(BitmapFont font) {
		this.font = font;
	}

	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @param color
	 *            the color to set
	 */
	public void setColor(Color color) {
		this.color = color;
	}
}
