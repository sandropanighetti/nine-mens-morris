package com.lpi.ninemensmorris.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

/**
 * @author Sandro Panighetti - 16-gen-2013 NineMensMorris
 */
public abstract class ClickableTextItem extends TextItem implements Clickable {

	public Color overColor;
	private Color oldColor;

	/**
	 * 
	 */
	public ClickableTextItem() {
		super();
	}

	/**
	 * @param text
	 * @param textSize
	 * @param x
	 * @param y
	 */
	public ClickableTextItem(String text, Color color, Color overColor,
			BitmapFont font, float textSize, float x, float y) {
		super(text, color, font, textSize, x, y);
		this.overColor = overColor;
		this.oldColor = color;
	}
	
	/**
	 * @param text
	 * @param textSize
	 * @param x
	 * @param y
	 */
	public ClickableTextItem(String text, Color color, Color overColor,
			BitmapFont font, float textSize, float x, float y, boolean center) {
		super(text, color, font, textSize, x, y, center);
		this.overColor = overColor;
		this.oldColor = color;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.lpi.ninemensmorris.guiitem.Clickable#onTouchDown(float, float)
	 */
	@Override
	public void onTouchDown(float x, float y) {
		if(!isVisible())
			return;
		if (bounds.contains(x, y))
			action();
	}

	public void onMouseMove(float x, float y) {
		if(!isVisible())
			return;
		if (bounds.contains(x, y)) {
			color = overColor;
		} else {
			color = oldColor;
		}
	}

}
