package com.lpi.ninemensmorris.gui;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

/**
 * @author Sandro Panighetti - 16-gen-2013
 * NineMensMorris
 */
public abstract class ClickableImageItem extends ImageItem implements Clickable{

	/**
	 * 
	 */
	public ClickableImageItem() {
		super();
	}

	/**
	 * @param texture
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public ClickableImageItem(TextureRegion texture, float x, float y,
			float width, float height) {
		super(texture, x, y, width, height);
	}

	/* (non-Javadoc)
	 * @see com.lpi.ninemensmorris.guiitem.Clickable#onTouchDown(float, float)
	 */
	@Override
	public void onTouchDown(float x, float y) {
		if (bounds.contains(x,y))
			action();
	}

}
