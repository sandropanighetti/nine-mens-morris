package com.lpi.ninemensmorris.gui;

/**
 * @author Sandro Panighetti - 16-gen-2013
 * NineMensMorris
 */
public interface Clickable {

	/**
	 * @param x
	 * @param y
	 */
	public void onTouchDown(float x, float y);
	
	/**
	 * 
	 */
	public void action();
}
