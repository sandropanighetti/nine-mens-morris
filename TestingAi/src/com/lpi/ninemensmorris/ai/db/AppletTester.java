package com.lpi.ninemensmorris.ai.db;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class AppletTester extends Applet implements MouseListener,
		MouseMotionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -357531028621360550L;

	private static int threadCounter = 0;
	private static int numberOfThread = 1;
	private static long startTime;
	private static long finalTime;

	final int depth = 6;
	Node first;

	public void init() {
		System.out.println("------------START------------");
		startTime = System.currentTimeMillis();

		new Thread(new Runnable() {

			@Override
			public void run() {
				int pre = -1;
				int cnt = 0;
				System.out.println("counter[" + cnt + "]: " + Node.getCounter());
				while (threadCounter < numberOfThread) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (pre != Node.getCounter()) {
						cnt++;
						pre = Node.getCounter();
						System.out.println("counter[" + cnt + "]: "
								+ Node.getCounter());
					}

				}
			}
		}).start();

		first = new Node("dacejmbgh1AAAS", true);
		first.fillChildren(depth);
		threadCounter++;

		new Thread(new Runnable() {

			@Override
			public void run() {
				boolean flag = true;
				while (flag) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (threadCounter == numberOfThread)
						flag = false;
				}

				finalTime = System.currentTimeMillis();
				long totalTime = finalTime - startTime;
				System.out.println("------------STOP------------");
				System.out.println("Total time: " + totalTime);
			}
		}).start();

		// System.out.println("counter: " + Node.getCounter());

		first.setWidth(50);
		first.setHeight(50);
		first.setX(this.getWidth() / 2);
		first.setY(40);
		first.setVisible(true);

		addMouseListener(this);
		addMouseMotionListener(this);
	}

	public void paint(Graphics g) {

		drawSchema(g, first);
	}

	public void drawSchema(Graphics g, Node node) {
		if (!node.isVisible())
			return;

//		if (node.getParent() != null) {
			if (node.isJustDoneATris()) {
				if (node.isMoveWhite())
					g.setColor(Color.blue);
				else
					g.setColor(Color.red);
			} else {
				if (!node.isMoveWhite())
					g.setColor(Color.blue);
				else
					g.setColor(Color.red);
			}
//		}

//		if (!node.isMoveWhite())
//			g.setColor(Color.blue);
//		else
//			g.setColor(Color.red);
		// g.drawRect(node.getX() - node.getWidth() / 2,
		// node.getY() - node.getHeight() / 2, node.getWidth(),
		// node.getHeight());

		int spacing = node.getWidth() / 6;

		g.drawRect(node.getX() - node.getWidth() / 2 + spacing, node.getY()
				- node.getHeight() / 2 + spacing, spacing * 4, spacing * 4);

		g.drawRect(node.getX() - node.getWidth() / 2 + spacing * 2, node.getY()
				- node.getHeight() / 2 + spacing * 2, spacing * 2, spacing * 2);

		g.drawLine(node.getX() - node.getWidth() / 2 + spacing * 3, node.getY()
				- node.getHeight() / 2 + spacing, node.getX() - node.getWidth()
				/ 2 + spacing * 3, node.getY() - node.getHeight() / 2 + spacing
				* 2);
		g.drawLine(node.getX() - node.getWidth() / 2 + spacing * 3, node.getY()
				- node.getHeight() / 2 + spacing * 4,
				node.getX() - node.getWidth() / 2 + spacing * 3, node.getY()
						- node.getHeight() / 2 + spacing * 5);

		g.drawLine(node.getX() - node.getWidth() / 2 + spacing, node.getY()
				- node.getHeight() / 2 + spacing * 3,
				node.getX() - node.getWidth() / 2 + spacing * 2, node.getY()
						- node.getHeight() / 2 + spacing * 3);

		g.drawLine(node.getX() - node.getWidth() / 2 + spacing * 4, node.getY()
				- node.getHeight() / 2 + spacing * 3,
				node.getX() - node.getWidth() / 2 + spacing * 5, node.getY()
						- node.getHeight() / 2 + spacing * 3);

		g.setColor(Color.blue);
		int ballSize = spacing * 5 / 6;
		for (byte i = 0; i < 5; i++) {
			char current = node.getState().charAt(i);
			if ('0' != current && '1' != current) {
				drawPiece(g, node.getX() - node.getWidth() / 2 + spacing
						* (1 + Node.getJ(current)) + spacing / 4,
						node.getY() - node.getHeight() / 2 + spacing
								* (1 + Node.getI(current)) + spacing / 4,
						ballSize, ballSize);
			}
		}

		g.setColor(Color.red);
		for (byte i = 5; i < 10; i++) {
			char current = node.getState().charAt(i);
			if ('0' != current && '1' != current) {
				drawPiece(g, node.getX() - node.getWidth() / 2 + spacing
						* (1 + Node.getJ(current)) + spacing / 4,
						node.getY() - node.getHeight() / 2 + spacing
								* (1 + Node.getI(current)) + spacing / 4,
						ballSize, ballSize);
			}
		}

		int row = 5;
		g.setFont(new Font(null, Font.PLAIN, node.getHeight() / row));
		g.setColor(Color.black);
		g.drawString("Value:", node.getX() + node.getWidth() * 3 / 4,
				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 1);
		g.drawString("Depth:", node.getX() + node.getWidth() * 3 / 4,
				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 2);
		g.drawString("In base:", node.getX() + node.getWidth() * 3 / 4,
				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 3);
		g.drawString("In game:", node.getX() + node.getWidth() * 3 / 4,
				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 4);
		g.drawString("Just tris:", node.getX() + node.getWidth() * 3 / 4,
				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 5);

		int spacingA = 30;
		int spacingB = 15;
		g.drawString(node.isJustDoneATris() + "", node.getX() + node.getWidth()
				* 3 / 4 + spacingA + spacingB * 1,
				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 5);

		g.setColor(Color.blue);
		g.drawString(node.getWhiteValue() + "", node.getX() + node.getWidth()
				* 3 / 4 + spacingA + spacingB * 1,
				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 1);
		g.drawString(
				node.getWhiteDepthFromTris() + "",
				node.getX() + node.getWidth() * 3 / 4 + spacingA + spacingB * 1,
				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 2);
		g.drawString(
				node.getWhitePiecesInBase() + "",
				node.getX() + node.getWidth() * 3 / 4 + spacingA + spacingB * 1,
				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 3);
		g.drawString(
				node.getWhitePiecesInGame() + "",
				node.getX() + node.getWidth() * 3 / 4 + spacingA + spacingB * 1,
				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 4);

		g.setColor(Color.red);

		g.drawString(node.getBlackValue() + "", node.getX() + node.getWidth()
				* 3 / 4 + spacingA + spacingB * 2,
				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 1);
		g.drawString(
				node.getBlackDepthFromTris() + "",
				node.getX() + node.getWidth() * 3 / 4 + spacingA + spacingB * 2,
				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 2);
		g.drawString(
				node.getBlackPiecesInBase() + "",
				node.getX() + node.getWidth() * 3 / 4 + spacingA + spacingB * 2,
				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 3);
		g.drawString(
				node.getBlackPiecesInGame() + "",
				node.getX() + node.getWidth() * 3 / 4 + spacingA + spacingB * 2,
				node.getY() - node.getHeight() / 2 + node.getHeight() / row * 4);

		for (Node n : node.getChildren()) {
			if (n.isVisible()) {
				// if (!node.isMoveWhite())
				// g.setColor(Color.red);
				// else
				// g.setColor(Color.blue);
				g.setColor(Color.black);
				g.drawLine(node.getX(), node.getY() + node.getHeight() / 2,
						n.getX(), n.getY() - n.getHeight() / 2);
				drawSchema(g, n);
			}
		}
	}

	private void drawPiece(Graphics g, int x, int y, int width, int height) {
		g.fillRect(x - width / 2, y - height / 2, width, height);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// System.out.println("pressed");
		Node dum = first.getAtPosition(e.getX(), e.getY());
		if (dum != null) {

			if (dum.isVisible()) {

				boolean isAChildrenVisible = false;
				for (Node node : dum.getChildren())
					if (node.isVisible())
						isAChildrenVisible = true;

				if (isAChildrenVisible) {
					dum.setVisible(false);
					dum.setVisible(true);
					// System.out.println("a children is visible");
				} else {
					if (dum.getParent() != null) {
						dum.getParent().setChildrenVisible(false);
						dum.getParent().setChildrenVisible(true);
					}
					dum.setChildrenVisible(true);
					dum.setVisible(true);
					dum.fillChildren(depth);
				}
				// dum.setVisible(true);

				repaint();
			}
		}

		// if (first.contains(e.getX(), e.getY()))
		// System.out.println("contains");
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		Node dum = first.getAtPosition(e.getX(), e.getY());
		if (dum == first) {
			first.setX(e.getX());
			first.setY(e.getY());
			repaint();
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
