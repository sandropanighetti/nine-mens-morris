package com.lpi.ninemensmorris.ai.db;

import java.util.LinkedList;
import java.util.List;

public class Node {

	private static int counter = 0;

	private String state;

	private Node parent;

	private List<Node> children;

	private short whiteValue;
	private short blackValue;
	private byte whiteDepthFromTris = 99;
	private byte blackDepthFromTris = 99;
	private boolean justDoneATris;

	private boolean moveWhite;

	private int x;
	private int y;
	private int width;
	private int height;
	private boolean visible;

	/**
	 * @param state
	 * @param parent
	 * @param lastMoveWhite
	 */
	public Node(Node parent, boolean moveWhite) {
		super();
		this.state = parent.state;
		this.parent = parent;
		this.moveWhite = moveWhite;
		children = new LinkedList<Node>();
		counter++;
	}

	/**
	 * @param state
	 * @param parent
	 * @param lastMoveWhite
	 */
	public Node(boolean moveWhite) {
		super();
		this.state = "00000000000000";
		this.moveWhite = moveWhite;
		children = new LinkedList<Node>();
		counter++;
	}

	/**
	 * @param state
	 * @param parent
	 * @param lastMoveWhite
	 */
	public Node(String state, boolean moveWhite) {
		super();
		this.state = state;
		this.moveWhite = moveWhite;
		children = new LinkedList<Node>();
		counter++;
	}

	public void copySchemeFromParent() {
		if (parent == null)
			return;

		this.state = parent.state;
	}

	public void fillChildren(int depth) {
		if (depth <= 0) {
			/*
			 * Valuate
			 */

			return;
		}

		// Generating children for second phase
		if (children.size() == 0) {

			if (justDoneATris) {

				if (moveWhite) {

					if (parent.getBlackPiecesInGame() == 4
							&& parent.getBlackPiecesInBase() == 0) {
						setWhiteValue((short) 999);
						// return;
					}
					for (byte i = 5; i < 10; i++) {
						char current = state.charAt(i);
						if (current != '0' && current != '1') {
							Node node = new Node(this, false);
							children.add(node);
							node.move(current, '1');
							node.fillChildren(depth - 1);
						}
					}
				} else {
					if (parent.getWhitePiecesInGame() == 4
							&& parent.getWhitePiecesInBase() == 0) {
						setBlackValue((short) 999);
						// return;
					}
					for (byte i = 0; i < 5; i++) {
						char current = state.charAt(i);
						if (current != '0' && current != '1') {
							Node node = new Node(this, true);
							children.add(node);
							node.move(current, '1');
							node.fillChildren(depth - 1);
						}
					}
				}
			} else {
				if (moveWhite) {
					for (byte i = 0; i < 5; i++) {
						char current = state.charAt(i);
						if (current != '1') {
							char[] result = null;
							if (getWhitePiecesInBase() == 0
									&& getWhitePiecesInGame() == 3)
								result = getAllPossibilities();
							else
								result = getPossibilities(current);

							for (byte j = 0; j < result.length; j++) {
								if (!isOccupied(result[j])) {
									Node node = new Node(this, false);
									children.add(node);
									byte trisBefore = node
											.getNumberOfWhiteTris();
									node.move(current, result[j]);
									if (trisBefore < node
											.getNumberOfWhiteTris()) {
										node.justDoneATris = true;
										node.moveWhite = true;
									}
									node.fillChildren(depth - 1);
								}
							}
						}
					}

				} else {
					for (byte i = 5; i < 10; i++) {
						char current = state.charAt(i);
						if (current != '1') {
							char[] result = null;
							if (getBlackPiecesInBase() == 0
									&& getBlackPiecesInGame() == 3)
								result = getAllPossibilities();
							else
								result = getPossibilities(current);
							for (byte j = 0; j < result.length; j++) {
								if (!isOccupied(result[j])) {
									Node node = new Node(this, true);
									children.add(node);
									byte trisBefore = node
											.getNumberOfWhiteTris();
									node.move(current, result[j]);
									if (trisBefore < node
											.getNumberOfBlackTris()) {
										node.justDoneATris = true;
										node.moveWhite = false;
									}
									node.fillChildren(depth - 1);
								}
							}
						}
					}
				}
			}
		} else {
			for (Node node : children)
				node.fillChildren(depth - 1);
		}

		whiteDepthFromTris = (byte) 90;
		blackDepthFromTris = (byte) 90;
		for (Node node : children) {
			if (node.getWhiteDepthFromTris() < whiteDepthFromTris)
				whiteDepthFromTris = node.getWhiteDepthFromTris();

			if (node.getBlackDepthFromTris() < blackDepthFromTris)
				blackDepthFromTris = node.getBlackDepthFromTris();

		}
		if (justDoneATris)
			if (moveWhite)
				whiteDepthFromTris = 0;
			else
				blackDepthFromTris = 0;
		else {
			whiteDepthFromTris++;
			blackDepthFromTris++;
		}
		if (parent != null)
			parent.fillChildren(1);

	}

	public Node getAtPosition(int x, int y) {
		// if(!visible)
		// return null;

		if (this.contains(x, y))
			return this;

		for (Node node : children) {
			if (node.visible) {
				Node dum = node.getAtPosition(x, y);
				if (dum != null)
					return dum;
			}
		}

		return null;
	}

	private boolean contains(int x, int y) {
		if (this.x - this.width / 2 <= x && this.x + this.width / 2 >= x
				&& this.y - this.height / 2 < y && this.y + this.height / 2 > y)
			return true;
		return false;
	}

	public boolean isOccupied(char c) {
		for (byte i = 0; i < 10; i++)
			if (state.charAt(i) == c)
				return true;
		return false;
	}

	public boolean isOccupiedByWhite(char c) {
		for (byte i = 0; i < 5; i++)
			if (state.charAt(i) == c)
				return true;
		return false;
	}

	public boolean isOccupiedByBlack(char c) {
		for (byte i = 5; i < 10; i++)
			if (state.charAt(i) == c)
				return true;
		return false;
	}

	public byte getNumberOfWhiteTris() {
		byte cnt = 0;
		// External
		if (isOccupiedByWhite('a') && isOccupiedByWhite('b')
				&& isOccupiedByWhite('c'))
			cnt++;
		if (isOccupiedByWhite('c') && isOccupiedByWhite('d')
				&& isOccupiedByWhite('e'))
			cnt++;
		if (isOccupiedByWhite('g') && isOccupiedByWhite('f')
				&& isOccupiedByWhite('e'))
			cnt++;
		if (isOccupiedByWhite('a') && isOccupiedByWhite('h')
				&& isOccupiedByWhite('g'))
			cnt++;

		// Internal
		if (isOccupiedByWhite('i') && isOccupiedByWhite('j')
				&& isOccupiedByWhite('k'))
			cnt++;
		if (isOccupiedByWhite('k') && isOccupiedByWhite('l')
				&& isOccupiedByWhite('m'))
			cnt++;
		if (isOccupiedByWhite('m') && isOccupiedByWhite('n')
				&& isOccupiedByWhite('o'))
			cnt++;
		if (isOccupiedByWhite('i') && isOccupiedByWhite('p')
				&& isOccupiedByWhite('o'))
			cnt++;

		return cnt;
	}

	public byte getNumberOfBlackTris() {
		byte cnt = 0;
		// External
		if (isOccupiedByBlack('a') && isOccupiedByBlack('b')
				&& isOccupiedByBlack('c'))
			cnt++;
		if (isOccupiedByBlack('c') && isOccupiedByBlack('d')
				&& isOccupiedByBlack('e'))
			cnt++;
		if (isOccupiedByBlack('g') && isOccupiedByBlack('f')
				&& isOccupiedByBlack('e'))
			cnt++;
		if (isOccupiedByBlack('a') && isOccupiedByBlack('h')
				&& isOccupiedByBlack('g'))
			cnt++;

		// Internal
		if (isOccupiedByBlack('i') && isOccupiedByBlack('j')
				&& isOccupiedByBlack('k'))
			cnt++;
		if (isOccupiedByBlack('k') && isOccupiedByBlack('l')
				&& isOccupiedByBlack('m'))
			cnt++;
		if (isOccupiedByBlack('m') && isOccupiedByBlack('n')
				&& isOccupiedByBlack('o'))
			cnt++;
		if (isOccupiedByBlack('i') && isOccupiedByBlack('p')
				&& isOccupiedByBlack('o'))
			cnt++;

		return cnt;
	}

	public byte getWhitePiecesInBase() {
		byte cnt = 0;
		for (byte i = 0; i < 5; i++)
			if (state.charAt(i) == '0')
				cnt++;

		return cnt;
	}

	public byte getWhitePiecesInGame() {
		byte cnt = 0;
		for (byte i = 0; i < 5; i++)
			if (state.charAt(i) != '0' && state.charAt(i) != '1')
				cnt++;

		return cnt;
	}

	public byte getBlackPiecesInGame() {
		byte cnt = 0;
		for (byte i = 5; i < 10; i++)
			if (state.charAt(i) != '0' && state.charAt(i) != '1')
				cnt++;

		return cnt;
	}

	public byte getBlackPiecesInBase() {
		byte cnt = 0;
		for (byte i = 5; i < 10; i++)
			if (state.charAt(i) == '0')
				cnt++;

		return cnt;
	}

	public byte getWhitePiecesKilled() {
		byte cnt = 0;
		for (byte i = 0; i < 5; i++)
			if (state.charAt(i) == '1')
				cnt++;

		return cnt;
	}

	public byte getBlackPiecesKilled() {
		byte cnt = 0;
		for (byte i = 5; i < 10; i++)
			if (state.charAt(i) == '1')
				cnt++;

		return cnt;
	}

	public void move(char from, char to) {
		for (byte i = 0; i < 10; i++) {
			if (state.charAt(i) == from) {
				state = replace(state, i, to);
				return;
			}
		}
	}

	/*
	 * From stack overflow
	 * http://stackoverflow.com/questions/6952363/java-replace
	 * -a-character-at-a-specific-index-in-a-string
	 */
	private String replace(String str, int index, char replace) {
		if (str == null) {
			return str;
		} else if (index < 0 || index >= str.length()) {
			return str;
		}
		char[] chars = str.toCharArray();
		chars[index] = replace;
		return String.valueOf(chars);
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the parent
	 */
	public Node getParent() {
		return parent;
	}

	/**
	 * @param parent
	 *            the parent to set
	 */
	public void setParent(Node parent) {
		this.parent = parent;
	}

	/**
	 * @return the children
	 */
	public List<Node> getChildren() {
		return children;
	}

	/**
	 * @param children
	 *            the children to set
	 */
	public void setChildren(List<Node> children) {
		this.children = children;
	}

	/**
	 * @return the whiteValue
	 */
	public short getWhiteValue() {
		return whiteValue;
	}

	/**
	 * @param whiteValue
	 *            the whiteValue to set
	 */
	public void setWhiteValue(short whiteValue) {
		this.whiteValue = whiteValue;
	}

	/**
	 * @return the blackValue
	 */
	public short getBlackValue() {
		return blackValue;
	}

	/**
	 * @param blackValue
	 *            the blackValue to set
	 */
	public void setBlackValue(short blackValue) {
		this.blackValue = blackValue;
	}

	/**
	 * @return the lastMoveWhite
	 */
	public boolean isMoveWhite() {
		return moveWhite;
	}

	/**
	 * @param lastMoveWhite
	 *            the lastMoveWhite to set
	 */
	public void setMoveWhite(boolean lastMoveWhite) {
		this.moveWhite = lastMoveWhite;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x
	 *            the x to set
	 */
	public void setX(int x) {
		this.x = x;
		int childrenSize = children.size();
		int cnt = 0;
		int totalSchemeWidth = width + 100;
		for (Node node : children) {
			if (node.isVisible()) {
				node.setX(this.x + totalSchemeWidth / 2 - (childrenSize / 2)
						* totalSchemeWidth + cnt * totalSchemeWidth);
				// node.setWidth(width);
				// node.setHeight(height);
				// s.setY(y+height*3/2);
				cnt++;
			}
		}
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y
	 *            the y to set
	 */
	public void setY(int y) {
		this.y = y;
		for (Node node : children) {
			if (node.isVisible()) {
				node.setY(y + height * 3 / 2);
			}
		}
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width
	 *            the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
		for (Node node : children) {
			if (node.isVisible())
				node.setWidth(width);
		}
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param height
	 *            the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
		for (Node node : children) {
			if (node.isVisible())
				node.setHeight(height);
		}
	}

	/**
	 * @return the whiteDepthFromTris
	 */
	public byte getWhiteDepthFromTris() {
		return whiteDepthFromTris;
	}

	/**
	 * @param whiteDepthFromTris
	 *            the whiteDepthFromTris to set
	 */
	public void setWhiteDepthFromTris(byte whiteDepthFromTris) {
		this.whiteDepthFromTris = whiteDepthFromTris;
	}

	/**
	 * @return the blackDepthFromTris
	 */
	public byte getBlackDepthFromTris() {
		return blackDepthFromTris;
	}

	/**
	 * @param blackDepthFromTris
	 *            the blackDepthFromTris to set
	 */
	public void setBlackDepthFromTris(byte blackDepthFromTris) {
		this.blackDepthFromTris = blackDepthFromTris;
	}

	public static char[] getAllPossibilities() {
		return new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
				'k', 'l', 'm', 'n', 'o', 'p' };
	}

	public static char[] getPossibilities(char c) {
		switch (c) {
		case 'a':
			return new char[] { 'b', 'h' };

		case 'b':

			return new char[] { 'a', 'c', 'j' };

		case 'c':

			return new char[] { 'b', 'd' };

		case 'd':

			return new char[] { 'c', 'e', 'l' };

		case 'e':

			return new char[] { 'd', 'f' };

		case 'f':

			return new char[] { 'e', 'g', 'n' };

		case 'g':

			return new char[] { 'h', 'f' };

		case 'h':

			return new char[] { 'a', 'g', 'p' };

		case 'i':

			return new char[] { 'j', 'p' };

		case 'j':

			return new char[] { 'b', 'k', 'i' };

		case 'k':

			return new char[] { 'j', 'l' };

		case 'l':

			return new char[] { 'd', 'k', 'm' };

		case 'm':

			return new char[] { 'n', 'l' };

		case 'n':

			return new char[] { 'f', 'o', 'm' };

		case 'o':

			return new char[] { 'n', 'p' };

		case 'p':

			return new char[] { 'i', 'o', 'h' };

		}
		return new char[] {};
	}

	public static int getJ(char c) {
		if (c == 'a' || c == 'h' || c == 'g')
			return 0;
		if (c == 'i' || c == 'p' || c == 'o')
			return 1;
		if (c == 'b' || c == 'j' || c == 'n' || c == 'f')
			return 2;
		if (c == 'k' || c == 'l' || c == 'm')
			return 3;
		if (c == 'c' || c == 'd' || c == 'e')
			return 4;
		return -1;
	}

	public static int getI(char c) {
		if (c == 'a' || c == 'b' || c == 'c')
			return 0;
		if (c == 'i' || c == 'j' || c == 'k')
			return 1;
		if (c == 'h' || c == 'p' || c == 'l' || c == 'd')
			return 2;
		if (c == 'o' || c == 'n' || c == 'm')
			return 3;
		if (c == 'g' || c == 'f' || c == 'e')
			return 4;
		return -1;
	}

	public void setChildrenVisible(boolean visible) {
		for (Node n : children)
			n.setVisible(visible);
		setX(x);
		setY(y);
		setWidth(width);
		setHeight(height);
	}

	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible
	 *            the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
		if (!visible) {
			setChildrenVisible(false);
		}
	}

	/**
	 * @return the counter
	 */
	public static int getCounter() {
		return counter;
	}

	/**
	 * @param counter
	 *            the counter to set
	 */
	public static void setCounter(int counter) {
		Node.counter = counter;
	}

	/**
	 * @return the justDoneATris
	 */
	public boolean isJustDoneATris() {
		return justDoneATris;
	}

	/**
	 * @param justDoneATris
	 *            the justDoneATris to set
	 */
	public void setJustDoneATris(boolean justDoneATris) {
		this.justDoneATris = justDoneATris;
	}

}
