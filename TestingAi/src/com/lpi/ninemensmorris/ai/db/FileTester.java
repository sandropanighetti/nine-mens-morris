package com.lpi.ninemensmorris.ai.db;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class FileTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			// Create file
			FileWriter fstream = new FileWriter("out.txt");
			BufferedWriter out = new BufferedWriter(fstream);
			for (int i = 0; i < 875000000; i++) {
				out.write("0acej1bghoAAAS");
			}
			// Close the output stream
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

	}

}
