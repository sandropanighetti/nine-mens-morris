package com.lpi.ninemensmorris.ai;

import java.util.LinkedList;

public class Schema {

	public static LinkedList<Node> getLinkedNodes() {

		LinkedList<Node> list = new LinkedList<Node>();
		for (byte i = 0; i <= 15; i++) {
			list.add(new Node(i));
		}

		byte[][] g = new byte[][] {
				// 0
				{ -1, 1, 7, -1 }, { -1, 2, 9, 0 }, { -1, -1, 3, 1 },
				{ 2, -1, 4, 11 },
				{ 3, -1, -1, 5 },
				// 5
				{ 13, 4, -1, 6 }, { 7, 5, -1, -1 }, { 0, 15, 6, -1 },
				{ -1, 9, 15, -1 }, { 1, 10, -1, 8 },
				// 10
				{ -1, -1, 11, 9 }, { 10, 3, 12, -1 }, { 11, -1, -1, 13 },
				{ -1, 12, 5, 14 }, { 15, 13, -1, -1 },
				// 15
				{ 8, -1, 14, 7 } };

		for (byte i = 0; i <= 15; i++) {
			if (g[i][0] != -1)
				list.get(i).up = list.get(g[i][0]);
			if (g[i][1] != -1)
				list.get(i).right = list.get(g[i][1]);
			if (g[i][2] != -1)
				list.get(i).down = list.get(g[i][2]);
			if (g[i][3] != -1)
				list.get(i).left = list.get(g[i][3]);
		}
		return list;
	}

	public LinkedList<Node> nodes;

	public Schema parent;
	public LinkedList<Schema> children;

	public byte whitePiecesToPlace = 5;
	public byte blackPiecesToPlace = 5;
	public byte whitePiecesInGame = 0;
	public byte blackPiecesInGame = 0;

	public byte whitePhase = 1;
	public byte blackPhase = 1;

	public byte lastMovement = 0;

	public boolean toKill = false;

	public byte winner = 0;

	public int movementCount = 0;

	/**
	 * 
	 */
	public Schema() {
		super();
		children = new LinkedList<Schema>();
	}

	public Schema(Schema parent) {
		super();
		this.parent = parent;
		children = new LinkedList<Schema>();
	}

	public void copySchemeFromParent() {
		if (parent == null)
			return;

		whitePiecesToPlace = parent.whitePiecesToPlace;
		blackPiecesToPlace = parent.blackPiecesToPlace;
		whitePiecesInGame = parent.whitePiecesInGame;
		blackPiecesInGame = parent.blackPiecesInGame;

		whitePhase = parent.whitePhase;
		blackPhase = parent.blackPhase;

		movementCount = parent.movementCount;

		nodes = getLinkedNodes();
		for (byte i = 0; i < nodes.size(); i++) {
			nodes.get(i).state = parent.nodes.get(i).state;
		}
	}

	public Schema getNextWhiteBetterSchema() {
		// Value schemaValue = calculate();

		Value dum = Value.getWorstValue();

		for (Schema schema : children) {
			Value currentValue = schema.calculate();
			if (currentValue.whiteMovesToTris < dum.whiteMovesToTris)
				dum = currentValue;
		}

		if (dum.schema == null) {
			children.clear();
			fillChildren(true, 2);
			if (children.size() == 0)
				return this;
			else
				return children.get(0);
		}
		return dum.schema;

	}

	public Schema getNextBlackBetterSchema() {
		Value dum = Value.getWorstValue();

		for (Schema schema : children) {
			Value currentValue = schema.calculate();
			if (currentValue.blackMovesToTris < dum.blackMovesToTris)
				dum = currentValue;
		}

		if (dum.schema == null) {
			children.clear();
			fillChildren(false, 2);
			if (children.size() == 0)
				return this;
			else
				return children.get(0);
		}

		return dum.schema;
	}

	public Value calculate() {

		Value value = new Value();

		children.clear();
		if (toKill && lastMovement == 1 || lastMovement == 2)
			fillChildren(true, 4);
		else if (toKill && lastMovement == 2 || lastMovement == 1)
			fillChildren(false, 4);

		value.whiteMovesToTris = getWhiteMovesToMill(4);
		value.blackMovesToTris = getBlackMovesToMill(4);
		value.schema = this;

		// value.

		// Schema bestWhiteKiller = null;
		// Value currentValue = null;
		// for (Schema schema : children) {
		// currentValue = schema.calculate();
		// if (null == bestWhiteKiller) {
		// bestWhiteKiller = schema;
		// } else {
		// // if(currentValue.whiteMovesToTris<bestWhiteKiller.)
		// }
		// }

		return value;
	}

	public int getWhiteMovesToMill(int deep) {
		if (isWhiteInAMill())
			return 0;
		if (deep < 0)
			return 999;

		if (children.size() == 0) {
			if (toKill && lastMovement == 1 || lastMovement == 2)
				fillChildren(true, deep);
			else if (toKill && lastMovement == 2 || lastMovement == 1)
				fillChildren(false, deep);
		}
		int dum = 999;
		for (Schema schema : children) {
			int gwmtt = schema.getWhiteMovesToMill(deep - 1);
			if (gwmtt < dum)
				dum = gwmtt;
		}

		return dum + 1;
	}

	public int getBlackMovesToMill(int deep) {
		if (isBlackInAMill())
			return 0;
		if (deep < 0)
			return 999;

		if (children.size() == 0) {
			if (toKill && lastMovement == 1 || lastMovement == 2)
				fillChildren(true, deep);
			else if (toKill && lastMovement == 2 || lastMovement == 1)
				fillChildren(false, deep);
		}
		int dum = 999;
		for (Schema schema : children) {
			int gwmtt = schema.getBlackMovesToMill(deep - 1);
			if (gwmtt < dum)
				dum = gwmtt;
		}

		return dum + 1;
	}

	public boolean isWhiteInAMill() {
		if (toKill && lastMovement == 1)
			return true;
		return false;
	}

	public boolean isBlackInAMill() {
		if (toKill && lastMovement == 2)
			return true;
		return false;
	}

	public void fillChildren(boolean white, int deep) {
		if (deep < 0)
			return;

		if (white) {
			// If can kill
			if (toKill) {
				// System.out.println("killing black");
				// Node n;
				for (byte i = 0; i < nodes.size(); i++) {
					// n = nodes.get(i);
					if (2 == nodes.get(i).state) {

						if (!isInAMill(nodes.get(i), 2)) {

							Schema scheme = new Schema(this);
							scheme.initNodes();
							scheme.copySchemeFromParent();
							// scheme.whitePiecesToPlace--;
							scheme.blackPiecesInGame--;
							if ((scheme.blackPhase == 1 || scheme.blackPhase == 2)
									&& scheme.blackPiecesInGame == 3)
								scheme.blackPhase = 3;
							if (scheme.blackPiecesInGame < 3
									&& scheme.blackPiecesToPlace == 0)
								winner = 1;

							scheme.nodes.get(i).state = 0;

							scheme.lastMovement = 1;

							children.add(scheme);

							scheme.fillChildren(false, deep - 1);
						}
					}
				}
			} else {
				if (1 == whitePhase) {
					for (byte i = 0; i < nodes.size(); i++) {
						if (0 == nodes.get(i).state) {
							Schema scheme = new Schema(this);
							scheme.initNodes();
							scheme.copySchemeFromParent();
							scheme.whitePiecesToPlace--;
							scheme.whitePiecesInGame++;
							if (scheme.whitePiecesToPlace == 0)
								scheme.whitePhase = 2;

							scheme.nodes.get(i).state = 1;
							scheme.lastMovement = 1;

							children.add(scheme);

							if (isInAMill(scheme.nodes.get(i), 1)
									&& areAllInAMill(2) == false) {
								scheme.toKill = true;
								scheme.fillChildren(true, deep - 1);
							} else {
								scheme.fillChildren(false, deep - 1);
							}
						}
					}
				} else if (2 == whitePhase) {
					Node node;
					for (byte i = 0; i < nodes.size(); i++) {
						node = nodes.get(i);
						if (1 == node.state && node.isFreeNearThisNode()) {

							if (null != node.up) {
								if (0 == node.up.state) {
									Schema scheme = new Schema(this);
									scheme.initNodes();
									scheme.copySchemeFromParent();
									// scheme.whitePiecesToPlace--;
									// scheme.whitePiecesInGame++;
									// if (scheme.whitePiecesToPlace == 0)
									// scheme.whitePhase = 2;

									scheme.nodes.get(i).up.state = 1;
									scheme.nodes.get(i).state = 0;
									scheme.lastMovement = 1;

									children.add(scheme);

									if (isInAMill(scheme.nodes.get(i).up, 1)
											&& areAllInAMill(2) == false) {
										scheme.toKill = true;
										scheme.fillChildren(true, deep - 1);
									} else {
										scheme.fillChildren(false, deep - 1);
									}
								}
							}

							if (null != node.right) {
								if (0 == node.right.state) {
									Schema scheme = new Schema(this);
									scheme.initNodes();
									scheme.copySchemeFromParent();

									scheme.nodes.get(i).right.state = 1;
									scheme.nodes.get(i).state = 0;
									scheme.lastMovement = 1;

									children.add(scheme);

									if (isInAMill(scheme.nodes.get(i).right, 1)
											&& areAllInAMill(2) == false) {
										scheme.toKill = true;
										scheme.fillChildren(true, deep - 1);
									} else {
										scheme.fillChildren(false, deep - 1);
									}
								}
							}

							if (null != node.down) {
								if (0 == node.down.state) {
									Schema scheme = new Schema(this);
									scheme.initNodes();
									scheme.copySchemeFromParent();

									scheme.nodes.get(i).down.state = 1;
									scheme.nodes.get(i).state = 0;
									scheme.lastMovement = 1;

									children.add(scheme);

									if (isInAMill(scheme.nodes.get(i).down, 1)
											&& areAllInAMill(2) == false) {
										scheme.toKill = true;
										scheme.fillChildren(true, deep - 1);
									} else {
										scheme.fillChildren(false, deep - 1);
									}
								}
							}

							if (null != node.left) {
								if (0 == node.left.state) {
									Schema scheme = new Schema(this);
									scheme.initNodes();
									scheme.copySchemeFromParent();

									scheme.nodes.get(i).left.state = 1;
									scheme.nodes.get(i).state = 0;
									scheme.lastMovement = 1;

									children.add(scheme);

									if (isInAMill(scheme.nodes.get(i).left, 1)
											&& areAllInAMill(2) == false) {
										scheme.toKill = true;
										scheme.fillChildren(true, deep - 1);
									} else {
										scheme.fillChildren(false, deep - 1);
									}
								}
							}

						}
					}
				} else if (3 == whitePhase) {
					// for (byte i = 0; i < nodes.size(); i++) {
					// if (1 == nodes.get(i).state) {
					// Schema scheme = new Schema(this);
					// scheme.initNodes();
					// scheme.copySchemeFromParent();
					// // scheme.whitePiecesToPlace--;
					// // scheme.whitePiecesInGame++;
					// // if (scheme.whitePiecesToPlace == 0)
					// // scheme.whitePhase = 2;
					//
					// nodes.get(i).state = 0;
					// scheme.nodes.get(i).state = 1;
					// scheme.lastMovement = 1;
					//
					// children.add(scheme);
					//
					// if (isInAMill(scheme.nodes.get(i), 1)
					// && areAllInAMill(2) == false) {
					// scheme.toKill = true;
					// scheme.fillChildren(true, deep - 1);
					// } else {
					// scheme.fillChildren(false, deep - 1);
					// }
					// }
					// }

				}
			}
		} else {
			// If can kill
			if (toKill) {
				// System.out.println("killing white");

				for (byte i = 0; i < nodes.size(); i++) {
					if (1 == nodes.get(i).state) {
						if (!isInAMill(nodes.get(i), 1)) {
							Schema scheme = new Schema(this);
							scheme.initNodes();
							scheme.copySchemeFromParent();
							// scheme.whitePiecesToPlace--;
							scheme.blackPiecesInGame--;
							if ((scheme.blackPhase == 1 || scheme.blackPhase == 2)
									&& scheme.blackPiecesInGame == 3)
								scheme.blackPhase = 3;
							if (scheme.blackPiecesInGame < 3
									&& scheme.blackPiecesToPlace == 0)
								winner = 2;

							scheme.lastMovement = 2;

							scheme.nodes.get(i).state = 0;

							children.add(scheme);

							scheme.fillChildren(false, deep - 1);
						}
					}
				}
			} else {
				if (1 == blackPhase) {
					for (byte i = 0; i < nodes.size(); i++) {
						if (0 == nodes.get(i).state) {
							Schema scheme = new Schema(this);
							scheme.initNodes();
							scheme.copySchemeFromParent();
							scheme.blackPiecesToPlace--;
							scheme.blackPiecesInGame++;
							if (scheme.blackPiecesToPlace == 0)
								scheme.blackPhase = 2;

							scheme.nodes.get(i).state = 2;
							scheme.lastMovement = 2;

							children.add(scheme);

							if (isInAMill(scheme.nodes.get(i), 2)
									&& areAllInAMill(1) == false) {
								scheme.toKill = true;
								scheme.fillChildren(false, deep - 1);
							} else {
								scheme.fillChildren(true, deep - 1);
							}

						}
					}
				} else if (2 == blackPhase) {
					Node node;
					for (byte i = 0; i < nodes.size(); i++) {
						node = nodes.get(i);
						if (2 == node.state && node.isFreeNearThisNode()) {

							if (null != node.up) {
								if (0 == node.up.state) {
									Schema scheme = new Schema(this);
									scheme.initNodes();
									scheme.copySchemeFromParent();
									// scheme.whitePiecesToPlace--;
									// scheme.whitePiecesInGame++;
									// if (scheme.whitePiecesToPlace == 0)
									// scheme.whitePhase = 2;

									scheme.nodes.get(i).up.state = 2;
									scheme.nodes.get(i).state = 0;
									scheme.lastMovement = 2;

									children.add(scheme);

									if (isInAMill(scheme.nodes.get(i).up, 2)
											&& areAllInAMill(1) == false) {
										scheme.toKill = true;
										scheme.fillChildren(false, deep - 1);
									} else {
										scheme.fillChildren(true, deep - 1);
									}
								}
							}

							if (null != node.right) {
								if (0 == node.right.state) {
									Schema scheme = new Schema(this);
									scheme.initNodes();
									scheme.copySchemeFromParent();

									scheme.nodes.get(i).right.state = 2;
									scheme.nodes.get(i).state = 0;
									scheme.lastMovement = 2;

									children.add(scheme);

									if (isInAMill(scheme.nodes.get(i).right, 2)
											&& areAllInAMill(1) == false) {
										scheme.toKill = true;
										scheme.fillChildren(false, deep - 1);
									} else {
										scheme.fillChildren(true, deep - 1);
									}
								}
							}

							if (null != node.down) {
								if (0 == node.down.state) {
									Schema scheme = new Schema(this);
									scheme.initNodes();
									scheme.copySchemeFromParent();

									scheme.nodes.get(i).down.state = 2;
									scheme.nodes.get(i).state = 0;
									scheme.lastMovement = 2;

									children.add(scheme);

									if (isInAMill(scheme.nodes.get(i).down, 2)
											&& areAllInAMill(1) == false) {
										scheme.toKill = true;
										scheme.fillChildren(false, deep - 1);
									} else {
										scheme.fillChildren(true, deep - 1);
									}
								}
							}

							if (null != node.left) {
								if (0 == node.left.state) {
									Schema scheme = new Schema(this);
									scheme.initNodes();
									scheme.copySchemeFromParent();

									scheme.nodes.get(i).left.state = 2;
									scheme.nodes.get(i).state = 0;
									scheme.lastMovement = 2;

									children.add(scheme);

									if (isInAMill(scheme.nodes.get(i).left, 2)
											&& areAllInAMill(1) == false) {
										scheme.toKill = true;
										scheme.fillChildren(false, deep - 1);
									} else {
										scheme.fillChildren(true, deep - 1);
									}
								}
							}

						}
					}
				} else if (3 == blackPhase) {
					// for (byte i = 0; i < nodes.size(); i++) {
					// if (2 == nodes.get(i).state) {
					// Schema scheme = new Schema(this);
					// scheme.initNodes();
					// scheme.copySchemeFromParent();
					// // scheme.whitePiecesToPlace--;
					// // scheme.whitePiecesInGame++;
					// // if (scheme.whitePiecesToPlace == 0)
					// // scheme.whitePhase = 2;
					//
					// nodes.get(i).state = 0;
					// scheme.nodes.get(i).state = 2;
					// scheme.lastMovement = 2;
					//
					// children.add(scheme);
					//
					// if (isInAMill(scheme.nodes.get(i), 2)
					// && areAllInAMill(1) == false) {
					// scheme.toKill = true;
					// scheme.fillChildren(false, deep - 1);
					// } else {
					// scheme.fillChildren(true, deep - 1);
					// }
					// }
					// }

				}

			}
		}
	}

	public int value(int deep) {
		if (deep < 0)
			return 0;

		if (winner == 1)
			return Integer.MAX_VALUE;

		if (winner == 2)
			return Integer.MIN_VALUE;

		for (Schema schema : children) {
			if (schema.lastMovement == 1 && schema.winner == 1)
				return Integer.MAX_VALUE;
			if (schema.lastMovement == 2 && schema.winner == 2)
				return Integer.MIN_VALUE;
		}

		return 0;
	}

	public void initNodes() {
		nodes = getLinkedNodes();
	}

	public void clearScheme() {
		for (Node n : nodes)
			n.state = 0;
		winner = 0;
		whitePiecesInGame = 0;
		whitePiecesToPlace = 5;
		whitePhase = 1;
		blackPhase = 1;
		blackPiecesInGame = 0;
		blackPiecesToPlace = 5;
		movementCount = 0;
	}

	public String printAllNodes() {
		String result = "";
		// System.out.println("-----------------------------");
		// System.out.println("-----------------------------");
		String[][] g = new String[][] {
				{ nodes.get(0).state + "", "-", nodes.get(1).state + "", "-",
						nodes.get(2).state + "" },
				{ "|", nodes.get(8).state + "", nodes.get(9).state + "",
						nodes.get(10).state + "", "|" },
				{ nodes.get(7).state + "", nodes.get(15).state + "", " ",
						nodes.get(11).state + "", nodes.get(3).state + "" },
				{ "|", nodes.get(14).state + "", nodes.get(13).state + "",
						nodes.get(12).state + "", "|" },
				{ nodes.get(6).state + "", "-", nodes.get(5).state + "", "-",
						nodes.get(4).state + "" }

		};
		for (byte i = 0; i < 5; i++) {
			for (byte j = 0; j < 5; j++) {
				result += g[i][j] + "";
			}
			result += "\n";
		}
		// System.out.println("-----------------------------");

		return result;
		// for(Node n : nodes)
		// System.out.println(n);
	}

	private Node nodeTo;
	private Node nodeFrom;
	private Node nodeToKill;

	/*
	 * 1 = white 2 = black
	 */
	public void execute(byte color) {
		if (winner != 0)
			return;
		movementCount++;
		nodeTo = null;
		nodeFrom = null;

		if (1 == color) {
			switch (whitePhase) {
			case 1:
				nodeTo = getRandomFreeNode(whitePhase, color);
				nodeTo.state = color;
				whitePiecesToPlace--;
				whitePiecesInGame++;
				if (whitePiecesToPlace == 0) {
					// System.out.println("whitePhase: 2");
					whitePhase = 2;
				}
				if (whitePiecesToPlace == 0 && whitePiecesInGame == 3) {
					// System.out.println("whitePhase: 3");
					whitePhase = 3;
				}
				break;

			case 2:

				boolean enableToMove = false;
				for (Node n : nodes)
					if (n.state == color)
						if (n.isFreeNearThisNode())
							enableToMove = true;

				if (!enableToMove) {
					winner = 2;
					return;
				}
				// System.out.println("inTheSecondPhase");
				nodeFrom = getRandomFreeNode(whitePhase, color);
				// System.out.println("nodeFromId: " + nodeFrom.id
				// + " - nodeFromState: " + nodeFrom.state);
				nodeFrom.state = 0;

				nodeTo = getRandomNodeNearANode(nodeFrom);
				// System.out.println("nodeToId: " + nodeTo.id
				// + " - nodeToState: " + nodeTo.state);
				nodeTo.state = color;

				break;

			case 3:
				// System.out.println("inTheThirdPhaseWhite");
				nodeFrom = getRandomFreeNode(whitePhase, color);
				// System.out.println("nodeFromId: " + nodeFrom.id
				// + " - nodeFromState: " + nodeFrom.state);
				nodeFrom.state = 0;

				nodeTo = getRandomFreeNode();
				// System.out.println("nodeToId: " + nodeTo.id
				// + " - nodeToState: " + nodeTo.state);
				nodeTo.state = color;

				break;

			default:
				break;
			}
		} else {
			switch (blackPhase) {
			case 1:
				nodeTo = getRandomFreeNode(blackPhase, color);
				nodeTo.state = color;
				blackPiecesToPlace--;
				blackPiecesInGame++;
				if (blackPiecesToPlace == 0) {
					// System.out.println("blackPhase: 2");
					blackPhase = 2;
				}
				if (blackPiecesToPlace == 0 && blackPiecesInGame == 3) {
					blackPhase = 3;
					// System.out.println("blackPhase: 3");
				}
				break;

			case 2:
				boolean enableToMove = false;
				for (Node n : nodes)
					if (n.state == color)
						if (n.isFreeNearThisNode())
							enableToMove = true;

				if (!enableToMove) {
					winner = 1;
					return;
				}

				nodeFrom = getRandomFreeNode(blackPhase, color);
				nodeFrom.state = 0;

				nodeTo = getRandomNodeNearANode(nodeFrom);
				nodeTo.state = color;

				break;

			case 3:

				// System.out.println("inTheThirdPhaseBlack");
				nodeFrom = getRandomFreeNode(blackPhase, color);
				// System.out.println("nodeFromId: " + nodeFrom.id
				// + " - nodeFromState: " + nodeFrom.state);
				nodeFrom.state = 0;

				nodeTo = getRandomFreeNode();
				// System.out.println("nodeToId: " + nodeTo.id
				// + " - nodeToState: " + nodeTo.state);
				nodeTo.state = color;

				break;

			default:
				break;
			}

		}

		if (isInAMill(nodeTo, color)) {
			if (areAllInAMill(getReverseColor(color)))
				return;
			nodeToKill = getRandomNodeToKill(getReverseColor(color));
			nodeToKill.state = 0;
			if (color == 1) {
				blackPiecesInGame--;
				if (blackPiecesInGame == 3 && blackPiecesToPlace == 0) {
					blackPhase = 3;
				} else if (blackPiecesInGame < 3 && blackPiecesToPlace == 0) {
					// System.out.println("BLACK DIE ~~~~~~~~~~~~~~~~~~~~");
					winner = 1;
				}
			} else {
				whitePiecesInGame--;
				if (whitePiecesInGame == 3 && whitePiecesToPlace == 0) {
					whitePhase = 3;
				} else if (whitePiecesInGame < 3 && whitePiecesToPlace == 0) {
					// System.out.println("WHITE DIE ~~~~~~~~~~~~~~~~~~~~");
					winner = 2;
				}
			}
		}

		// System.out.println(color == 1 ? "whiteInGame: " + whitePiecesInGame
		// : "blackInGame: " + blackPiecesInGame);
	}

	private boolean areAllInAMill(int i) {
		for (Node node : nodes)
			if (node.state == i)
				if (!isInAMill(node, i))
					return false;

		return true;

	}

	public Node grfnNode;

	public Node getRandomFreeNode() {
		do {
			grfnNode = nodes.get(Tester.random.nextInt(16));
		} while (0 != grfnNode.state);
		return grfnNode;
	}

	public Node grfn2;

	public Node getRandomFreeNode(byte state, byte color) {
		grfn2 = null;
		switch (state) {
		case 1:
			grfn2 = getRandomFreeNode();
			break;

		case 2:
			// System.out.println("getRandomFreeNode: " + color);
			do {
				grfn2 = nodes.get(Tester.random.nextInt(16));
			} while (!grfn2.isFreeNearThisNode() || grfn2.state != color);

			break;
		case 3:

			do {
				grfn2 = nodes.get(Tester.random.nextInt(16));
			} while (grfn2.state != color);

			break;

		default:
			break;
		}
		return grfn2;
	}

	public Node getRandomNodeNearANode(Node node) {
		Node n = null;
		do {
			do {
				n = node.get(Tester.random.nextInt(4));
			} while (null == n);
		} while (0 != n.state);
		return n;
	}

	public boolean isInAMill(Node node, int i) {
		if (null == node)
			return false;
		if (i != node.state)
			return false;

		byte cnt = 1;

		Node dum = node;

		// Vertical
		while (null != dum.up) {
			dum = dum.up;
			if (dum.state == i)
				cnt++;
		}
		dum = node;
		while (null != dum.down) {
			dum = dum.down;
			if (dum.state == i)
				cnt++;
		}
		if (cnt == 3)
			return true;

		cnt = 1;
		// Horizontal
		dum = node;
		while (null != dum.right) {
			dum = dum.right;
			if (dum.state == i)
				cnt++;
		}
		dum = node;
		while (null != dum.left) {
			dum = dum.left;
			if (dum.state == i)
				cnt++;
		}
		if (cnt == 3)
			return true;

		return false;

	}

	public Node getRandomNodeToKill(byte color) {
		Node node;
		do {
			do {
				node = nodes.get(Tester.random.nextInt(16));
			} while (null == node);
		} while (isInAMill(node, color) || node.state != color);
		return node;
	}

	private byte getReverseColor(byte color) {
		if (color == 1)
			return 2;
		else
			return 1;
	}

}
