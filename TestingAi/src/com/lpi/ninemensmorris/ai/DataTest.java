package com.lpi.ninemensmorris.ai;

import java.util.Random;

public class DataTest {

	public String name;
//	public String name;
//	public String name;

	public long startTime;
	public long finalTime = 0;

	public byte cntWhite;
	public byte cntBlack;
	public int deep;
	
	public Random random;
	
	public Schema first;

	public long totalTime() {
		if (0 == finalTime)
			finalTime = System.currentTimeMillis();
		return finalTime - startTime;
	}

	/**
	 * @param cntWhite
	 * @param cntBlack
	 * @param deep
	 */
	public DataTest(String name, byte cntWhite, byte cntBlack, int deep) {
		super();
		this.name = name;
		this.cntWhite = cntWhite;
		this.cntBlack = cntBlack;
		this.deep = deep;
		random = new Random();
	}
	
	public DataTest(String name, long seed, byte cntWhite, byte cntBlack, int deep) {
		super();
		this.name = name;
		this.cntWhite = cntWhite;
		this.cntBlack = cntBlack;
		this.deep = deep;
		random = new Random(seed);
	}
	
	public void init(){ 
		this.startTime = System.currentTimeMillis();

		first = new Schema();
		first.initNodes();
		first.whitePhase = 2;
		first.blackPhase = 2;
		first.whitePiecesInGame = cntWhite;
		first.blackPiecesInGame = cntBlack;
		first.whitePiecesToPlace = 0;
		first.blackPiecesToPlace = 0;

		Node node;
		for (int i = 0; i < cntWhite; i++) {
			do {
				node = first.nodes.get(random.nextInt(16));
			} while (node.state != 0);
			node.state = 1;
		}

		for (int i = 0; i < cntBlack; i++) {
			do {
				node = first.nodes.get(random.nextInt(16));
			} while (node.state != 0);
			node.state = 2;
		}

		first.fillChildren(true, deep);
		this.finalTime = System.currentTimeMillis();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DataTest [name=");
		builder.append(name);
		builder.append(", cntWhite=");
		builder.append(cntWhite);
		builder.append(", cntBlack=");
		builder.append(cntBlack);
		builder.append(", deep=");
		builder.append(deep);
		builder.append(", totalTime=");
		builder.append(totalTime());
		builder.append("]");
		return builder.toString();
	}
	
	

}
