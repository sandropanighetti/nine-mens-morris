package com.lpi.ninemensmorris.ai;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class TreeTester extends Applet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8726598043538125959L;

	public static Random random;
	Schema schema;

	public List<Schema> bettersSchema;

	private int schemeWidth = 40;
	private int schemeHeight = 40;
	private int ballWidth = 10;
	private int heightSpacing = 2;

	private int[][] nodePosition;
	private int[] sel;

	public void init() {
		random = new Random(1L);
		System.out.println("-------------START-------------");

		nodePosition = new int[][] { { 0, 0 }, { 0, 2 }, { 0, 4 }, { 2, 4 },
				{ 4, 4 }, { 4, 2 }, { 4, 0 }, { 2, 0 }, { 1, 1 }, { 1, 2 },
				{ 1, 3 }, { 2, 3 }, { 3, 3 }, { 3, 2 }, { 3, 1 }, { 2, 1 } };

		long startTime = System.currentTimeMillis();

		List<DataTest> tests = new LinkedList<DataTest>();

		// tests.add(new DataTest("1�", (byte) 5, (byte) 5, 1));
		// tests.add(new DataTest("2�", (byte) 5, (byte) 5, 2));
		// tests.add(new DataTest("3�", (byte) 5, (byte) 5, 3));
		// tests.add(new DataTest("4�", (byte) 5, (byte) 5, 4));
		// tests.add(new DataTest("5�", (byte) 5, (byte) 5, 5));
		// tests.add(new DataTest("6�", (byte) 5, (byte) 5, 6));
		// tests.add(new DataTest("7�", (byte) 5, (byte) 5, 7));
		// tests.add(new DataTest("8�", (byte) 5, (byte) 5, 7));

		// tests.add(new DataTest("9�", (byte) 5, (byte) 4, 1));
		// tests.add(new DataTest("10�", (byte) 5, (byte) 4, 2));
		// tests.add(new DataTest("11�", (byte) 5, (byte) 4, 3));
		// tests.add(new DataTest("12�", (byte) 5, (byte) 4, 4));
		// tests.add(new DataTest("13�", (byte) 5, (byte) 4, 5));
		// tests.add(new DataTest("14�", (byte) 5, (byte) 4, 6));
		// tests.add(new DataTest("15�", (byte) 5, (byte) 4, 7));
		// tests.add(new DataTest("16�", (byte) 5, (byte) 4, 8));
		//
		// tests.add(new DataTest("17�", (byte) 4, (byte) 4, 1));
		// tests.add(new DataTest("18�", (byte) 4, (byte) 4, 2));
		// tests.add(new DataTest("19�", (byte) 4, (byte) 4, 3));
		// tests.add(new DataTest("20�", (byte) 4, (byte) 4, 4));
		// tests.add(new DataTest("21�", (byte) 4, (byte) 4, 5));
		// tests.add(new DataTest("22�", (byte) 4, (byte) 4, 6));
		// tests.add(new DataTest("23�", (byte) 4, (byte) 4, 7));

		// for (DataTest dt : tests) {
		// dt.init();
		// System.out.println(dt);
		// }

		sel = new int[] { 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
				0 };
		// schema = tests.get(5).first;
		DataTest t = new DataTest("test", 1L, (byte) 5, (byte) 5, 0);
		t.init();

		bettersSchema = new LinkedList<Schema>();

		schema = t.first;
		Schema a = t.first;
		Schema b = a.getNextWhiteBetterSchema();
		Schema c = b.getNextBlackBetterSchema();
		Schema d = c.getNextWhiteBetterSchema();
		Schema e = d.getNextWhiteBetterSchema();
		Schema f = e.getNextBlackBetterSchema();
		Schema g = f.getNextBlackBetterSchema();
		Schema h = g.getNextWhiteBetterSchema();
		Schema i = h.getNextBlackBetterSchema();
		Schema j = i.getNextWhiteBetterSchema();

		bettersSchema.add(a);
		bettersSchema.add(b);
		bettersSchema.add(c);
		bettersSchema.add(d);
		bettersSchema.add(e);
		bettersSchema.add(f);
		bettersSchema.add(g);
		bettersSchema.add(h);
		bettersSchema.add(i);
		bettersSchema.add(j);
		
		

//		bettersSchema.add(schema.getNextWhiteBetterSchema());
//		bettersSchema.add(bettersSchema.get(bettersSchema.size() - 1)
//				.getNextBlackBetterSchema());
//		bettersSchema.add(bettersSchema.get(bettersSchema.size() - 1)
//				.getNextWhiteBetterSchema());
//		bettersSchema.add(bettersSchema.get(bettersSchema.size() - 1)
//				.getNextWhiteBetterSchema());
//		
//
//		bettersSchema.add(bettersSchema.get(bettersSchema.size() - 1)
//				.getNextBlackBetterSchema());
//		bettersSchema.get(bettersSchema.size()-1).children.clear();
//		bettersSchema.add(bettersSchema.get(bettersSchema.size() - 1)
//				.getNextBlackBetterSchema());
//		bettersSchema.add(bettersSchema.get(bettersSchema.size() - 1)
//				.getNextWhiteBetterSchema());
//		bettersSchema.add(bettersSchema.get(bettersSchema.size() - 1)
//				.getNextWhiteBetterSchema());

		// bettersSchema.add(schema.getNextWhiteBetterSchema());
		// if (bettersSchema.get(bettersSchema.size() - 1).toKill)
		// bettersSchema.add(bettersSchema.get(bettersSchema.size() - 1)
		// .getNextWhiteBetterSchema());
		//
		// bettersSchema.add(schema.getNextBlackBetterSchema());
		// if (bettersSchema.get(bettersSchema.size() - 1).toKill)
		// bettersSchema.add(bettersSchema.get(bettersSchema.size() - 1)
		// .getNextBlackBetterSchema());
		// bettersSchema.add(bettersSchema.get(0).get)
		// for (int i = 0; i < 8; i++) {
		// bettersSchema.add(schema.getNextWhiteBetterSchema());
		// if (bettersSchema.get(bettersSchema.size() - 1).toKill)
		// bettersSchema.add(bettersSchema.get(bettersSchema.size() - 1)
		// .getNextWhiteBetterSchema());
		//
		// bettersSchema.add(schema.getNextBlackBetterSchema());
		// if (bettersSchema.get(bettersSchema.size() - 1).toKill)
		// bettersSchema.add(bettersSchema.get(bettersSchema.size() - 1)
		// .getNextBlackBetterSchema());
		// }
		System.out.println("schema.getWhiteMovesToMill(4): "
				+ schema.getWhiteMovesToMill(4));
		System.out.println("schema.getBlackMovesToMill(4): "
				+ schema.getBlackMovesToMill(4));

		// Schema dum = schema;
		//
		// for (int i = 0; i < sel.length && dum.children.size() != 0; i++) {
		// dum = dum.children.get(sel[i + 1]);
		// }
		//
		// dum.fillChildren(true, 6);
		//
		// Schema dum2 = dum;
		// while (dum2.children.size() != 0) {
		// dum2 = dum2.children.get(0);
		// }
		// dum2.fillChildren(true, 5);

		// test.init();
		// System.out.println(test.toString());

		// first = new Schema();
		// first.initNodes();
		//
		// for (int i = 0; i < 5; i++) {
		// first.nodes.get(i).state = 1;
		// }
		// first.whitePhase = 2;
		// first.whitePiecesInGame = 5;
		// first.whitePiecesToPlace = 0;
		//
		// for (int i = 0; i < 5; i++) {
		// first.nodes.get(i + 5).state = 2;
		// }
		// first.blackPhase = 2;
		// first.blackPiecesInGame = 5;
		// first.blackPiecesToPlace = 0;

		// first = getNewRandomSecondPhaseScheme((byte) 5, (byte) 5, 7);

		// Test killing
		// first.nodes.get(0).state=1;
		// first.nodes.get(1).state=1;
		// first.nodes.get(3).state=1;
		// first.nodes.get(4).state=1;
		// first.whitePiecesInGame=4;
		// first.whitePiecesToPlace=1;
		//
		// first.nodes.get(5).state=2;
		// first.nodes.get(6).state=2;
		// first.nodes.get(9).state=2;
		// first.nodes.get(10).state=2;
		// first.blackPiecesInGame=4;
		// first.blackPiecesToPlace=1;
		//
		// for (int i = 0; i < 0; i++) {
		// first.nodes.get(i).state = (byte) (1 + i % 2);
		// }

		// byte t = 0;
		// int t1 = 23;
		// System.out.println("t: " + t);
		// t = (byte) t1;
		// System.out.println("t: " + t);

		// first.fillChildren(true, 8);

		// System.out.println("first.children.size(): " +
		// first.children.size());
		// System.out.println("first.children.get(0).children.size(): "
		// + first.children.get(0).children.size());

		long finalTime = System.currentTimeMillis();
		long totalTime = finalTime - startTime;
		System.out.println("-------------STOP-------------");
		System.out.println("Total time: " + totalTime);

		repaint();
	}

	public void paint(Graphics g) {

		g.setColor(Color.white);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(Color.black);

//		drawSchema(g, schema, 10 + (1) * schemeWidth / 2, 0 * schemeHeight
//				+ (1) * schemeHeight / 2);

		int cnt = 1;
		for (Schema s : bettersSchema) {
			// g.setc

			if(cnt==8)
				System.out.println("cnt: " + cnt);
			
			drawSchema(g, s, 10 + (1) * schemeWidth / 2, cnt * schemeHeight
					+ (1 + cnt) * schemeHeight / 2);
			

			if (s.lastMovement == 1)
				g.setColor(Color.cyan);
			else
				g.setColor(Color.red);

			g.drawLine(10 + (1) * schemeWidth, (cnt) * schemeHeight + (cnt)
					* schemeHeight / 2, 10 + (1) * schemeWidth, (cnt)
					* schemeHeight + (1 + cnt) * schemeHeight / 2);
			cnt++;
		}

		// int n = 0;
		// drawSchema(g, schema, 10 + (sel[0] + 1) * schemeWidth / 2,
		// schemeHeight);
		// drawChildren(g, schema.children, 10, schemeHeight * 2 + schemeHeight,
		// n, 20);
	}

	public void drawChildren(Graphics g, List<Schema> children, int x, int y,
			int n, int deep) {

		if (deep < 0)
			return;

		int nu = sel[n];

		int cnt = 0;
		for (Schema schema : children) {
			// g.drawRect(x + cnt * schemeWidth, y, schemeWidth, schemeHeight);
			drawSchema(g, schema,
					x + cnt * schemeWidth + cnt * schemeWidth / 2, y);

			try {
				if (children.get(sel[n - 1]).parent != null) {
					if (children.get(sel[n - 1]).lastMovement == 1)
						g.setColor(Color.cyan);
					else
						g.setColor(Color.red);

					g.drawLine(x + (nu) * schemeWidth / 2 + (nu + 1)
							* schemeWidth / 2, y - schemeHeight
							* (heightSpacing - 1), x + cnt * schemeWidth
							+ (cnt + 1) * schemeWidth / 2, y);
				}
			} catch (Exception e) {
				// e.printStackTrace();
			}
			cnt++;
		}
		if (children.size() > 0) {
			// n %= children.size();
			// System.out.println("deep: " + deep);
			try {

				drawChildren(g, children.get(sel[n + 1]).children, x
						+ sel[n + 1] * schemeWidth / 2, y + schemeHeight
						* heightSpacing, n + 1, deep - 1);
				// if (deep == 3) {
				// children.get(sel[n - 1]).fillChildren(true, 5);
				// drawChildren(g, children.get(sel[n - 1]).children, x
				// + sel[n + 1] * schemeWidth / 2, y + schemeHeight
				// * heightSpacing, 0, deep - 1);
				// }
			} catch (Exception e) {
				drawChildren(g, children.get(0).children, x + 0 * schemeWidth
						/ 2, y + schemeHeight * heightSpacing, n + 1, deep - 1);
				// if(deep==3)
				// System.out.println("catch");
			}

		}
	}

	public void drawSchema(Graphics g, Schema schema, int x, int y) {
		g.setColor(Color.black);
		g.drawRect(x, y, schemeWidth, schemeHeight);
		g.drawRect(x + schemeWidth / 4, y + schemeHeight / 4, schemeWidth / 2,
				schemeHeight / 2);

		g.drawLine(x + schemeWidth / 2, y, x + schemeWidth / 2, y
				+ schemeHeight / 4);
		g.drawLine(x + schemeWidth / 2, y + schemeHeight * 3 / 4, x
				+ schemeWidth / 2, y + schemeHeight);
		g.drawLine(x, y + schemeHeight / 2, x + schemeWidth / 4, y
				+ schemeHeight / 2);
		g.drawLine(x + schemeWidth * 3 / 4, y + schemeHeight / 2, x
				+ schemeWidth, y + schemeHeight / 2);

		byte cnt = 0;
		for (Node node : schema.nodes) {
			if (node.state == 1)
				drawBall(g, Color.blue, x + schemeWidth * nodePosition[cnt][1]
						/ 4, y + schemeWidth * nodePosition[cnt][0] / 4);
			else if (node.state == 2)
				drawBall(g, Color.red, x + schemeWidth * nodePosition[cnt][1]
						/ 4, y + schemeWidth * nodePosition[cnt][0] / 4);
			cnt++;
		}

	}

	public void drawBall(Graphics g, Color color, int x, int y) {
		g.setColor(color);
		g.fillOval(x - ballWidth / 2, y - ballWidth / 2, ballWidth, ballWidth);
	}

}
