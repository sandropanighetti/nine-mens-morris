package com.lpi.ninemensmorris.ai;

import java.util.Random;

public class Tester {

	public static Schema scheme;

	public static Random random;

	public static void main(String[] args) {
		random = new Random();
		// random = new Random(1L);

		scheme = new Schema();
		scheme.initNodes();
		scheme.clearScheme();

		int numberOfMatch = 10000;
		int whiteWins = 0;
		int blackWins = 0;

		long startTime = System.currentTimeMillis();
		for (int cnt = 0; cnt < numberOfMatch; cnt++) {
			scheme.clearScheme();
			// random = new Random();
			while (scheme.winner == 0) {

				scheme.execute((byte) 1);
				scheme.execute((byte) 2);
				// }
			}
			// System.out.println("Winner: " + scheme.winner
			// + " - movementCount: " + scheme.movementCount);
			if (scheme.winner == 1)
				whiteWins++;
			else
				blackWins++;
			// System.out.println("cnt: " + cnt);
		}
		long endTime = System.currentTimeMillis();

		System.out.println("-----------------------");
		System.out.println("Time: " + (endTime - startTime) + " millis");
		System.out.println("Number of match: " + numberOfMatch);
		System.out.println("White wins: " + whiteWins);
		System.out.println("Black wins: " + blackWins);
	}

}
