package com.lpi.ninemensmorris.ai;

public class Value{
	
	public static Value getWorstValue(){
		Value value = new Value();
		value.blackMovesToBlockATris = 999;
		value.blackMovesToTris = 999;
		value.whiteMovesToBlockATris = 999;
		value.whiteMovesToTris = 999;
		return value;
	}

	public int whiteMovesToTris = Integer.MAX_VALUE;
	public int blackMovesToTris = Integer.MAX_VALUE;
	public int whiteMovesToBlockATris = Integer.MAX_VALUE;
	public int blackMovesToBlockATris = Integer.MAX_VALUE;
	public Schema schema;

	// public int blackMoves
	/**
	 * @param whiteMovesToTris
	 * @param blackMovesToTris
	 * @param whiteMovesToBlockATris
	 * @param blackMovesToBlockATris
	 */
	public Value(int whiteMovesToTris, int blackMovesToTris,
			int whiteMovesToBlockATris, int blackMovesToBlockATris) {
		super();
		this.whiteMovesToTris = whiteMovesToTris;
		this.blackMovesToTris = blackMovesToTris;
		this.whiteMovesToBlockATris = whiteMovesToBlockATris;
		this.blackMovesToBlockATris = blackMovesToBlockATris;
	}

	/**
	 * 
	 */
	public Value() {
		super();
	}

	


}
