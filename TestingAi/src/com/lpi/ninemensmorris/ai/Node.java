package com.lpi.ninemensmorris.ai;

/**
 * @author Sandro Panighetti - 20-feb-2013 TestingAI
 */
public class Node {

	public byte id;

	/**
	 * 0 = empty 
	 * 1 = white 
	 * 2 = black
	 */
	public byte state;

	public Node up;
	public Node right;
	public Node down;
	public Node left;

	/**
	 * 
	 */
	public Node() {
		super();
	}

	/**
	 * 
	 */
	public Node(byte id) {
		super();
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Node [id=");
		builder.append(id);
		builder.append(", state=");
		builder.append(state);
		builder.append("]");
		return builder.toString();
	}

	public byte getNumberOfNeighbour() {
		byte cnt = 0;
		if (null != up)
			cnt++;
		if (null != right)
			cnt++;
		if (null != down)
			cnt++;
		if (null != left)
			cnt++;

		return cnt;

	}

	public boolean isFreeNearThisNode() {
		if(null != up)
			if(0 == up.state)
				return true;
		if(null != right)
			if(0 == right.state)
				return true;
		if(null != down)
			if(0 == down.state)
				return true;
		if(null != left)
			if(0 == left.state)
				return true;
		return false;
	}

	public Node get(int i){
		if(i==0)
			return up;
		if(i==1)
			return right;
		if(i==2)
			return down;
		if(i==3)
			return left;
		return null;
	}
	
}
