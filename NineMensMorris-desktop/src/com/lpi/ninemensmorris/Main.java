package com.lpi.ninemensmorris;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "NineMensMorris";
		cfg.useGL20 = true;
		cfg.width = 400;
		cfg.height = 600;
		cfg.height = 600;
//		cfg.width = 320*2;
//		cfg.height = 480*2;
		
		new LwjglApplication(new NineMensMorris(), cfg);
	}
}
