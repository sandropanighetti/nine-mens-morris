package com.lpi.ninemensmorris.tris;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Schema {

	/*
	 * 0 = empty 1 = x 2 = o
	 */
	private int[][] table;

	/*
	 * 1 = x 2 = o
	 */
	public int lastInsert = 0;

	public int numberOfBlueTris = 0;
	public int numberOfRedTris = 0;
	public int depthFromBlueTris = 999;
	public int depthFromRedTris = 999;

	private int x;
	private int y;
	private int width;
	private int height;
	public boolean visible = false;

	public int id;

	private Schema parent;

	private List<Schema> children;

	public Schema() {
		super();
		table = new int[3][3];
		children = new LinkedList<Schema>();
	}

	public Schema(Schema parent) {
		this.parent = parent;
		table = new int[3][3];
		children = new LinkedList<Schema>();
	}

	public void copySchemeFromParent() {
		// if (null == parent)
		// throw new NullPointerException("parent is null");

		table = new int[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				table[i][j] = parent.table[i][j];
			}
		}
	}

	public int getNumberOfChildren() {
		return children.size();
	}

	public int getNumberOfEmptyCells() {
		int cnt = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (table[i][j] == 0)
					cnt++;
			}
		}
		return cnt;
	}

	public void fillChildren() {
		if (tris() == 1) {
			parent.numberOfBlueTris++;
			parent.depthFromBlueTris = 1;
			depthFromBlueTris = 0;
			return;
		} else if (tris() == 2) {
			parent.numberOfRedTris++;
			parent.depthFromRedTris = 1;
			depthFromRedTris = 0;
			return;
		}

		int index = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (table[i][j] == 0) {
					addNewChildren(index, i, j);
					index++;
				}
			}
		}

		// int minWhiteDepth=999;
		// int minBlackDepth=999;
		if (parent != null) {
			for (Schema s : children) {
				if (parent.depthFromBlueTris > s.depthFromBlueTris)
					parent.depthFromBlueTris = s.depthFromBlueTris + 1;
				if (parent.depthFromRedTris > s.depthFromRedTris)
					parent.depthFromRedTris = s.depthFromRedTris + 1;

			}

			parent.numberOfBlueTris += numberOfBlueTris;
			parent.numberOfRedTris += numberOfRedTris;
		}
		/*
		 * int numberOfEmptyCells = getNumberOfEmptyCells(); for (int k = 0; k <
		 * numberOfEmptyCells; k++) { addNewChildren(k); }
		 */
	}

	private void addNewChildren(int index, int i, int j) {
		Schema dum = new Schema(this);
		children.add(dum);
		dum.copySchemeFromParent();
		dum.lastInsert = lastInsert == 1 ? 2 : 1;
		dum.id = index;

		dum.table[i][j] = dum.lastInsert;
		/*
		 * int cnt = 0; mainLoop: for (int i = 0; i < 3; i++) { for (int j = 0;
		 * j < 3; j++) { if (dum.table[i][j] == 0) { if (cnt == index) {
		 * dum.table[i][j]=dum.lastInsert; break mainLoop; } cnt++; } } }
		 */
		dum.fillChildren();

	}

	/*
	 * 0 = nessuno 1 = x 2 = o
	 */
	public int tris() {
		if (tris(1))
			return 1;
		else if (tris(2))
			return 2;
		return 0;
	}

	public boolean tris(int colore) {
		int cnt = 0;
		// Righe
		for (int i = 0; i < 3; i++) {
			cnt = 0;
			for (int j = 0; j < 3; j++) {
				if (table[i][j] == colore)
					cnt++;
			}
			if (cnt == 3)
				return true;
		}

		// Colonne
		for (int i = 0; i < 3; i++) {
			cnt = 0;
			for (int j = 0; j < 3; j++) {
				if (table[j][i] == colore)
					cnt++;
			}
			if (cnt == 3)
				return true;
		}

		if (table[0][0] == colore && table[1][1] == colore
				&& table[2][2] == colore)
			return true;

		if (table[0][2] == colore && table[1][1] == colore
				&& table[2][0] == colore)
			return true;

		return false;

	}

	public int value(int depth) {
		int t = tris();
		if (t == 1 && lastInsert == 1)
			return 999;
		else if (t == 2 && lastInsert == 2)
			return -999;

		if (depth < 0)
			return 0;

		for (Schema scheme : children) {
			if (scheme.value(depth - 1) == -999)
				return -999;
			if (scheme.value(depth - 1) == 999)
				return 999;

		}

		return 0;
	}

	public int get(int i, int j) {
		return table[i][j];
	}

	public void set(int i, int j, int value) {
		table[i][j] = value;
	}

	public String tableToString() {
		String result = "";
		result += Arrays.toString(table[0]) + "\n";
		result += Arrays.toString(table[1]) + "\n";
		result += Arrays.toString(table[2]) + "\n";
		return result;
	}

	public String parentsToString() {
		Schema scheme = parent;
		String result = "";
		while (scheme != null) {
			result = scheme.id + " -> " + result;
			scheme = scheme.parent;
		}
		result += id + "";
		return result;
	}

	public List<Schema> getChildren() {
		return children;
	}

	public int getNumberOfParents() {
		Schema scheme = parent;
		int cnt = 0;
		while (scheme != null) {
			scheme = scheme.parent;
			cnt++;
		}
		return cnt;
	}

	public Schema getBestSchema(int colore) {

		Schema bestSchema = null;
		int bestValue = 0;
		for (Schema schema : children) {
			int currentValue = schema.value(18);
			// System.out.println(currentValue);
			if (colore == 1 && currentValue > bestValue || colore == 2
					&& currentValue < bestValue) {
				bestSchema = schema;
				bestValue = currentValue;
			}
		}

		return bestSchema;
	}

	public Schema getParent() {
		return parent;
	}

	public void setParent(Schema parent) {
		this.parent = parent;
	}

	public boolean isVisible() {
		return visible;
	}

	public Schema getAtPosition(int x, int y) {
		// if(!visible)
		// return null;

		if (this.contains(x, y))
			return this;

		for (Schema s : children) {
			if (s.visible) {
				Schema dum = s.getAtPosition(x, y);
				if (dum != null)
					return dum;
			}
		}

		return null;
	}

	public boolean contains(int x, int y) {
		if (this.x - this.width / 2 <= x && this.x + this.width / 2 >= x
				&& this.y - this.height / 2 < y && this.y + this.height / 2 > y)
			return true;
		return false;
	}
	
	public void setChildrenVisible(boolean visible){
//		System.out.println("setChildrenVisible");
		for (Schema s : children)
			s.visible = visible;

		setX(x);
		setY(y);
	}

	public void setVisible(boolean visible) {
		if (!visible) {
			for (Schema s : children)
				s.setVisible(false);
		} else {
			setChildrenVisible(true);
		}
		setX(x);
		setY(y);
		this.visible = visible;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
		int childrenSize = children.size();
		int cnt = 0;
		int totalSchemeWidth = width + 100;
		for (Schema s : children) {
			if (s.isVisible()) {
				s.setX(this.x +totalSchemeWidth/2- (childrenSize / 2) * totalSchemeWidth + cnt
						* totalSchemeWidth);
				s.setWidth(width);
				s.setHeight(height);
//				s.setY(y+height*3/2);
				cnt++;
			}
		}
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
		int childrenSize = children.size();
		int cnt = 0;
		int totalSchemeWidth = width + 100;
		for (Schema s : children) {
			if (s.isVisible()) {
//				s.setX(this.x +totalSchemeWidth/2- (childrenSize / 2) * totalSchemeWidth + cnt
//						* totalSchemeWidth);
				s.setWidth(width);
				s.setHeight(height);
				s.setY(y+height*3/2);
				cnt++;
			}
		}
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

}
