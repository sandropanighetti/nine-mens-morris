package com.lpi.ninemensmorris.tris;

public class TrisTester {

	public static void main(String[] args) {
		System.out.println("----------------------START----------------------");
		long startTime = System.currentTimeMillis();
		Schema first = new Schema();
//		Schema first = new Schema();

		first.set(0, 1, 1);
		first.set(1, 2, 1);
		first.set(2, 0, 1);
		first.set(1, 1, 2);
		first.set(2, 2, 2);
//		first.set(2, 0, 2);
		
//		first.table[0][1] = 1;
//		first.table[1][2] = 1;
//		first.table[2][0] = 1;
//		first.table[1][1] = 2;
//		first.table[2][2] = 2;
		first.lastInsert = 1;

		first.fillChildren();
		long finalTime = System.currentTimeMillis();
		long totalTime = finalTime - startTime;

		printAllChildren(first);

		System.out.println("won: " + first.value(7));
		/*
		 * Scheme t = new Scheme(); int c = 2; t.table[0][0] = c; t.table[0][1]
		 * = c; t.table[1][2] = c; System.out.println(t.tableToString());
		 * 
		 * System.out.println("t.tris("+c+") = " +t.tris(c));
		 */

		// System.out.println("First node:");
		// System.out.println(first.tableToString());
		//
		//
		// System.out.println("First node - 0:");
		// System.out.println(first.getChildren().get(0).tableToString());
		//
		// System.out.println("First node - 0 - 0:");
		// System.out.println(first.getChildren().get(0).getChildren().get(0).tableToString());
		//
		// System.out.println("First node - 0 - 1:");
		// System.out.println(first.getChildren().get(0).getChildren().get(1).tableToString());
		//
		// System.out.println("First node - 0 - 2:");
		// System.out.println(first.getChildren().get(0).getChildren().get(2).tableToString());
		//
		// System.out.println("First node - 0 - 3:");
		// System.out.println(first.getChildren().get(0).getChildren().get(3).tableToString());
		//
		// System.out.println("First node - 1:");
		// System.out.println(first.getChildren().get(1).tableToString());
		//
		// System.out.println("first.numberOfChildrens(): "
		// + first.numberOfChildren());

		System.out.println("Total time: " + totalTime);
		System.out.println("----------------------STOP----------------------");
	}

	public static void printAllChildren(Schema first) {

		System.out.println("Node: " + first.parentsToString());
		System.out.println(first.tableToString());
		if (first.getNumberOfParents() < 4) {
			for (Schema s : first.getChildren())
				printAllChildren(s);
		}
	}
}
