package com.lpi.ninemensmorris.tris;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class AppletTester extends Applet implements MouseListener,
		MouseMotionListener {

	Schema first;

	// int schemaWidth = 30;
	// int schemaHeight = 30;

	public void init() {
		first = new Schema();
		// first.set(0, 1, 1);
		// first.set(1, 2, 1);
		// first.set(2, 0, 1);
		// first.set(1, 1, 2);
		// first.set(2, 2, 2);
		first.fillChildren();
		first.setX(this.getWidth() / 2);
		first.setY(30);
		first.setWidth(40);
		first.setHeight(40);
		first.setVisible(false);
		first.visible = true;
		// first.getChildren().get(0).setVisible(true);

		// System.out.println("first.numberOfBlueTris: " +
		// first.numberOfBlueTris);
		// System.out.println("first.numberOfRedTris: " +
		// first.numberOfRedTris);
		// System.out.println("first.depthFromBlueTris: "
		// + first.depthFromBlueTris);
		// System.out.println("first.depthFromRedTris: " +
		// first.depthFromRedTris);

		addMouseListener(this);
		addMouseMotionListener(this);
	}

	public void paint(Graphics g) {

		drawSchema(g, first);

	}

	public void drawSchema(Graphics g, Schema schema) {
		g.setColor(Color.black);
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (schema.get(i, j) > 0) {
					if (schema.get(i, j) != 0) {
						if (schema.get(i, j) == 1)
							g.setColor(Color.blue);
						else if (schema.get(i, j) == 2)
							g.setColor(Color.red);

						g.fillRect(schema.getX() - schema.getWidth() / 2 + j
								* schema.getWidth() / 3 + 1,
								schema.getY() - schema.getHeight() / 2 + i
										* schema.getHeight() / 3 + 1,
								schema.getHeight() / 3, schema.getHeight() / 3);
					}

				}
			}
		}

		g.setColor(Color.black);
		g.drawRect(schema.getX() - schema.getWidth() / 2, schema.getY()
				- schema.getHeight() / 2, schema.getWidth(), schema.getHeight());
		g.drawLine(schema.getX() - schema.getWidth() / 2 + schema.getWidth()
				* 1 / 3, schema.getY() - schema.getHeight() / 2, schema.getX()
				- schema.getWidth() / 2 + schema.getWidth() * 1 / 3,
				schema.getY() + schema.getHeight() / 2);

		g.drawLine(schema.getX() - schema.getWidth() / 2 + schema.getWidth()
				* 2 / 3, schema.getY() - schema.getHeight() / 2, schema.getX()
				- schema.getWidth() / 2 + schema.getWidth() * 2 / 3,
				schema.getY() + schema.getHeight() / 2);

		g.drawLine(schema.getX() - schema.getWidth() / 2, 
				schema.getY()- schema.getHeight() / 2 + schema.getHeight() * 1 / 3,
				schema.getX() + schema.getWidth() / 2,
				schema.getY() - schema.getHeight() / 2 + schema.getHeight() * 1	/ 3);
		
		g.drawLine(schema.getX() - schema.getWidth() / 2, 
				schema.getY()- schema.getHeight() / 2 + schema.getHeight() * 2 / 3,
				schema.getX() + schema.getWidth() / 2,
				schema.getY() - schema.getHeight() / 2 + schema.getHeight() * 2	/ 3);

		g.drawLine(schema.getX() - schema.getWidth() / 2 + schema.getWidth()
				* 2 / 3, schema.getY() - schema.getHeight() / 2, schema.getX()
				- schema.getWidth() / 2 + schema.getWidth() * 2 / 3,
				schema.getY() + schema.getHeight() / 2);
		
		if(schema.depthFromBlueTris ==0 || schema.depthFromRedTris==0){
			g.setColor(Color.GREEN);
			g.drawRect(schema.getX() - schema.getWidth() / 2, schema.getY()
					- schema.getHeight() / 2, schema.getWidth(), schema.getHeight());
		}

		g.setFont(new Font(null, Font.PLAIN, schema.getHeight() / 4));
		g.setColor(Color.blue);
		g.drawString("Blue tris: " + schema.numberOfBlueTris, schema.getX()
				+ schema.getWidth() * 3 / 4, schema.getY() - schema.getHeight()
				/ 2 + schema.getHeight() / 4 * 1);
		g.drawString("Blue depth: " + schema.depthFromBlueTris, schema.getX()
				+ schema.getWidth() * 3 / 4, schema.getY() - schema.getHeight()
				/ 2 + schema.getHeight() / 4 * 3);
		g.setColor(Color.red);
		g.drawString("Red tris: " + schema.numberOfRedTris, schema.getX()
				+ schema.getWidth() * 3 / 4, schema.getY() - schema.getHeight()
				/ 2 + schema.getHeight() / 4 * 2);
		g.drawString("Red depth: " + schema.depthFromRedTris, schema.getX()
				+ schema.getWidth() * 3 / 4, schema.getY() - schema.getHeight()
				/ 2 + schema.getHeight() / 4 * 4);
		// g.drawString("Blue tris: " + schema.numberOfWhiteTris,
		// schema.x+schema.width, schema.y-schema.height/2+schema.height/4*1);

		if (schema.getChildren().size() > 0) {
			if (schema.getChildren().get(0).isVisible()) {
				
				
				for (Schema s : schema.getChildren()) {
					if (s.isVisible()) {
						g.setColor(Color.black);
						g.drawLine(schema.getX(),
								schema.getY() + schema.getHeight() / 2,
								s.getX(), s.getY() - s.getHeight() / 2);
						drawSchema(g, s);
					}
				}
			}
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// System.out.println("pressed");
		Schema dum = first.getAtPosition(e.getX(), e.getY());
		if (dum != null) {
			// System.out.println("dum.numberOfBlueTris: " +
			// dum.numberOfBlueTris);
			if (dum.isVisible()) {

				boolean isAChildrenVisible = false;
				for (Schema s : dum.getChildren())
					if (s.visible)
						isAChildrenVisible = true;

				// if(dum.getParent()!=null)
				// dum.getParent().setChildrenVisible(false);

				if (isAChildrenVisible)
					dum.setVisible(false);
				else {
					if (dum.getParent() != null) {
						dum.getParent().setVisible(false);
						dum.getParent().visible = true;
						dum.getParent().setChildrenVisible(true);
					}
					dum.setVisible(true);
				}
				dum.visible = true;

				// if (dum == first) {
				// first.setChildrenVisible(false);
				// } else {
				// Schema parent = dum.getParent();
				// if (parent != null) {
				// for (Schema s : parent.getChildren())
				// s.setVisible(false);
				// parent.setChildrenVisible(true);
				// }else{
				// dum.setChildrenVisible(false);
				// }
				//
				// if (dum.getChildren().size() > 0)
				// if (dum.getChildren().get(0).isVisible())
				// dum.setVisible(false);
				// else {
				//
				// dum.setVisible(true);
				// dum.setChildrenVisible(true);
				// }
				// }
			}
			repaint();
		}

		// if (first.contains(e.getX(), e.getY()))
		// System.out.println("contains");
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Schema dum = first.getAtPosition(e.getX(), e.getY());
		if (first == dum) {
			selected = dum;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		selected = null;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	Schema selected = null;
	
	@Override
	public void mouseDragged(MouseEvent e) {
		
		if(selected != null){
			selected.setX(e.getX());
			selected.setY(e.getY());
			repaint();
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}

}
